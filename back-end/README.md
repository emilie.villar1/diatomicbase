## Installation

First clone the repo with git:

```bash
git clone
```

Then install the python dependencies:

```bash
cd diatomicbase-back-end
pip install -r src/requirements.txt
```

### Configuration

Next step is to configure the constants in src/app/init.py

Wrote the installation path to the back-end in `DIATOMICBASE_PATH`

Define URL to postgresql db in `DATABASE_URL`

Define path to datafolder in `API_DATA_FOLDER_PATH` (it crash if it doesn't exists)

Define `RNA_SEQ_WORK_DIR` and `RNA_SEQ_DOWNLOAD_DIR`

### Database setup

We need to setup table using alembic

```bash
cd src
alembic revision --autogenerate # Generate script to update the db
alembic upgrade head # Upgrade db
```

Some search API use `pg_trgm` extension from PostgreSQL.

It need to be activated in the PostgreSQL database using the following command:

```bash
psql --username=YOUR_USERNAME --dbname=YOUR_DB -c "CREATE EXTENSION pg_trgm"
```
