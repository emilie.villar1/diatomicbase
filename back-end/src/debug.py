import asyncio
from app.load_db.loader import loader

# The server use absolute import from the app folder.
# While this work for all the scripts when uses by the server,
# it's not the case when trying to debug them individualy.
# You can import the scripts here to launch them without import error.
# In docker use:
#   docker-compose exec server python debug.py
asyncio.run(loader())
