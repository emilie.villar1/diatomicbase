from app.pipeline.background_worker import queue_manager
from app import PROCESS_CPUS
from app.db import SessionLocal

session = SessionLocal()
queue_manager(session, PROCESS_CPUS)
