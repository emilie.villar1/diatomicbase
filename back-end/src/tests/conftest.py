import os
import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from starlette.testclient import TestClient

from app.main import app
from app.db.base import Base

DATABASE_TEST_URL = os.getenv("DATABASE_TEST_URL")
if not DATABASE_TEST_URL:
    from app import DATABASE_TEST_URL

# Create a sqlite database for testing (simpler configuration, migth upgrade later)
engine = create_engine(DATABASE_TEST_URL, connect_args={"check_same_thread": False})
Session = sessionmaker()
Base.metadata.create_all(bind=engine)


@pytest.fixture(scope="module")
def test_app():
    client = TestClient(app)
    yield client


@pytest.fixture(scope="module")
def connection():
    connection = engine.connect()
    yield connection
    connection.close()


@pytest.fixture(scope="function")
def session(connection):
    transaction = connection.begin()
    session = Session(bind=connection)
    yield session
    session.close()
    transaction.rollback()
