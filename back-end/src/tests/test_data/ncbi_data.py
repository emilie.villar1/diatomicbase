sra_runinfo_row = {
    "Run": "SRR1138231",
    "ReleaseDate": "2015-01-22 20:33:38",
    "LoadDate": "2014-01-23 08:49:30",
    "spots": "12031705",
    "bases": "1167075385",
    "spots_with_mates": "0",
    "avgLength": "97",
    "size_MB": "743",
    "AssemblyName": "",
    "download_path": "https://sra-downloadb.be-md.ncbi.nlm.nih.gov/sos1/sra-pub-run-1/SRR1138231/SRR1138231.1",
    "Experiment": "SRX444227",
    "LibraryName": "nitrogen starved_48 h",
    "LibraryStrategy": "RNA-Seq",
    "LibrarySelection": "cDNA",
    "LibrarySource": "TRANSCRIPTOMIC",
    "LibraryLayout": "SINGLE",
    "InsertSize": "0",
    "InsertDev": "0",
    "Platform": "ILLUMINA",
    "Model": "Illumina HiSeq 2000",
    "SRAStudy": "SRP035546",
    "BioProject": "PRJNA235975",
    "Study_Pubmed_id": "",
    "ProjectID": "235975",
    "Sample": "SRS539867",
    "BioSample": "SAMN02592888",
    "SampleType": "simple",
    "TaxID": "2850",
    "ScientificName": "Phaeodactylum tricornutum",
    "SampleName": "Nitrogen starved_48 h",
    "g1k_pop_code": "",
    "source": "",
    "g1k_analysis_group": "",
    "Subject_ID": "",
    "Sex": "",
    "Disease": "",
    "Tumor": "no",
    "Affection_Status": "",
    "Analyte_Type": "",
    "Histological_Type": "",
    "Body_Site": "",
    "CenterName": "INSTITUTE OF OCEANOLOGY, CHINESE ACADEMY OF SCIENC",
    "Submission": "SRA128410",
    "dbgap_study_accession": "",
    "Consent": "public",
    "RunHash": "7A168B39AC380E82D10EC01458782241",
    "ReadHash": "C8491DF399B685DCE9FFE4672070B76C",
}

sra_runinfo_csv = """Run,ReleaseDate,LoadDate,spots,bases,spots_with_mates,avgLength,size_MB,AssemblyName,download_path,Experiment,LibraryName,LibraryStrategy,LibrarySelection,LibrarySource,LibraryLayout,InsertSize,InsertDev,Platform,Model,SRAStudy,BioProject,Study_Pubmed_id,ProjectID,Sample,BioSample,SampleType,TaxID,ScientificName,SampleName,g1k_pop_code,source,g1k_analysis_group,Subject_ID,Sex,Disease,Tumor,Affection_Status,Analyte_Type,Histological_Type,Body_Site,CenterName,Submission,dbgap_study_accession,Consent,RunHash,ReadHash
SRR11570918,2020-04-20 08:09:02,2020-04-20 08:05:31,15854505,4788060510,15854505,302,1715,,https://sra-download.ncbi.nlm.nih.gov/traces/sra0/SRR/011299/SRR11570918,SRX8139246,D4-RNA_1,RNA-Seq,PCR,TRANSCRIPTOMIC,PAIRED,0,0,ILLUMINA,HiSeq X Ten,SRP257461,PRJNA625589,,625589,SRS6502267,SAMN14641820,simple,2850,Phaeodactylum tricornutum,D4-RNA_1,,,,,,,no,,,,,KIST GANGNEUNG INSTITUTE OF NATURAL PRODUCTS,SRA1066953,,public,16073FC3A53A5258537CF19FBD865FB7,ADADD25AD006379A85ECDD62D190E49B
SRR11570919,2020-04-20 08:05:44,2020-04-20 08:02:37,12197436,3683625672,12197436,302,1374,,https://sra-download.ncbi.nlm.nih.gov/traces/sra80/SRR/011299/SRR11570919,SRX8139245,D4_1,RNA-Seq,PCR,TRANSCRIPTOMIC,PAIRED,0,0,ILLUMINA,HiSeq X Ten,SRP257461,PRJNA625589,,625589,SRS6502266,SAMN14641819,simple,2850,Phaeodactylum tricornutum,D4_1,,,,,,,no,,,,,KIST GANGNEUNG INSTITUTE OF NATURAL PRODUCTS,SRA1066953,,public,79E165AC26EF139F4F0BF70A61C7C5AE,ADE8FAA2E19C155B6410A0D0C10D2DEF
ERR3535086,2019-11-09 07:49:59,2019-11-09 07:52:47,24469075,7340722500,24469075,300,2751,,https://sra-download.ncbi.nlm.nih.gov/traces/era3/ERR/ERR3535/ERR3535086,ERX3551684,Sample HC1_p,RNA-Seq,Oligo-dT,TRANSCRIPTOMIC,PAIRED,150,25,ILLUMINA,Illumina HiSeq 2500,ERP108146,PRJEB34512,,588545,ERS3759321,SAMEA5789183,simple,2850,Phaeodactylum tricornutum,E-MTAB-8351:Sample HC1,,,,,,,no,,,,,Ocean University of China,ERA2128689,,public,C7CCDF1CA1E2D9CDAA428FDD1D326AAA,D5A22D83324EFB56A83458A579313DCD
SRR10586392,2019-12-17 15:32:02,2019-12-04 22:27:45,26455,203640859,0,7697,158,,https://sra-download.ncbi.nlm.nih.gov/traces/sra57/SRR/010338/SRR10586392,SRX7266726,epi,WGS,other,GENOMIC,SINGLE,0,0,OXFORD_NANOPORE,MinION,SRP234684,PRJNA593624,,593624,SRS5761389,SAMN13482973,simple,2850,Phaeodactylum tricornutum,EE-GmV-97,,,,,,,no,,,,,UNIVERSITY OF TECHNOLOGY SYDNEY,SRA1006547,,public,10CD0929524A94C16AFBB431197D9058,48442A16501D8EC42D1C218EFD31DE35
"""

sra_records_example = {
    "Count": "608",
    "RetMax": "608",
    "RetStart": "0",
    "IdList": [
        "10616483",
        "10616482",
        "10616481",
        "10616480",
        "10616479",
        "10616478",
        "10288207",
        "10288206",
        "10288205",
        "10288204",
        "10288203",
        "10288202",
        "10288201",
        "10288200",
        "10288199",
        "9674565",
        "9674564",
        "9674563",
        "9674562",
        "9674561",
        "9674560",
        "9674559",
        "9674558",
        "9541241",
        "9541240",
        "9541239",
        "9541238",
        "9541237",
        "9360443",
        "9360442",
        "9360441",
        "9360440",
        "9360439",
        "9360438",
        "9307042",
        "9307041",
        "9307040",
        "9247165",
        "9247164",
        "9247163",
        "9247162",
        "9247161",
        "9247160",
        "9247159",
        "9247158",
        "9247157",
        "9247156",
        "9247155",
        "9247154",
        "9050518",
        "9050517",
        "9050516",
        "9050515",
        "9015617",
        "9015616",
        "9015615",
        "9015614",
        "9015613",
        "8824671",
        "8824670",
        "8824669",
        "8770744",
        "8770719",
        "8770718",
        "8770717",
        "8430032",
        "8430031",
        "8430030",
        "8430029",
        "8430028",
        "8430027",
        "8258158",
        "8258157",
        "8258156",
        "8258155",
        "8258154",
        "8258153",
        "8258152",
        "8258151",
        "8258150",
        "8258149",
        "8258148",
        "7362106",
        "7362105",
        "7362104",
        "7362103",
        "7362102",
        "7362101",
        "7362100",
        "7362099",
        "7362098",
        "7362097",
        "7362090",
        "7362089",
        "7362080",
        "7362079",
        "7362078",
        "7362077",
        "7187604",
        "7187603",
        "6384934",
        "6324041",
        "6324040",
        "6324039",
        "6324038",
        "6324037",
        "6324036",
        "6324035",
        "6324034",
        "6324033",
        "6239875",
        "6239874",
        "6107479",
        "6107478",
        "6107477",
        "6107476",
        "6107475",
        "6107474",
        "5943537",
        "5943536",
        "5943535",
        "5943534",
        "5943533",
        "5943532",
        "5943531",
        "5943530",
        "5943529",
        "5943528",
        "5943527",
        "5943526",
        "5943525",
        "5943524",
        "5943523",
        "5943522",
        "5943521",
        "5943520",
        "5943519",
        "5943518",
        "5943517",
        "5943516",
        "5943515",
        "5943514",
        "5943513",
        "5943512",
        "5943511",
        "5943510",
        "5943509",
        "5943508",
        "5943507",
        "5943506",
        "5943505",
        "5943504",
        "5943503",
        "5943502",
        "5943501",
        "5457558",
        "5457557",
        "4952068",
        "4952067",
        "4952066",
        "4952065",
        "4952064",
        "4952063",
        "4952062",
        "4952061",
        "4952060",
        "4952059",
        "3939195",
        "3939194",
        "3939193",
        "3939192",
        "3939191",
        "3939190",
        "3939189",
        "3939188",
        "3939187",
        "3939186",
        "3939185",
        "3939184",
        "3939183",
        "3939182",
        "3939181",
        "3939180",
        "3939179",
        "3939178",
        "3939177",
        "3939176",
        "3939175",
        "3939174",
        "3939173",
        "3939172",
        "3939171",
        "3939170",
        "3939169",
        "3939168",
        "3939167",
        "3939166",
        "3939165",
        "3939164",
        "3939163",
        "3939162",
        "3939161",
        "3939160",
        "3939159",
        "3939158",
        "3939157",
        "3939156",
        "3939155",
        "3939154",
        "3939153",
        "3939152",
        "3939151",
        "3939150",
        "3939149",
        "3939148",
        "3939147",
        "3939146",
        "3939145",
        "3939144",
        "3939143",
        "3939142",
        "3939141",
        "3939140",
        "3939139",
        "3939138",
        "3939137",
        "3939136",
        "3939135",
        "3939134",
        "3939133",
        "3939132",
        "3939131",
        "3939130",
        "3763519",
        "3763514",
        "3763596",
        "3763595",
        "3763594",
        "3763593",
        "3763592",
        "3763591",
        "3763590",
        "3763589",
        "3763588",
        "3763587",
        "3763586",
        "3763585",
        "3763584",
        "3763583",
        "3763582",
        "3763515",
        "3721065",
        "3721064",
        "3721063",
        "3721062",
        "3721061",
        "3721060",
        "3721059",
        "3721058",
        "3721057",
        "3721056",
        "3721055",
        "3721054",
        "3594713",
        "3594712",
        "3594711",
        "3594710",
        "3594709",
        "3594708",
        "3535550",
        "3379916",
        "3379915",
        "3379914",
        "3379913",
        "3379912",
        "3379911",
        "3379910",
        "3379909",
        "3379908",
        "3379907",
        "3379906",
        "3379905",
        "3379904",
        "3379903",
        "3379902",
        "3379901",
        "3379900",
        "3379899",
        "3379898",
        "3379897",
        "3379896",
        "3379895",
        "3379894",
        "3379893",
        "3379892",
        "3379891",
        "3379890",
        "3379889",
        "3379888",
        "3379887",
        "3379886",
        "3379885",
        "3379884",
        "3379883",
        "3379882",
        "3379881",
        "3379880",
        "3379879",
        "3379878",
        "3379877",
        "3379876",
        "3379875",
        "3379874",
        "3379873",
        "3379872",
        "3379871",
        "3379870",
        "3379869",
        "3379868",
        "3379867",
        "3379866",
        "3379865",
        "3379864",
        "3379863",
        "3340425",
        "3340424",
        "3340423",
        "3340422",
        "3340421",
        "3340420",
        "3340419",
        "3340418",
        "3340417",
        "3340416",
        "3340415",
        "3340414",
        "3340413",
        "3340412",
        "3340411",
        "3340410",
        "2770356",
        "2770355",
        "2770354",
        "2770353",
        "2770346",
        "2770345",
        "2770344",
        "2770343",
        "2770342",
        "2770341",
        "2770340",
        "2770339",
        "2770338",
        "2770337",
        "2770336",
        "2770324",
        "2770323",
        "2770322",
        "2770321",
        "2770313",
        "2770312",
        "2770311",
        "2770310",
        "2770309",
        "2770308",
        "2770307",
        "2770306",
        "2770305",
        "2770304",
        "2770303",
        "2769956",
        "2769955",
        "2769954",
        "2769953",
        "2769952",
        "2769951",
        "2769950",
        "2769949",
        "2769948",
        "2769947",
        "2769946",
        "2769945",
        "2769944",
        "2769937",
        "2769936",
        "2769935",
        "2769934",
        "2769933",
        "2769932",
        "2769931",
        "2769930",
        "2769929",
        "2764417",
        "2764359",
        "2764316",
        "2764271",
        "2764231",
        "2764167",
        "2764057",
        "2764027",
        "2763978",
        "2763950",
        "2763898",
        "2763834",
        "2763816",
        "2763788",
        "2763729",
        "2763682",
        "2761981",
        "2761968",
        "2761938",
        "2761898",
        "2761368",
        "2761027",
        "2579004",
        "2579003",
        "2579002",
        "2579001",
        "2579000",
        "2578999",
        "2578998",
        "2578997",
        "2578996",
        "2578995",
        "2578994",
        "2578993",
        "2578992",
        "2578991",
        "2578990",
        "2578989",
        "2578988",
        "2578987",
        "2578986",
        "2578985",
        "2578984",
        "2578983",
        "2578982",
        "2578981",
        "2578980",
        "2578979",
        "2578978",
        "2578977",
        "2578976",
        "2578975",
        "2578974",
        "2578973",
        "2578972",
        "2578971",
        "2578970",
        "2578969",
        "2504534",
        "2504540",
        "2504539",
        "2485343",
        "2485340",
        "2485339",
        "2485336",
        "2485333",
        "2485331",
        "2485326",
        "2485325",
        "2485324",
        "2485319",
        "2485318",
        "2485313",
        "2485312",
        "2485309",
        "2485302",
        "2485299",
        "2485168",
        "2485128",
        "2219478",
        "2219477",
        "2219476",
        "2219475",
        "2219474",
        "2219473",
        "2219472",
        "2219471",
        "2219470",
        "2219469",
        "2219468",
        "2219467",
        "2219466",
        "2219465",
        "2219464",
        "2219463",
        "2219462",
        "2219461",
        "2219460",
        "2219459",
        "2219458",
        "2219457",
        "2219456",
        "2219455",
        "2219454",
        "2219453",
        "2219452",
        "2219451",
        "2219450",
        "2219449",
        "2219448",
        "2219447",
        "2219446",
        "2219445",
        "2219444",
        "2219443",
        "2219442",
        "2219441",
        "2219440",
        "2219439",
        "2219437",
        "2219436",
        "2219435",
        "2219434",
        "2219433",
        "2219432",
        "2219431",
        "2219430",
        "2219438",
        "2080851",
        "2080850",
        "2080849",
        "2080848",
        "2080847",
        "2080846",
        "2080845",
        "2080844",
        "2080843",
        "2080842",
        "2080841",
        "2080840",
        "2080839",
        "2080838",
        "2080837",
        "2080836",
        "2080835",
        "2080834",
        "2080833",
        "2080832",
        "2080831",
        "2080830",
        "2080829",
        "2080828",
        "2080827",
        "2080826",
        "2080825",
        "2080824",
        "2080823",
        "2080822",
        "1473677",
        "1473676",
        "1473675",
        "1473674",
        "1473673",
        "1473672",
        "1473671",
        "1473670",
        "1473669",
        "1473668",
        "1416676",
        "1416675",
        "1416674",
        "1416673",
        "1416672",
        "1416671",
        "1416670",
        "1416669",
        "1416668",
        "1416667",
        "1416666",
        "1416665",
        "1359455",
        "1359454",
        "1359453",
        "1359452",
        "1359451",
        "1359450",
        "1359449",
        "1359448",
        "1359447",
        "1359446",
        "907105",
        "160515",
        "160514",
        "160489",
        "160488",
        "160487",
        "160485",
        "160483",
        "791623",
        "791622",
        "791621",
        "702655",
        "702654",
        "702653",
        "702652",
        "702651",
        "702650",
        "624110",
        "624109",
        "621274",
        "390469",
        "390468",
        "73433",
        "73432",
        "73431",
        "160486",
    ],
    "TranslationSet": [
        {
            "From": "Phaeodactylum tricornutum",
            "To": '"Phaeodactylum tricornutum"[Organism] OR Phaeodactylum tricornutum[All Fields]',
        }
    ],
    "TranslationStack": [
        {
            "Term": '"Phaeodactylum tricornutum"[Organism]',
            "Field": "Organism",
            "Count": "599",
            "Explode": "Y",
        },
        {
            "Term": "Phaeodactylum tricornutum[All Fields]",
            "Field": "All Fields",
            "Count": "577",
            "Explode": "N",
        },
        "OR",
        "GROUP",
    ],
    "QueryTranslation": '"Phaeodactylum tricornutum"[Organism] OR Phaeodactylum tricornutum[All Fields]',
}

sra_xml_example = """<?xml version="1.0"  ?>
<EXPERIMENT_PACKAGE_SET>
    <EXPERIMENT_PACKAGE>
        <EXPERIMENT accession="SRX8139250" alias="PT-D14-2">
            <IDENTIFIERS>
                <PRIMARY_ID>SRX8139250</PRIMARY_ID>
            </IDENTIFIERS>
            <TITLE>D14_3</TITLE>
            <STUDY_REF accession="SRP257461">
                <IDENTIFIERS>
                    <PRIMARY_ID>SRP257461</PRIMARY_ID>
                    <EXTERNAL_ID namespace="BioProject">PRJNA625589</EXTERNAL_ID>
                </IDENTIFIERS>
            </STUDY_REF>
            <DESIGN>
                <DESIGN_DESCRIPTION>RNA-sequence on day 14 of P.tricornutum_replicate 3</DESIGN_DESCRIPTION>
                <SAMPLE_DESCRIPTOR accession="SRS6502271">
                    <IDENTIFIERS>
                        <PRIMARY_ID>SRS6502271</PRIMARY_ID>
                        <EXTERNAL_ID namespace="BioSample">SAMN14641824</EXTERNAL_ID>
                    </IDENTIFIERS>
                </SAMPLE_DESCRIPTOR>
                <LIBRARY_DESCRIPTOR>
                    <LIBRARY_NAME>PT-D14-2</LIBRARY_NAME>
                    <LIBRARY_STRATEGY>RNA-Seq</LIBRARY_STRATEGY>
                    <LIBRARY_SOURCE>TRANSCRIPTOMIC</LIBRARY_SOURCE>
                    <LIBRARY_SELECTION>PCR</LIBRARY_SELECTION>
                    <LIBRARY_LAYOUT>
                        <PAIRED/>
                    </LIBRARY_LAYOUT>
                </LIBRARY_DESCRIPTOR>
            </DESIGN>
            <PLATFORM>
                <ILLUMINA>
                    <INSTRUMENT_MODEL>HiSeq X Ten</INSTRUMENT_MODEL>
                </ILLUMINA>
            </PLATFORM>
        </EXPERIMENT>
        <SUBMISSION lab_name="SmartFarm Research Center" center_name="KIST Gangneung Institute of Natural Products" accession="SRA1066953" alias="SUB7277819">
            <IDENTIFIERS>
                <PRIMARY_ID>SRA1066953</PRIMARY_ID>
            </IDENTIFIERS>
        </SUBMISSION>
        <Organization type="institute" url="https://gn.kist.re.kr:8443/portal/main/main.do">
            <Name>KIST Gangneung Institute of Natural Products</Name>
            <Address postal_code="25451">
                <Department>SmartFarm Research Center</Department>
                <Institution>KIST Gangneung Institute of Natural Products</Institution>
                <Street>679, Saimdang-ro</Street>
                <City>Gangneung</City>
                <Sub>Gangwon-do</Sub>
                <Country>Korea, Republic of</Country>
            </Address>
            <Contact email="kdy309@naver.com">
                <Address postal_code="25451">
                    <Department>SmartFarm Research Center</Department>
                    <Institution>KIST Gangneung Institute of Natural Products</Institution>
                    <Street>679, Saimdang-ro</Street>
                    <City>Gangneung</City>
                    <Sub>Gangwon-do</Sub>
                    <Country>Korea, Republic of</Country>
                </Address>
                <Name>
                    <First>DO YEON</First>
                    <Last>KWON</Last>
                </Name>
            </Contact>
        </Organization>
        <STUDY center_name="BioProject" alias="PRJNA625589" accession="SRP257461">
            <IDENTIFIERS>
                <PRIMARY_ID>SRP257461</PRIMARY_ID>
                <EXTERNAL_ID namespace="BioProject" label="primary">PRJNA625589</EXTERNAL_ID>
            </IDENTIFIERS>
            <DESCRIPTOR>
                <STUDY_TITLE>Phaeodactylum tricornutum strain:CCAP1055 Raw sequence reads</STUDY_TITLE>
                <STUDY_TYPE existing_study_type="Whole Genome Sequencing"/>
                <STUDY_ABSTRACT>Compare to transcriptome about early and stationalry phage of P.T</STUDY_ABSTRACT>
                <CENTER_PROJECT_NAME>Phaeodactylum tricornutum</CENTER_PROJECT_NAME>
            </DESCRIPTOR>
        </STUDY>
        <SAMPLE alias="PT-D14-2" accession="SRS6502271">
            <IDENTIFIERS>
                <PRIMARY_ID>SRS6502271</PRIMARY_ID>
                <EXTERNAL_ID namespace="BioSample">SAMN14641824</EXTERNAL_ID>
            </IDENTIFIERS>
            <TITLE>D14_replicate 3</TITLE>
            <SAMPLE_NAME>
                <TAXON_ID>2850</TAXON_ID>
                <SCIENTIFIC_NAME>Phaeodactylum tricornutum</SCIENTIFIC_NAME>
            </SAMPLE_NAME>
            <DESCRIPTION>replicate3</DESCRIPTION>
            <SAMPLE_LINKS>
                <SAMPLE_LINK>
                    <XREF_LINK>
                        <DB>bioproject</DB>
                        <ID>625589</ID>
                        <LABEL>PRJNA625589</LABEL>
                    </XREF_LINK>
                </SAMPLE_LINK>
            </SAMPLE_LINKS>
            <SAMPLE_ATTRIBUTES>
                <SAMPLE_ATTRIBUTE>
                    <TAG>strain</TAG>
                    <VALUE>CCAP1055</VALUE>
                </SAMPLE_ATTRIBUTE>
                <SAMPLE_ATTRIBUTE>
                    <TAG>isolation_source</TAG>
                    <VALUE>RNA</VALUE>
                </SAMPLE_ATTRIBUTE>
                <SAMPLE_ATTRIBUTE>
                    <TAG>BioSampleModel</TAG>
                    <VALUE>Microbe, viral or environmental</VALUE>
                </SAMPLE_ATTRIBUTE>
            </SAMPLE_ATTRIBUTES>
        </SAMPLE>
        <Pool>
            <Member member_name="" accession="SRS6502271" sample_name="PT-D14-2" sample_title="D14_replicate 3" spots="13110543" bases="3959383986" tax_id="2850" organism="Phaeodactylum tricornutum">
                <IDENTIFIERS>
                    <PRIMARY_ID>SRS6502271</PRIMARY_ID>
                    <EXTERNAL_ID namespace="BioSample">SAMN14641824</EXTERNAL_ID>
                </IDENTIFIERS>
            </Member>
        </Pool>
        <RUN_SET>
            <RUN accession="SRR11570914" alias="PT-D14-2_1.gz" total_spots="13110543" total_bases="3959383986" size="1548245133" load_done="true" published="2020-04-20 07:59:13" is_public="true" cluster_name="public" static_data_available="1">
                <IDENTIFIERS>
                    <PRIMARY_ID>SRR11570914</PRIMARY_ID>
                </IDENTIFIERS>
                <EXPERIMENT_REF accession="SRX8139250">
                    <IDENTIFIERS/>
                </EXPERIMENT_REF>
                <Pool>
                    <Member member_name="" accession="SRS6502271" sample_name="PT-D14-2" sample_title="D14_replicate 3" spots="13110543" bases="3959383986" tax_id="2850" organism="Phaeodactylum tricornutum">
                        <IDENTIFIERS>
                            <PRIMARY_ID>SRS6502271</PRIMARY_ID>
                            <EXTERNAL_ID namespace="BioSample">SAMN14641824</EXTERNAL_ID>
                        </IDENTIFIERS>
                    </Member>
                </Pool>
                <SRAFiles>
                    <SRAFile cluster="public" filename="PT-D14-2_1" url="https://sra-pub-src-8.s3.amazonaws.com/SRR11570914/PT-D14-2_1.1" size="4911369809" date="2020-04-20 07:54:59" md5="bb6caa93bb6e08e8eeb30ee8f3147579" semantic_name="fastq" supertype="Original" sratoolkit="0">
                        <Alternatives url="https://storage.googleapis.com/sra-pub-src-8/SRR11570914/PT-D14-2_1.1" free_egress="worldwide" access_type="anonymous" org="GCP"/>
                        <Alternatives url="https://sra-pub-src-8.s3.amazonaws.com/SRR11570914/PT-D14-2_1.1" free_egress="worldwide" access_type="anonymous" org="AWS"/>
                    </SRAFile>
                    <SRAFile cluster="public" filename="PT-D14-2_2" url="https://sra-download.ncbi.nlm.nih.gov/traces/sra77/SRZ/011570/SRR11570914/PT-D14-2_2" size="4911369809" date="2020-04-20 07:55:23" md5="658c97019814edf5e3a8d8c301ff4d1d" semantic_name="fastq" supertype="Original" sratoolkit="0">
                        <Alternatives url="https://sra-download.ncbi.nlm.nih.gov/traces/sra77/SRZ/011570/SRR11570914/PT-D14-2_2" free_egress="worldwide" access_type="anonymous" org="NCBI"/>
                        <Alternatives url="https://sra-pub-src-7.s3.amazonaws.com/SRR11570914/PT-D14-2_2.1" free_egress="worldwide" access_type="anonymous" org="AWS"/>
                    </SRAFile>
                    <SRAFile cluster="public" filename="SRR11570914" url="https://sra-download.ncbi.nlm.nih.gov/traces/sra49/SRR/011299/SRR11570914" size="1548246358" date="2020-04-20 07:55:49" md5="29388d98e273232857a3e5c0ed59a322" semantic_name="run" supertype="Primary ETL" sratoolkit="1">
                        <Alternatives url="https://sra-download.ncbi.nlm.nih.gov/traces/sra49/SRR/011299/SRR11570914" free_egress="worldwide" access_type="anonymous" org="NCBI"/>
                        <Alternatives url="s3://sra-pub-run-8/SRR11570914/SRR11570914.1" free_egress="s3.us-east-1" access_type="aws identity" org="AWS"/>
                        <Alternatives url="gs://sra-pub-run-9/SRR11570914/SRR11570914.1" free_egress="gs.US" access_type="gcp identity" org="GCP"/>
                    </SRAFile>
                </SRAFiles>
                <CloudFiles>
                    <CloudFile filetype="fastq" provider="gs" location="gs.US"/>
                    <CloudFile filetype="fastq" provider="s3" location="s3.us-east-1"/>
                    <CloudFile filetype="run" provider="gs" location="gs.US"/>
                    <CloudFile filetype="run" provider="s3" location="s3.us-east-1"/>
                </CloudFiles>
                <Statistics nreads="2" nspots="13110543">
                    <Read index="0" count="13110543" average="151" stdev="0"/>
                    <Read index="1" count="13110543" average="151" stdev="0"/>
                </Statistics>
                <Databases>
                    <Database>
                        <Table name="SEQUENCE">
                            <Statistics source="meta">
                                <Rows count="13110543"/>
                                <Elements count="3959383986"/>
                            </Statistics>
                        </Table>
                    </Database>
                </Databases>
                <Bases cs_native="false" count="3959383986">
                    <Base value="A" count="974664062"/>
                    <Base value="C" count="1006669230"/>
                    <Base value="G" count="1011995785"/>
                    <Base value="T" count="966016811"/>
                    <Base value="N" count="38098"/>
                </Bases>
            </RUN>
        </RUN_SET>
    </EXPERIMENT_PACKAGE>
    <EXPERIMENT_PACKAGE>
        <EXPERIMENT accession="ERX3311543" alias="ena-EXPERIMENT-Normandie University, UNIROUEN-17-04-2019-16:47:33:499-10" center_name="Normandie University, UNIROUEN">
            <IDENTIFIERS>
                <PRIMARY_ID>ERX3311543</PRIMARY_ID>
            </IDENTIFIERS>
            <TITLE>Illumina Genome Analyzer IIx paired end sequencing</TITLE>
            <STUDY_REF accession="ERP108146">
                <IDENTIFIERS>
                    <PRIMARY_ID>ERP108146</PRIMARY_ID>
                    <EXTERNAL_ID namespace="BioProject">PRJEB26173</EXTERNAL_ID>
                </IDENTIFIERS>
            </STUDY_REF>
            <DESIGN>
                <DESIGN_DESCRIPTION>unspecified</DESIGN_DESCRIPTION>
                <SAMPLE_DESCRIPTOR accession="ERS2764661">
                    <IDENTIFIERS>
                        <PRIMARY_ID>ERS2764661</PRIMARY_ID>
                        <EXTERNAL_ID namespace="BioSample">SAMEA4945976</EXTERNAL_ID>
                    </IDENTIFIERS>
                </SAMPLE_DESCRIPTOR>
                <LIBRARY_DESCRIPTOR>
                    <LIBRARY_NAME>Pt3OTF</LIBRARY_NAME>
                    <LIBRARY_STRATEGY>RNA-Seq</LIBRARY_STRATEGY>
                    <LIBRARY_SOURCE>TRANSCRIPTOMIC</LIBRARY_SOURCE>
                    <LIBRARY_SELECTION>cDNA</LIBRARY_SELECTION>
                    <LIBRARY_LAYOUT>
                        <PAIRED NOMINAL_LENGTH="260"/>
                    </LIBRARY_LAYOUT>
                    <LIBRARY_CONSTRUCTION_PROTOCOL>Illumina Truseq� Stranded mRNA Sample Preparation kit</LIBRARY_CONSTRUCTION_PROTOCOL>
                </LIBRARY_DESCRIPTOR>
            </DESIGN>
            <PLATFORM>
                <ILLUMINA>
                    <INSTRUMENT_MODEL>Illumina Genome Analyzer IIx</INSTRUMENT_MODEL>
                </ILLUMINA>
            </PLATFORM>
        </EXPERIMENT>
        <SUBMISSION accession="ERA1840382" alias="ena-SUBMISSION-Normandie University, UNIROUEN-17-04-2019-16:45:18:315-1" center_name="Normandie University, UNIROUEN" lab_name="European Nucleotide Archive">
            <IDENTIFIERS>
                <PRIMARY_ID>ERA1840382</PRIMARY_ID>
            </IDENTIFIERS>
            <TITLE>Submitted by Normandie University, UNIROUEN on 17-APR-2019</TITLE>
        </SUBMISSION>
        <Organization type="center">
            <Name abbr="Normandie University, UNIROUEN">Normandie University, UNIROUEN</Name>
        </Organization>
        <STUDY accession="ERP108146" alias="ena-STUDY-Normandie University, UNIROUEN-17-04-2018-15:10:44:362-175" center_name="Normandie University, UNIROUEN">
            <IDENTIFIERS>
                <PRIMARY_ID>ERP108146</PRIMARY_ID>
                <EXTERNAL_ID namespace="BioProject">PRJEB26173</EXTERNAL_ID>
            </IDENTIFIERS>
            <DESCRIPTOR>
                <STUDY_TITLE>Determination of the whole mRNA transcriptome of the three morphotypes of Phaeodactylum tricornutum by RNA-sequencing</STUDY_TITLE>
                <STUDY_TYPE existing_study_type="Other"/>
                <STUDY_ABSTRACT>We performed RNA-sequencing experiments to analyse the differential gene expression in the three morphotypes of Phaeodactylum tricornutum Pt3 strain.</STUDY_ABSTRACT>
                <CENTER_PROJECT_NAME>RNA-sequencing of the diatom Phaeodactylum tricornutum</CENTER_PROJECT_NAME>
                <STUDY_DESCRIPTION>We performed RNA-sequencing experiments to analyse the differential gene expression in the three morphotypes of Phaeodactylum tricornutum Pt3 strain. P. tricornutum is an atypical diatom since it can display three morphotypes: fusiform, oval and triradiate. Currently, little information is available regarding the molecular and cellular biology of P. tricornutum and especially the physiological significance of its morphogenesis as well as its mechanism of regulation. In this study, we adapted the P. tricornutum Pt3 strain to obtain algal cultures particularly enriched in one dominant morphotype: fusiform, triradiate or oval in order to determine their whole mRNA transcriptome.  The diatom cells were grown at 19°C in 1L bioreactors on a 16h/8h light night cycle. The intensity of the light used was 68 µmol.m-2.s-1. The nutritive medium was composed of natural seawater 100% for the fusiform and triradiate morphotypes and 10% for the oval morphotype, enriched in Conway containing 80 mg.L-1 of sodium metasilicate (Na2SiO3). The diatom cells were cultured under ambient air at a regulated flow rate of 0.83 L.min-1 CO2. Bioreactors were inoculated with a final microalgae concentration of 1x106 cells.mL-1. According to this protocol, the cells follow similar growth curves and the culture conditions used in this study allow us to maintain the specificity of each culture batch as well as the diatom cell integrity and organization for each morphotype. The cultures were done in parallel. Four biological replicates for each culture were prepared for RNA extraction. The cells were harvested in the middle of the exponential growth phase at day 8 to proceed with the RNA purification. Total RNA was isolated from 8.108 cells. The cells were recovered by centrifugation and immediately resuspended in 1mL of TRIzol® Reagent (Ambion by Life Technologies).The suspension was then transferred in a 2 mL tube of lysing matrix E (MP Biomedicals) and immediately flash frozen in liquid nitrogen. The samples were then stored at -80°C until RNA extraction. For RNA purification, cells were lysed by using the FastPrep®-24 high-speed benchtop homogenizer (MP Biomedicals). Then, RNA was isolated by using the standard TRIzol® reagent protocol given by the supplier (Ambion®, LifeTechnlogies) followed by a purification on a Nucleospin RNA II column (Macherey-Nagel) including an on-column DNAse I treatment. For each preparation, the RNA concentration was determined in duplicate using a Nanodrop spectrophotometer (Thermo Scientific). In addition, the RNA quality was controlled using an Agilent 2100 Bioanalyser (Agilent Technologies).Libraries were then prepared from 4µg of total RNA using the Illumina Truseq® Stranded mRNA Sample Preparation kit according to the manufacturer's protocol (Illumina; RS-122-2101). Briefly, mRNAs were purified and fragmented before first strand synthesis with random hexamers, then the second strand synthesis was performed and adaptors were ligated to the products before PCR amplification of the samples as previously recommended to minimize bias. Indexed libraries were normalized and pooled two by two for random multiplexing. Libraries were subjected to a 75 bp paired-end sequencing on an Illumina GAIIx, according to the manufacturer's protocol. Two samples were pooled on a flowcell lane. Raw image files were processed using the Illumina Real Time Analysis (RTA1.9) software (Illumina). Demultiplexing was performed using CASAVA 1.8.2 (Illumina). On average, 38 millions of reads were generated per sample corresponding to 2.9 Gb per sample. 94% of bases were read with a Qscore above 30.</STUDY_DESCRIPTION>
            </DESCRIPTOR>
            <STUDY_ATTRIBUTES>
                <STUDY_ATTRIBUTE>
                    <TAG>ENA-FIRST-PUBLIC</TAG>
                    <VALUE>2019-10-16</VALUE>
                </STUDY_ATTRIBUTE>
                <STUDY_ATTRIBUTE>
                    <TAG>ENA-LAST-UPDATE</TAG>
                    <VALUE>2018-08-28</VALUE>
                </STUDY_ATTRIBUTE>
            </STUDY_ATTRIBUTES>
        </STUDY>
        <SAMPLE alias="SAMEA4945976" accession="ERS2764661">
            <IDENTIFIERS>
                <PRIMARY_ID>ERS2764661</PRIMARY_ID>
                <EXTERNAL_ID namespace="BioSample">SAMEA4945976</EXTERNAL_ID>
            </IDENTIFIERS>
            <TITLE>Pt3OTF1</TITLE>
            <SAMPLE_NAME>
                <TAXON_ID>2850</TAXON_ID>
                <SCIENTIFIC_NAME>Phaeodactylum tricornutum</SCIENTIFIC_NAME>
            </SAMPLE_NAME>
            <SAMPLE_ATTRIBUTES>
                <SAMPLE_ATTRIBUTE>
                    <TAG>Alias</TAG>
                    <VALUE>Pt3OTF1</VALUE>
                </SAMPLE_ATTRIBUTE>
                <SAMPLE_ATTRIBUTE>
                    <TAG>Description</TAG>
                    <VALUE>Oval biological replicate one</VALUE>
                </SAMPLE_ATTRIBUTE>
                <SAMPLE_ATTRIBUTE>
                    <TAG>ENA checklist</TAG>
                    <VALUE>ERC000011</VALUE>
                </SAMPLE_ATTRIBUTE>
                <SAMPLE_ATTRIBUTE>
                    <TAG>INSDC center alias</TAG>
                    <VALUE>Normandie University, UNIROUEN</VALUE>
                </SAMPLE_ATTRIBUTE>
                <SAMPLE_ATTRIBUTE>
                    <TAG>INSDC center name</TAG>
                    <VALUE>Normandie University, UNIROUEN</VALUE>
                </SAMPLE_ATTRIBUTE>
                <SAMPLE_ATTRIBUTE>
                    <TAG>INSDC first public</TAG>
                    <VALUE>2019-10-16T04:05:29Z</VALUE>
                </SAMPLE_ATTRIBUTE>
                <SAMPLE_ATTRIBUTE>
                    <TAG>INSDC last update</TAG>
                    <VALUE>2018-09-27T15:01:03Z</VALUE>
                </SAMPLE_ATTRIBUTE>
                <SAMPLE_ATTRIBUTE>
                    <TAG>INSDC status</TAG>
                    <VALUE>public</VALUE>
                </SAMPLE_ATTRIBUTE>
                <SAMPLE_ATTRIBUTE>
                    <TAG>SRA accession</TAG>
                    <VALUE>ERS2764661</VALUE>
                </SAMPLE_ATTRIBUTE>
                <SAMPLE_ATTRIBUTE>
                    <TAG>Sample Name</TAG>
                    <VALUE>ERS2764661</VALUE>
                </SAMPLE_ATTRIBUTE>
                <SAMPLE_ATTRIBUTE>
                    <TAG>Title</TAG>
                    <VALUE>RNA-sequencing of the 3 morphotypes of Phaeodactylum tricornutum</VALUE>
                </SAMPLE_ATTRIBUTE>
                <SAMPLE_ATTRIBUTE>
                    <TAG>cell_type</TAG>
                    <VALUE>OTF</VALUE>
                </SAMPLE_ATTRIBUTE>
            </SAMPLE_ATTRIBUTES>
        </SAMPLE>
        <Pool>
            <Member member_name="" accession="ERS2764661" sample_name="SAMEA4945976" sample_title="Pt3OTF1" spots="14314071" bases="2175738792" tax_id="2850" organism="Phaeodactylum tricornutum">
                <IDENTIFIERS>
                    <PRIMARY_ID>ERS2764661</PRIMARY_ID>
                    <EXTERNAL_ID namespace="BioSample">SAMEA4945976</EXTERNAL_ID>
                </IDENTIFIERS>
            </Member>
        </Pool>
        <RUN_SET>
            <RUN accession="ERR3285020" alias="ena-RUN-Normandie University, UNIROUEN-17-04-2019-16:47:33:499-10" center_name="Normandie University, UNIROUEN" total_spots="14314071" total_bases="2175738792" size="1238977974" load_done="true" published="2019-10-22 08:43:23" is_public="true" cluster_name="public" static_data_available="1">
                <IDENTIFIERS>
                    <PRIMARY_ID>ERR3285020</PRIMARY_ID>
                </IDENTIFIERS>
                <TITLE>Illumina Genome Analyzer IIx paired end sequencing</TITLE>
                <EXPERIMENT_REF accession="ERX3311543">
                    <IDENTIFIERS>
                        <PRIMARY_ID>ERX3311543</PRIMARY_ID>
                    </IDENTIFIERS>
                </EXPERIMENT_REF>
                <RUN_ATTRIBUTES>
                    <RUN_ATTRIBUTE>
                        <TAG>ENA-FIRST-PUBLIC</TAG>
                        <VALUE>2019-10-16</VALUE>
                    </RUN_ATTRIBUTE>
                    <RUN_ATTRIBUTE>
                        <TAG>ENA-LAST-UPDATE</TAG>
                        <VALUE>2019-04-18</VALUE>
                    </RUN_ATTRIBUTE>
                </RUN_ATTRIBUTES>
                <Pool>
                    <Member member_name="" accession="ERS2764661" sample_name="SAMEA4945976" sample_title="Pt3OTF1" spots="14314071" bases="2175738792" tax_id="2850" organism="Phaeodactylum tricornutum">
                        <IDENTIFIERS>
                            <PRIMARY_ID>ERS2764661</PRIMARY_ID>
                            <EXTERNAL_ID namespace="BioSample">SAMEA4945976</EXTERNAL_ID>
                        </IDENTIFIERS>
                    </Member>
                </Pool>
                <SRAFiles>
                    <SRAFile cluster="public" filename="ERR3285020" url="https://sra-download.ncbi.nlm.nih.gov/traces/era3/ERR/ERR3285/ERR3285020" size="1238979719" date="2019-10-24 14:55:26" md5="8cd1f914b820dda8ce46b6fae8b08d59" semantic_name="run" supertype="Primary ETL" sratoolkit="1">
                        <Alternatives url="https://sra-download.ncbi.nlm.nih.gov/traces/era3/ERR/ERR3285/ERR3285020" free_egress="worldwide" access_type="anonymous" org="NCBI"/>
                        <Alternatives url="s3://sra-pub-run-9/ERR3285020/ERR3285020.1" free_egress="s3.us-east-1" access_type="aws identity" org="AWS"/>
                        <Alternatives url="gs://sra-pub-run-9/ERR3285020/ERR3285020.1" free_egress="gs.US" access_type="gcp identity" org="GCP"/>
                    </SRAFile>
                </SRAFiles>
                <CloudFiles>
                    <CloudFile filetype="run" provider="gs" location="gs.US"/>
                    <CloudFile filetype="run" provider="s3" location="s3.us-east-1"/>
                </CloudFiles>
                <Statistics nreads="2" nspots="14314071">
                    <Read index="0" count="14314071" average="76" stdev="0"/>
                    <Read index="1" count="14314071" average="76" stdev="0"/>
                </Statistics>
                <Bases cs_native="false" count="2175738792">
                    <Base value="A" count="546814595"/>
                    <Base value="C" count="542742682"/>
                    <Base value="G" count="542972652"/>
                    <Base value="T" count="542902448"/>
                    <Base value="N" count="306415"/>
                </Bases>
            </RUN>
        </RUN_SET>
    </EXPERIMENT_PACKAGE>
</EXPERIMENT_PACKAGE_SET>
"""
