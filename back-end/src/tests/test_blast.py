import pytest
from typing import List
from copy import deepcopy
from sqlalchemy.orm import Session
from tests.test_data.ensembl_data import (
    genome_example,
    gene_example,
    transcript_example,
    cds_example,
)
from app.db import models
from app.load_db.ensembl.cds import insert_cds_in_db
from app.load_db.ensembl.gene import insert_genes_in_db
from app.load_db.ensembl.transcript import insert_transcripts_in_db
from app.load_db.ensembl.dna import EnsemblRetrieveGenomeSequence
from app.load_db.ensembl.genome_info import EnsemblRetrieveGenomeInfo
from app.pipeline.BLAST.database_setup import BLASTDatabaseSetup

SPECIES = "Phaeodactylum tricornutum"


def setup_blastdb(session, tmp_path):
    work_path = tmp_path / "BLAST"
    blastdb = BLASTDatabaseSetup(session, work_path)
    return blastdb


def setup_genome_and_genes(session):
    """Put genome_example in db, and add 2 genes Phatr3_J50624 and Phatr3_J50640 based on gene_example

    Args:
        session (Session): A Session object to access db
    """
    # # Setup Genome, GenomeTopLevelRegion
    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)
    gene_example2 = deepcopy(gene_example)
    gene_example2["id"] = "Phatr3_J50640"
    insert_genes_in_db(
        session,
        [gene_example, gene_example2],
        [
            {"assembly_id": 1, "eco": "", "embl": "", "uniprotkb": "", "region_id": 1},
            {"assembly_id": 1, "eco": "", "embl": "", "uniprotkb": "", "region_id": 2},
        ],
    )
    insert_transcripts_in_db(
        session,
        [transcript_example, transcript_example],
        [
            {"gene_id": 1, "assembly_id": 1, "region_id": 1},
            {"gene_id": 2, "assembly_id": 1, "region_id": 2},
        ],
    )

    insert_cds_in_db(
        session,
        [cds_example, cds_example],
        [
            {
                "gene_id": 1,
                "transcript_id": 1,
                "assembly_id": 1,
                "region_id": 2,
            },
            {
                "gene_id": 2,
                "transcript_id": 2,
                "assembly_id": 1,
                "region_id": 2,
            },
        ],
    )


def test_create_folder_correctly(session: Session, tmp_path):
    # Create instance to test
    work_path = tmp_path / "BLAST"
    prot_db_folder = work_path / "protein_db"
    nucl_db_folder = work_path / "nucleotide_db"
    blastdb = BLASTDatabaseSetup(session, work_path)
    # Test it doesn't crash when work path, prot db folder and nucl db folder already exists
    blastdb2 = BLASTDatabaseSetup(session, work_path)

    # Test automatic creation of folder
    assert (
        work_path.exists() == True
    ), "BLASTDatabaseSetup should have created the work path if it doesn't exist"
    assert (
        prot_db_folder.exists() == True
    ), "BLASTDatabaseSetup should have created the protein db folder if it doesn't exist"
    assert (
        nucl_db_folder.exists() == True
    ), "BLASTDatabaseSetup should have created the nucleotide db folder if it doesn't exist"


def test_get_all_genes(session: Session, tmp_path):
    # # Setup Genome, GenomeTopLevelRegion
    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)
    gene_example2 = deepcopy(gene_example)
    gene_example2["id"] = "Phatr3_J50640"
    insert_genes_in_db(
        session,
        [gene_example, gene_example2],
        [
            {"assembly_id": 1, "eco": "", "embl": "", "uniprotkb": "", "region_id": 1},
            {"assembly_id": 1, "eco": "", "embl": "", "uniprotkb": "", "region_id": 2},
        ],
    )

    blastdb = setup_blastdb(session, tmp_path)
    # test function
    genes = blastdb.get_all_gene()

    assert len(genes) == 2


def test_create_header_gene_fasta(session: Session, tmp_path):
    setup_genome_and_genes(session)
    blastdb = setup_blastdb(session, tmp_path)

    genes: List[models.Gene] = session.query(models.Gene).all()

    print(genes[0].name)
    print(genes[0].seq_region.name)
    print(genes[1].name)
    print(genes[1].seq_region.name)
    expected_headers = [
        ">Phatr3_J50624 chrom=33 species=Phaeodactylum_tricornutum",
        ">Phatr3_J50640 chrom=chloroplast species=Phaeodactylum_tricornutum",
    ]

    for i, gene in enumerate(genes):
        header = blastdb.create_header_gene_fasta(gene)
        assert header == expected_headers[i]


def test_create_fasta(session, tmp_path, monkeypatch):
    # Mock Gene.get_sequence()
    def mock_gene_get_sequence(self, session):
        return "ATCTTAGTCGATCGATCAGT"

    monkeypatch.setattr(
        models.Gene,
        "get_sequence",
        mock_gene_get_sequence,
    )
    monkeypatch.setattr(
        models.Gene,
        "get_protein_sequence",
        mock_gene_get_sequence,
    )

    # setup gneome, genes and BLASTDatabaseSetup
    setup_genome_and_genes(session)
    blastdb = setup_blastdb(session, tmp_path)

    # Create gene fasta
    nt_path = tmp_path / "test_nt.fa"
    blastdb.create_fasta(nt_path, "nucl")

    expected_test_nt_file = (
        ">Phatr3_J50624 chrom=33 species=Phaeodactylum_tricornutum\n"
        "ATCTTAGTCGATCGATCAGT\n"
        ">Phatr3_J50640 chrom=chloroplast species=Phaeodactylum_tricornutum\n"
        "ATCTTAGTCGATCGATCAGT\n"
    )
    # Get gene fasta and test it
    nt = open(nt_path, "r").read()
    assert expected_test_nt_file == nt

    aa_path = tmp_path / "test_aa.fa"
    blastdb.create_fasta(aa_path, "prot")

    expected_test_aa_file = (
        ">Phatr3_J50624 chrom=33 species=Phaeodactylum_tricornutum\n"
        "ATCTTAGTCGATCGATCAGT\n"
        ">Phatr3_J50640 chrom=chloroplast species=Phaeodactylum_tricornutum\n"
        "ATCTTAGTCGATCGATCAGT\n"
    )
    # Get gene fasta and test it
    aa = open(aa_path, "r").read()
    assert expected_test_aa_file == aa
