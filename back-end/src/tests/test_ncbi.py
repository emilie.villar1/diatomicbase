import csv
import pytest
from typing import List
from io import StringIO
from datetime import datetime
import xmltodict
from app.db.models import SraExperiment, SraStudy
from app.load_db.ncbi.rna_seq_metadata import NcbiRetrieveRnaSeqMetaData
from tests.test_data.ncbi_data import (
    sra_runinfo_row,
    sra_runinfo_csv,
    sra_records_example,
    sra_xml_example,
)

email = "test@mail.com"


def test_filter_non_rna_seq_row(session):
    # Get input for function
    sra_csv = csv.DictReader(StringIO(sra_runinfo_csv))
    sra_csv_filtered = NcbiRetrieveRnaSeqMetaData(session).filter_non_rna_seq_row(
        sra_csv
    )
    # Test the filtering
    for experience in sra_csv_filtered:
        assert experience["LibraryStrategy"] == "RNA-Seq"


def test_create_sra_studies(session):
    # Get input for function
    sra_csv = csv.DictReader(StringIO(sra_runinfo_csv))
    expected_sra_studies = [
        SraStudy(accession="SRP257461"),
        SraStudy(accession="ERP108146"),
        SraStudy(accession="SRP234684"),
    ]
    # Get instance for access to self.sra_studies
    retrieve = NcbiRetrieveRnaSeqMetaData(session)
    # Test function
    retrieve.create_sra_studies(sra_csv)

    for sra_study, expected_sra_study in zip(
        retrieve.sra_studies, expected_sra_studies
    ):
        assert sra_study.accession == expected_sra_study.accession
        assert sra_study.title == expected_sra_study.title


def test_not_in_sra_studies(session):
    # Setup
    # not_in_sra_studies use self.sra_studies
    retrieve = NcbiRetrieveRnaSeqMetaData(session)
    retrieve.sra_studies = [
        SraStudy(accession="SRP251837"),
        SraStudy(accession="ERP117424"),
        SraStudy(accession="ERP193249", title="A title"),
    ]
    # Test function
    should_be_find = retrieve.not_in_sra_studies("SRP251837")
    should_not_be_find = retrieve.not_in_sra_studies("SSS123554")

    assert should_be_find is False
    assert should_not_be_find is True


def test_create_sra_study_object(session):
    sra_study_expected_1 = SraStudy(accession="SRP251837")
    sra_study_expected_2 = SraStudy(accession="SRP251964", title="An study article")

    retrieve_info = NcbiRetrieveRnaSeqMetaData(session)
    sra_study_1 = retrieve_info.create_sra_study_object(accession="SRP251837")
    sra_study_2 = retrieve_info.create_sra_study_object(
        accession="SRP251964", title="An study article"
    )

    assert sra_study_1.accession == sra_study_expected_1.accession
    assert sra_study_2.accession == sra_study_expected_2.accession
    assert sra_study_1.title == sra_study_expected_1.title
    assert sra_study_2.title == sra_study_expected_2.title


def test_create_new_sra_experiment(session):

    # Prepare sra_test_csv to be only two line for simplicity
    sra_test_csv = sra_runinfo_csv.splitlines()[0:3]
    sra_test_csv = "\n".join(sra_test_csv)
    # Read csv
    sra = csv.DictReader(StringIO(sra_test_csv))

    expected_sra_experiments = [
        SraExperiment(
            run="SRR11570918",
            release_date=datetime.strptime("2020-04-20 08:09:02", "%Y-%m-%d %H:%M:%S"),
            load_date=datetime.strptime("2020-04-20 08:05:31", "%Y-%m-%d %H:%M:%S"),
            average_length="302",
            dl_path="https://sra-download.ncbi.nlm.nih.gov/"
            "traces/sra0/SRR/011299/SRR11570918",
            library_name="D4-RNA_1",
            library_selection="PCR",
            library_layout="PAIRED",
            platform="ILLUMINA",
            model="HiSeq X Ten",
            project="625589",
            sample_name="D4-RNA_1",
            center_name="KIST GANGNEUNG INSTITUTE OF NATURAL PRODUCTS",
            species_name="Phaeodactylum tricornutum",
            study_id=1,
        ),
        SraExperiment(
            run="SRR11570919",
            release_date=datetime.strptime("2020-04-20 08:05:44", "%Y-%m-%d %H:%M:%S"),
            load_date=datetime.strptime("2020-04-20 08:02:37", "%Y-%m-%d %H:%M:%S"),
            average_length="302",
            dl_path="https://sra-download.ncbi.nlm.nih.gov/"
            "traces/sra80/SRR/011299/SRR11570919",
            library_name="D4_1",
            library_selection="PCR",
            library_layout="PAIRED",
            platform="ILLUMINA",
            model="HiSeq X Ten",
            project="625589",
            sample_name="D4_1",
            center_name="KIST GANGNEUNG INSTITUTE OF NATURAL PRODUCTS",
            species_name="Phaeodactylum tricornutum",
            study_id=1,
        ),
    ]
    # Get instance for access to self.sra_studies
    retrieve = NcbiRetrieveRnaSeqMetaData(session)
    # Create an sra_study for the link in SraExperiment
    retrieve.sra_studies = [
        SraStudy(accession="SRP257461"),
        SraStudy(accession="ERP108146"),
    ]
    retrieve.insert_all_sra_study_in_db()
    # Test function
    retrieve.create_new_sra_experiment(sra)

    # With zip the loop would stop if one of the two list is shorter than the other
    assert len(retrieve.sra_experiments) == len(expected_sra_experiments)

    for sra_experiment, expected_sra_experiment in zip(
        retrieve.sra_experiments, expected_sra_experiments
    ):
        assert sra_experiment.run == expected_sra_experiment.run
        assert sra_experiment.release_date == expected_sra_experiment.release_date
        assert sra_experiment.load_date == expected_sra_experiment.load_date
        assert sra_experiment.average_length == expected_sra_experiment.average_length
        assert sra_experiment.dl_path == expected_sra_experiment.dl_path
        assert sra_experiment.library_name == expected_sra_experiment.library_name
        assert sra_experiment.library_layout == expected_sra_experiment.library_layout
        assert sra_experiment.platform == expected_sra_experiment.platform
        assert sra_experiment.model == expected_sra_experiment.model
        assert sra_experiment.study_id == expected_sra_experiment.study_id
        assert sra_experiment.project == expected_sra_experiment.project
        assert sra_experiment.sample_name == expected_sra_experiment.sample_name
        assert sra_experiment.center_name == expected_sra_experiment.center_name
        assert sra_experiment.species_name == expected_sra_experiment.species_name
        assert (
            sra_experiment.library_selection
            == expected_sra_experiment.library_selection
        )

    # Test that create_not_existing_experiment work
    # Add previous sra_experiments in db
    session.add_all(retrieve.sra_experiments)
    # Update with one addictional row
    sra_test_csv = sra_runinfo_csv.splitlines()[0:4]
    sra_test_csv = "\n".join(sra_test_csv)
    sra = csv.DictReader(StringIO(sra_test_csv))
    # Add new expected sra_experiment
    expected_sra_experiments.append(
        SraExperiment(
            run="ERR3535086",
            release_date=datetime.strptime("2019-11-09 07:49:59", "%Y-%m-%d %H:%M:%S"),
            load_date=datetime.strptime("2019-11-09 07:52:47", "%Y-%m-%d %H:%M:%S"),
            average_length="300",
            dl_path="https://sra-download.ncbi.nlm.nih.gov/"
            "traces/era3/ERR/ERR3535/ERR3535086",
            library_name="Sample HC1_p",
            library_selection="Oligo-dT",
            library_layout="PAIRED",
            platform="ILLUMINA",
            model="Illumina HiSeq 2500",
            project="588545",
            sample_name="E-MTAB-8351:Sample HC1",
            center_name="Ocean University of China",
            species_name="Phaeodactylum tricornutum",
            study_id=2,
        )
    )
    # Test function
    retrieve.create_new_sra_experiment(sra)
    # Verify it just added the last sra
    # With zip the loop would stop if one of the two list is shorter than the other
    assert len(retrieve.sra_experiments) == len(expected_sra_experiments)

    for sra_experiment, expected_sra_experiment in zip(
        retrieve.sra_experiments, expected_sra_experiments
    ):
        assert sra_experiment.run == expected_sra_experiment.run
        assert sra_experiment.release_date == expected_sra_experiment.release_date
        assert sra_experiment.load_date == expected_sra_experiment.load_date
        assert sra_experiment.average_length == expected_sra_experiment.average_length
        assert sra_experiment.dl_path == expected_sra_experiment.dl_path
        assert sra_experiment.library_name == expected_sra_experiment.library_name
        assert sra_experiment.library_layout == expected_sra_experiment.library_layout
        assert sra_experiment.platform == expected_sra_experiment.platform
        assert sra_experiment.model == expected_sra_experiment.model
        assert sra_experiment.study_id == expected_sra_experiment.study_id
        assert sra_experiment.project == expected_sra_experiment.project
        assert sra_experiment.sample_name == expected_sra_experiment.sample_name
        assert sra_experiment.center_name == expected_sra_experiment.center_name
        assert sra_experiment.species_name == expected_sra_experiment.species_name
        assert (
            sra_experiment.library_selection
            == expected_sra_experiment.library_selection
        )


def test_find_associated_study_id(session):
    # Add study to db for test
    retrieve = NcbiRetrieveRnaSeqMetaData(session)
    retrieve.sra_studies = [
        SraStudy(accession="SRP257461"),
        SraStudy(accession="ERP117424"),
        SraStudy(accession="SRP234684"),
    ]
    retrieve.insert_all_sra_study_in_db()

    # Test normal function
    ids = retrieve.find_associated_study_id("SRP257461")
    assert ids == 1
    # Test with no study to find
    with pytest.raises(NameError):
        retrieve.find_associated_study_id("SRPSDAW23")


def test_create_sra_experiment_object(session):
    session.add(SraStudy(accession="SRP035546", title="hello"))
    sra_object_expected = SraExperiment(
        run="SRR1138231",
        release_date=datetime.strptime("2015-01-22 20:33:38", "%Y-%m-%d %H:%M:%S"),
        load_date=datetime.strptime("2014-01-23 08:49:30", "%Y-%m-%d %H:%M:%S"),
        average_length="97",
        dl_path="https://sra-downloadb.be-md.ncbi.nlm.nih.gov/"
        "sos1/sra-pub-run-1/SRR1138231/SRR1138231.1",
        library_name="nitrogen starved_48 h",
        library_selection="cDNA",
        library_layout="SINGLE",
        platform="ILLUMINA",
        model="Illumina HiSeq 2000",
        project="235975",
        sample_name="Nitrogen starved_48 h",
        center_name="INSTITUTE OF OCEANOLOGY, CHINESE ACADEMY OF SCIENC",
        species_name="Phaeodactylum tricornutum",
        study_id=1,
    )

    sra_object = NcbiRetrieveRnaSeqMetaData(
        session, email=email
    ).create_sra_experiment_object(sra_runinfo_row, 1)

    assert sra_object.run == sra_object_expected.run
    assert sra_object.release_date == sra_object_expected.release_date
    assert sra_object.load_date == sra_object_expected.load_date
    assert sra_object.average_length == sra_object_expected.average_length
    assert sra_object.dl_path == sra_object_expected.dl_path
    assert sra_object.library_name == sra_object_expected.library_name
    assert sra_object.library_layout == sra_object_expected.library_layout
    assert sra_object.library_selection == sra_object_expected.library_selection
    assert sra_object.platform == sra_object_expected.platform
    assert sra_object.model == sra_object_expected.model
    assert sra_object.study_id == sra_object_expected.study_id
    assert sra_object.project == sra_object_expected.project
    assert sra_object.sample_name == sra_object_expected.sample_name
    assert sra_object.center_name == sra_object_expected.center_name
    assert sra_object.species_name == sra_object_expected.species_name


def test_insert_all_sra_study_in_db(session):

    retrieve = NcbiRetrieveRnaSeqMetaData(session)
    expected_sra_studies = [
        SraStudy(accession="SRP257461"),
        SraStudy(accession="ERP117424"),
        SraStudy(accession="SRP234684"),
    ]
    retrieve.sra_studies = expected_sra_studies
    # Test function
    retrieve.insert_all_sra_study_in_db()

    studies_find = session.query(SraStudy).all()

    assert len(studies_find) == 3

    for study, expected_study in zip(studies_find, expected_sra_studies):
        assert study.accession == expected_study.accession
        assert study.title == expected_study.title


def test_bulk_insert_sra_experiment_in_db(session):
    expected_sra_experiments = [
        SraExperiment(
            run="SRR11570918",
            release_date=datetime.strptime("2020-04-20 08:09:02", "%Y-%m-%d %H:%M:%S"),
            load_date=datetime.strptime("2020-04-20 08:05:31", "%Y-%m-%d %H:%M:%S"),
            average_length=302,
            dl_path="https://sra-download.ncbi.nlm.nih.gov/"
            "traces/sra0/SRR/011299/SRR11570918",
            library_name="D4-RNA_1",
            library_selection="PCR",
            library_layout="PAIRED",
            platform="ILLUMINA",
            model="HiSeq X Ten",
            project=625589,
            sample_name="D4-RNA_1",
            center_name="KIST GANGNEUNG INSTITUTE OF NATURAL PRODUCTS",
            species_name="Phaeodactylum tricornutum",
            study_id=1,
        ),
        SraExperiment(
            run="SRR11570919",
            release_date=datetime.strptime("2020-04-20 08:05:44", "%Y-%m-%d %H:%M:%S"),
            load_date=datetime.strptime("2020-04-20 08:02:37", "%Y-%m-%d %H:%M:%S"),
            average_length=302,
            dl_path="https://sra-download.ncbi.nlm.nih.gov/"
            "traces/sra80/SRR/011299/SRR11570919",
            library_name="D4_1",
            library_selection="PCR",
            library_layout="PAIRED",
            platform="ILLUMINA",
            model="HiSeq X Ten",
            project=625589,
            sample_name="D4_1",
            center_name="KIST GANGNEUNG INSTITUTE OF NATURAL PRODUCTS",
            species_name="Phaeodactylum tricornutum",
            study_id=1,
        ),
    ]
    # Get instance
    retrieve = NcbiRetrieveRnaSeqMetaData(session)
    # Create an sra_study for the link in SraExperiment
    retrieve.sra_studies = [SraStudy(accession="SRP257461")]
    retrieve.insert_all_sra_study_in_db()
    # Bulk insert use attribute sra_experiments
    retrieve.sra_experiments = expected_sra_experiments
    # Test function
    retrieve.bulk_insert_sra_experiment_in_db()
    # Retrieve inserted SraExperiment
    sra_experiments = session.query(SraExperiment).all()

    assert len(sra_experiments) == 2

    for sra_experiment, expected_sra_experiment in zip(
        sra_experiments, expected_sra_experiments
    ):
        assert sra_experiment.run == expected_sra_experiment.run
        assert sra_experiment.release_date == expected_sra_experiment.release_date
        assert sra_experiment.load_date == expected_sra_experiment.load_date
        assert sra_experiment.average_length == expected_sra_experiment.average_length
        assert sra_experiment.dl_path == expected_sra_experiment.dl_path
        assert sra_experiment.library_name == expected_sra_experiment.library_name
        assert sra_experiment.library_layout == expected_sra_experiment.library_layout
        assert sra_experiment.platform == expected_sra_experiment.platform
        assert sra_experiment.model == expected_sra_experiment.model
        assert sra_experiment.study_id == expected_sra_experiment.study_id
        assert sra_experiment.project == expected_sra_experiment.project
        assert sra_experiment.sample_name == expected_sra_experiment.sample_name
        assert sra_experiment.center_name == expected_sra_experiment.center_name
        assert sra_experiment.species_name == expected_sra_experiment.species_name
        assert (
            sra_experiment.library_selection
            == expected_sra_experiment.library_selection
        )


def test_ncbi_retrieve_rna_seq_meta_data(session, monkeypatch):
    expected_sra_experiments = [
        SraExperiment(
            run="SRR11570918",
            release_date=datetime.strptime("2020-04-20 08:09:02", "%Y-%m-%d %H:%M:%S"),
            load_date=datetime.strptime("2020-04-20 08:05:31", "%Y-%m-%d %H:%M:%S"),
            average_length=302,
            dl_path="https://sra-download.ncbi.nlm.nih.gov/"
            "traces/sra0/SRR/011299/SRR11570918",
            library_name="D4-RNA_1",
            library_selection="PCR",
            library_layout="PAIRED",
            platform="ILLUMINA",
            model="HiSeq X Ten",
            project=625589,
            sample_name="D4-RNA_1",
            center_name="KIST GANGNEUNG INSTITUTE OF NATURAL PRODUCTS",
            species_name="Phaeodactylum tricornutum",
            study_id=1,
        ),
        SraExperiment(
            run="SRR11570919",
            release_date=datetime.strptime("2020-04-20 08:05:44", "%Y-%m-%d %H:%M:%S"),
            load_date=datetime.strptime("2020-04-20 08:02:37", "%Y-%m-%d %H:%M:%S"),
            average_length=302,
            dl_path="https://sra-download.ncbi.nlm.nih.gov/"
            "traces/sra80/SRR/011299/SRR11570919",
            library_name="D4_1",
            library_selection="PCR",
            library_layout="PAIRED",
            platform="ILLUMINA",
            model="HiSeq X Ten",
            project=625589,
            sample_name="D4_1",
            center_name="KIST GANGNEUNG INSTITUTE OF NATURAL PRODUCTS",
            species_name="Phaeodactylum tricornutum",
            study_id=1,
        ),
        SraExperiment(
            run="ERR3535086",
            release_date=datetime.strptime("2019-11-09 07:49:59", "%Y-%m-%d %H:%M:%S"),
            load_date=datetime.strptime("2019-11-09 07:52:47", "%Y-%m-%d %H:%M:%S"),
            average_length=300,
            dl_path="https://sra-download.ncbi.nlm.nih.gov/"
            "traces/era3/ERR/ERR3535/ERR3535086",
            library_name="Sample HC1_p",
            library_selection="Oligo-dT",
            library_layout="PAIRED",
            platform="ILLUMINA",
            model="Illumina HiSeq 2500",
            project=588545,
            sample_name="E-MTAB-8351:Sample HC1",
            center_name="Ocean University of China",
            species_name="Phaeodactylum tricornutum",
            study_id=2,
        ),
    ]
    expected_sra_studies = [
        SraStudy(
            accession="SRP257461",
            title="Phaeodactylum tricornutum strain:CCAP1055 Raw sequence reads",
            abstract="Compare to transcriptome about early and stationalry phage of P.T",
        ),
        SraStudy(
            accession="ERP108146",
            title="Determination of the whole mRNA transcriptome of the three morphotypes of Phaeodactylum tricornutum by RNA-sequencing",
            abstract="We performed RNA-sequencing experiments to analyse the differential gene expression in the three morphotypes of Phaeodactylum tricornutum Pt3 strain.",
        ),
    ]

    # Mock NCBI API call
    def mock_fetch_sra_id_from_species(self, species_name):
        return sra_records_example

    def mock_fetch_sra_runinfo_from_sra_records(self, esearch_record):
        return csv.DictReader(StringIO(sra_runinfo_csv))

    def mock_fetch_sra_xml_from_sra_records(self, esearch_record):
        return xmltodict.parse(sra_xml_example)

    monkeypatch.setattr(
        NcbiRetrieveRnaSeqMetaData,
        "fetch_sra_id_from_species",
        mock_fetch_sra_id_from_species,
    )

    monkeypatch.setattr(
        NcbiRetrieveRnaSeqMetaData,
        "fetch_sra_runinfo_from_sra_records",
        mock_fetch_sra_runinfo_from_sra_records,
    )

    monkeypatch.setattr(
        NcbiRetrieveRnaSeqMetaData,
        "fetch_sra_xml_from_sra_records",
        mock_fetch_sra_xml_from_sra_records,
    )
    # Launch automaticaly run() when the species name is given
    NcbiRetrieveRnaSeqMetaData(session).run("Phaeodactylum Tricornutum")

    # Test that inserted sra_studies correspond
    sra_studies = session.query(SraStudy).all()
    assert len(sra_studies) == len(expected_sra_studies)
    for study, expected_study in zip(sra_studies, expected_sra_studies):
        assert study.accession == expected_study.accession
        assert study.title == expected_study.title

    # Test that inserted sra_experiments correspond
    sra_experiments = session.query(SraExperiment).all()
    assert len(sra_experiments) == len(expected_sra_experiments)
    for sra_experiment, expected_sra_experiment in zip(
        sra_experiments, expected_sra_experiments
    ):
        assert sra_experiment.run == expected_sra_experiment.run
        assert sra_experiment.release_date == expected_sra_experiment.release_date
        assert sra_experiment.load_date == expected_sra_experiment.load_date
        assert sra_experiment.average_length == expected_sra_experiment.average_length
        assert sra_experiment.dl_path == expected_sra_experiment.dl_path
        assert sra_experiment.library_name == expected_sra_experiment.library_name
        assert sra_experiment.library_layout == expected_sra_experiment.library_layout
        assert sra_experiment.platform == expected_sra_experiment.platform
        assert sra_experiment.model == expected_sra_experiment.model
        assert sra_experiment.study_id == expected_sra_experiment.study_id
        assert sra_experiment.project == expected_sra_experiment.project
        assert sra_experiment.sample_name == expected_sra_experiment.sample_name
        assert sra_experiment.center_name == expected_sra_experiment.center_name
        assert sra_experiment.species_name == expected_sra_experiment.species_name
        assert (
            sra_experiment.library_selection
            == expected_sra_experiment.library_selection
        )


def test_get_sra_study_by_accession(session):
    study_accession = "SRP006802"
    session.add(SraStudy(accession="SRP006802"))

    study_find = NcbiRetrieveRnaSeqMetaData(session).get_sra_study_by_accession(
        study_accession
    )
    study_not_find = NcbiRetrieveRnaSeqMetaData(session).get_sra_study_by_accession(
        "NotGoodAccession"
    )

    assert study_find.id == 1
    assert study_find.accession == study_accession
    assert study_not_find is None


def test_update_sra_studies_with_title_and_abstract(session):
    # Create a SraStudy object to update
    session.add(SraStudy(accession="SRP257461"))
    session.add(SraStudy(accession="ERP108146"))
    # Prepare xml
    sra_xml = xmltodict.parse(sra_xml_example)
    # Test function
    NcbiRetrieveRnaSeqMetaData(session).update_sra_studies_with_title_and_abstract(
        sra_xml
    )
    # Retrieve sra_study
    sra_studies: List[SraStudy] = session.query(SraStudy).all()
    # Assert correct info have been inserted
    assert sra_studies[0].accession == "SRP257461"
    assert sra_studies[1].accession == "ERP108146"
    assert (
        sra_studies[0].title
        == "Phaeodactylum tricornutum strain:CCAP1055 Raw sequence reads"
    )
    assert (
        sra_studies[1].title
        == "Determination of the whole mRNA transcriptome of the three morphotypes of Phaeodactylum tricornutum by RNA-sequencing"
    )
    assert (
        sra_studies[0].abstract
        == "Compare to transcriptome about early and stationalry phage of P.T"
    )
    assert (
        sra_studies[1].abstract
        == "We performed RNA-sequencing experiments to analyse the differential gene expression in the three morphotypes of Phaeodactylum tricornutum Pt3 strain."
    )


def test_sra_expriment_in_db(session):
    # Create an experiment in db
    sra_experiment = SraExperiment(
        run="SRR11570918",
        release_date=datetime.strptime("2020-04-20 08:09:02", "%Y-%m-%d %H:%M:%S"),
        load_date=datetime.strptime("2020-04-20 08:05:31", "%Y-%m-%d %H:%M:%S"),
        average_length="302",
        dl_path="https://sra-download.ncbi.nlm.nih.gov/"
        "traces/sra0/SRR/011299/SRR11570918",
        library_name="D4-RNA_1",
        library_selection="PCR",
        library_layout="PAIRED",
        platform="ILLUMINA",
        model="HiSeq X Ten",
        project="625589",
        sample_name="D4-RNA_1",
        center_name="KIST GANGNEUNG INSTITUTE OF NATURAL PRODUCTS",
        species_name="Phaeodactylum tricornutum",
        study_id=1,
    )
    session.add(sra_experiment)
    # test function with id in db and not in db
    found = True
    is_in_db = NcbiRetrieveRnaSeqMetaData(session).sra_experiment_in_db("SRR11570918")
    not_in_db = NcbiRetrieveRnaSeqMetaData(session).sra_experiment_in_db("SRJ74567943")
    assert found == is_in_db
    assert not found == not_in_db
