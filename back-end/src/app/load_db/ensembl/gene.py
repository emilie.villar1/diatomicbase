import re
from typing import List, Dict
from sqlalchemy.orm import Session
from app.db.models import Gene
from app.load_db.utils import (
    get_seq_regions,
    select_genomes_from_species,
    select_last_genome,
    get_last_assembly_id_by_name,
    get_assembly_name_from_gene_name,
)
from app.load_db.ensembl.utils import fetch_ensembl


def fetch_genes_from_region(species: str, region: str):
    genes = fetch_ensembl(f"overlap/region/{species}/{region}?feature=gene")
    return genes


def extract_ECO_EMBL_UniProtKB_id(description: str):
    if not description:
        return ("", "", "")

    eco, embl, uniprotkb = "", "", ""

    eco_match = re.search(r"ECO:(\d*)", description)
    embl_match = re.search(r"EMBL:([^}]*)", description)
    uniprotkb_match = re.search(r"UniProtKB/TrEMBL;Acc:([^\]]*)", description)

    if not uniprotkb_match:
        uniprotkb_match = re.search(r"UniProtKB/Swiss-Prot;Acc:([^\]]*)", description)

    if eco_match:
        eco = eco_match.group(1)
    if embl_match:
        embl = embl_match.group(1)
    if uniprotkb_match:
        uniprotkb = uniprotkb_match.group(1)

    return eco, embl, uniprotkb


def insert_genes_in_db(session: Session, genes, genes_additional_info):
    genes_objects = [
        Gene(
            name=gene["id"],
            start=gene["start"],
            end=gene["end"],
            biotype=gene["biotype"],
            strand=gene["strand"],
            source=gene["source"],
            description=gene["description"],
            search_description=format_search_description(gene["description"]),
            assembly_id=gene_info["assembly_id"],
            seq_region_id=gene_info["region_id"],
            eco=gene_info["eco"],
            embl=gene_info["embl"],
            uniprotkb=gene_info["uniprotkb"],
        )
        for gene, gene_info in zip(genes, genes_additional_info)
    ]
    session.bulk_save_objects(genes_objects)
    session.commit()


def retrieve_genes_and_insert_in_db(session: Session, species: str):
    # Ensembl REST API allow to retrieve feature (gene/transcript/exon/feature)
    # from species name only by region (chromosome/contig). This code will
    # retrieve the region name and fetch the feature region by region
    # to insert them in db
    genomes = select_genomes_from_species(session, species)
    genome = select_last_genome(genomes)
    seq_regions = get_seq_regions(genome)
    for region in seq_regions:
        print(f"Retrieving genes from chromosome/contig {region.name}")
        genes = fetch_genes_from_region(species, region.name)
        # Extract info for each gene in a dict and add them in a list.
        # Allow to insert genes in bulk after, increasing the performance
        genes_additional_info = []
        for gene in genes:
            eco, embl, uniprotkb = extract_ECO_EMBL_UniProtKB_id(gene["description"])
            # Some gene doesn't have an assembly associated, or have
            # '_bd' added at the end. this code use their name to determine
            # at which assembly they belong
            if not gene["assembly_name"] or gene["assembly_name"].endswith("bd"):
                gene["assembly_name"] = get_assembly_name_from_gene_name(gene["id"])

            assembly_id = get_last_assembly_id_by_name(session, gene["assembly_name"])

            genes_additional_info.append(
                {
                    "assembly_id": assembly_id,
                    "eco": eco,
                    "embl": embl,
                    "uniprotkb": uniprotkb,
                    "region_id": region.id,
                }
            )

        insert_genes_in_db(session, genes, genes_additional_info)


def format_search_description(description: str):
    if not description:
        return None
    clean = re.search(r"[^\[\]]*", description)
    return description[clean.start() : clean.end()].strip()
