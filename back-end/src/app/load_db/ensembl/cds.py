import re
from sqlalchemy.orm import Session
from app.db.models import Cds
from app.load_db.utils import (
    get_seq_regions,
    select_genomes_from_species,
    select_last_genome,
    get_last_assembly_id_by_name,
    get_assembly_name_from_gene_name,
    get_gene_id_from_name,
    get_gene_name_from_transcript_name,
    get_transcript_id_from_name,
)
from app.load_db.ensembl.utils import fetch_feature_from_region


def get_gene_name_from_cds_name(cds_name: str) -> str:
    match_gene = re.search(r"([\w]*).p\d*$", cds_name)
    if match_gene:
        return match_gene.group(1)
    else:
        return ""


def insert_cds_in_db(session: Session, CDS, CDS_additional_info):

    CDS_object = [
        Cds(
            name=cds["id"],
            strand=cds["strand"],
            start=cds["start"],
            end=cds["end"],
            source=cds["source"],
            phase=cds["phase"],
            gene_id=cds_additional_info["gene_id"],
            transcript_id=cds_additional_info["transcript_id"],
            assembly_id=cds_additional_info["assembly_id"],
            seq_region_id=cds_additional_info["region_id"],
        )
        for cds, cds_additional_info in zip(CDS, CDS_additional_info)
    ]
    session.bulk_save_objects(CDS_object)
    session.commit()


def correct_draftJ_typo(gene_name: str) -> str:
    if "draftJ" in gene_name:
        return gene_name.replace("draftJ", "Jdraft")
    else:
        return gene_name


def retrieve_cds_and_insert_in_db(session: Session, species: str) -> None:
    # Ensembl REST API allow to retrieve feature (gene/transcript/exon/feature)
    # from species name only by region (chromosome/contig). This code will
    # retrieve the region name and fetch the feature region by region
    # to insert them in db
    genomes = select_genomes_from_species(session, species)
    genome = select_last_genome(genomes)
    seq_regions = get_seq_regions(genome)
    for region in seq_regions:
        print(f"Retrieving CDS from chromosome/contig {region.name}")
        CDS = fetch_feature_from_region("cds", region.name, species)
        # Extract info from each trancript in a dict and add them in a list.
        # Allow to insert cds in bulk after, increasing the performance
        CDS_additional_info = []
        for cds in CDS:
            gene_name = get_gene_name_from_cds_name(cds["id"])
            # Some transcript and exon (coding for tRNA generally) doesn't
            # have classic naming like 'Phatr3_EG02414-E2' but something
            # like 'EMLSAE00000058895'. With those last id you can't extract
            # the gene name from it, so we use the function
            # get_gene_name_from_transcript_name. It will retrieve the
            # transcript in the db and return the gene name associated
            if not gene_name:
                gene_name = get_gene_name_from_transcript_name(session, cds["Parent"])
            # Some cds doesn't have an assembly associated, or have
            # '_bd' added at the end. This if/elif code use their name to
            # determine at which assembly they belong
            if cds["assembly_name"].endswith("bd"):
                cds["assembly_name"] = cds["assembly_name"].replace("_bd", "")
            elif not cds["assembly_name"]:
                cds["assembly_name"] = get_assembly_name_from_gene_name(gene_name)

            assembly_id = get_last_assembly_id_by_name(session, cds["assembly_name"])
            gene_id = get_gene_id_from_name(session, gene_name)
            transcript_id = get_transcript_id_from_name(session, cds["Parent"])

            # If doesn't find the gene associated, look for typo and correct it
            if gene_id == 0:
                gene_name = correct_draftJ_typo(gene_name)
                gene_id = get_gene_id_from_name(session, gene_name)

            CDS_additional_info.append(
                {
                    "gene_id": gene_id,
                    "transcript_id": transcript_id,
                    "assembly_id": assembly_id,
                    "region_id": region.id,
                }
            )

        insert_cds_in_db(session, CDS, CDS_additional_info)
