from sqlalchemy.orm import Session
from app.db.models import Transcript
from app.load_db.utils import (
    get_seq_regions,
    select_genomes_from_species,
    select_last_genome,
    get_last_assembly_id_by_name,
    get_assembly_name_from_gene_name,
    get_gene_id_from_name,
)
from app.load_db.ensembl.utils import fetch_feature_from_region


def insert_transcripts_in_db(
    session: Session, transcripts, transcripts_additional_info
):
    transcripts_object = [
        Transcript(
            name=transcript["id"],
            biotype=transcript["biotype"],
            description=transcript["description"],
            strand=transcript["strand"],
            start=transcript["start"],
            end=transcript["end"],
            source=transcript["source"],
            gene_id=transcript_additional_info["gene_id"],
            assembly_id=transcript_additional_info["assembly_id"],
            seq_region_id=transcript_additional_info["region_id"],
        )
        for transcript, transcript_additional_info in zip(
            transcripts, transcripts_additional_info
        )
    ]
    session.bulk_save_objects(transcripts_object)
    session.commit()


def retrieve_transcript_and_insert_in_db(session: Session, species: str):
    # Ensembl REST API allow to retrieve feature (gene/transcript/exon/feature)
    # from species name only by region (chromosome/contig). This code will
    # retrieve the region name and fetch the feature region by region
    # to insert them in db
    genomes = select_genomes_from_species(session, species)
    genome = select_last_genome(genomes)
    seq_regions = get_seq_regions(genome)
    for region in seq_regions:
        print(f"Retrieving trancript from chromosome/contig {region.name}")
        transcripts = fetch_feature_from_region("transcript", region.name, species)
        # Extract info from each trancript in a dict and add them in a list.
        # Allow to insert transcript in bulk after, increasing the performance
        transcripts_additional_info = []
        for transcript in transcripts:
            # Some transcript doesn't have an assembly associated, or have
            # '_bd' added at the end. This if/elif code use their name to
            # determine at which assembly they belong
            if transcript["assembly_name"].endswith("bd"):
                transcript["assembly_name"] = transcript["assembly_name"].replace(
                    "_bd", ""
                )
            elif not transcript["assembly_name"]:
                transcript["assembly_name"] = get_assembly_name_from_gene_name(
                    transcript["Parent"]
                )

            assembly_id = get_last_assembly_id_by_name(
                session, transcript["assembly_name"]
            )
            gene_id = get_gene_id_from_name(session, transcript["Parent"])
            transcripts_additional_info.append(
                {"gene_id": gene_id, "assembly_id": assembly_id, "region_id": region.id}
            )

        insert_transcripts_in_db(session, transcripts, transcripts_additional_info)
