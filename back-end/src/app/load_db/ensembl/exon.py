import re
from sqlalchemy.orm import Session
from app.db.models import Exon
from app.load_db.utils import (
    get_seq_regions,
    select_genomes_from_species,
    select_last_genome,
    get_last_assembly_id_by_name,
    get_assembly_name_from_gene_name,
    get_gene_id_from_name,
    get_gene_name_from_transcript_name,
    get_transcript_id_from_name,
)
from app.load_db.ensembl.utils import fetch_feature_from_region


def get_gene_name_from_exon_name(exon_name: str) -> str:
    match_gene = re.search(r"([\w]*)-E\d*$", exon_name)
    if match_gene:
        return match_gene.group(1)
    else:
        return ""


def insert_exons_in_db(session: Session, exons, exons_additional_info):
    exons_object = [
        Exon(
            name=exon["id"],
            strand=exon["strand"],
            start=exon["start"],
            end=exon["end"],
            constitutive=exon["constitutive"],
            rank=exon["rank"],
            phase=exon["ensembl_phase"],
            end_phase=exon["ensembl_end_phase"],
            source=exon["source"],
            gene_id=exon_additional_info["gene_id"],
            transcript_id=exon_additional_info["transcript_id"],
            assembly_id=exon_additional_info["assembly_id"],
            seq_region_id=exon_additional_info["region_id"],
        )
        for exon, exon_additional_info in zip(exons, exons_additional_info)
    ]
    session.bulk_save_objects(exons_object)
    session.commit()


def retrieve_exons_and_insert_in_db(session: Session, species: str):
    # Ensembl REST API allow to retrieve feature (gene/transcript/exon/feature)
    # from species name only by region (chromosome/contig). This code will
    # retrieve the region name and fetch the feature region by region
    # to insert them in db
    genomes = select_genomes_from_species(session, species)
    genome = select_last_genome(genomes)
    seq_regions = get_seq_regions(genome)
    for region in seq_regions:
        print(f"Retrieving exons from chromosome/contig {region.name}")
        exons = fetch_feature_from_region("exon", region.name, species)
        # Extract info from each trancript in a dict and add them in a list.
        # Allow to insert exons in bulk after, increasing the performance
        exons_additional_info = []
        for exon in exons:
            gene_name = get_gene_name_from_exon_name(exon["id"])
            # Some transcript and exon (coding for tRNA generally) doesn't
            # have classic naming like 'Phatr3_EG02414-E2' but something
            # like 'EMLSAE00000058895'. With those last id you can't extract
            # the gene name from it, so we use the function
            # get_gene_name_from_transcript_name. It will retrieve the
            # transcript in the db and return the gene name associated
            if not gene_name:
                gene_name = get_gene_name_from_transcript_name(session, exon["Parent"])
            # Some exon doesn't have an assembly associated, or have
            # '_bd' added at the end. This if/elif code use their name to
            # determine at which assembly they belong
            if exon["assembly_name"].endswith("bd"):
                exon["assembly_name"] = exon["assembly_name"].replace("_bd", "")
            elif not exon["assembly_name"]:
                exon["assembly_name"] = get_assembly_name_from_gene_name(gene_name)

            assembly_id = get_last_assembly_id_by_name(session, exon["assembly_name"])
            gene_id = get_gene_id_from_name(session, gene_name)
            transcript_id = get_transcript_id_from_name(session, exon["Parent"])

            exons_additional_info.append(
                {
                    "gene_id": gene_id,
                    "transcript_id": transcript_id,
                    "assembly_id": assembly_id,
                    "region_id": region.id,
                }
            )

        insert_exons_in_db(session, exons, exons_additional_info)
