from typing import List
from sqlalchemy.orm.session import Session

from app import UNIPROT_API
from app.db.models import Gene, DomainAnnotation
from app.load_db.utils import fetch_endpoint


def fetch_uniprot(request, content_type="application/json"):
    return fetch_endpoint(UNIPROT_API, request, content_type)


def get_genes_with_uniprot_id(session: Session) -> List[Gene]:
    genes = session.query(Gene).all()
    genes_filter = [gene for gene in genes if gene.uniprotkb != ""]
    return genes_filter


def get_domain_annotation_by_accession(
    session: Session, domain_annotation_accession: str
) -> DomainAnnotation:
    query = (
        session.query(DomainAnnotation)
        .filter(DomainAnnotation.accession == domain_annotation_accession)
        .first()
    )
    return query
