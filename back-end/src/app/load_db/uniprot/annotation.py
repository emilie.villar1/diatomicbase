import re
from copy import deepcopy
from typing import List, Dict, Optional
from sqlalchemy.orm.session import Session
from app.db.models import Gene, GeneOntology, DomainAnnotation
from app.load_db.uniprot.utils import (
    get_domain_annotation_by_accession,
    get_genes_with_uniprot_id,
    fetch_uniprot,
)


class GoInfo:
    go_term: str
    type: str
    annotation: str
    assert_by: str
    gene: Optional[Gene]


def retrieve_uniprot_sheet_from_uniprot_id(uniprot_id: str) -> str:
    sheet = fetch_uniprot(f"{uniprot_id}.txt", "text/plain")
    return sheet


def extract_go_info(sheet: str) -> List[Dict[str, str]]:
    """Using a txt uniprot page, extract GO_terms
    and return a List of dict with the GO info

    Args:
        sheet (str): An uniprot protein page in txt format

    Returns:
        List[GoInfo]: List of dict with the 'go_term', 'annotation', 'type'
                        and 'assert_by' keys
    """
    go_terms = []
    for line in sheet.splitlines():
        if not line.startswith("DR   GO;"):
            continue

        tmp_go = {}
        go_match = re.search(r"(GO:\d{7})", line)
        cell_component_match = re.search(r"; C:([^;]*)", line)
        mol_function_match = re.search(r"; F:([^;]*)", line)
        biol_process_match = re.search(r"; P:([^;]*)", line)
        assert_by_match = re.search(r"; IEA:([^.]*)", line)

        if go_match:
            tmp_go["go_term"] = go_match.group(1)
        if mol_function_match:
            tmp_go["type"] = "F"
            tmp_go["annotation"] = mol_function_match.group(1)
        if cell_component_match:
            tmp_go["type"] = "C"
            tmp_go["annotation"] = cell_component_match.group(1)
        if biol_process_match:
            tmp_go["type"] = "P"
            tmp_go["annotation"] = biol_process_match.group(1)
        if assert_by_match:
            tmp_go["assert_by"] = assert_by_match.group(1)
        if tmp_go == {}:
            print("NO TMP GO in: ", line)
        go_terms.append(tmp_go)

    return go_terms


def get_gene_ontology_by_name(session: Session, go_term: str):
    query = session.query(GeneOntology).filter(GeneOntology.term == go_term).first()
    return query


def insert_go_term_in_db(session: Session, go_terms: List[Dict[str, str]]) -> None:
    """Using a list of GoInfo dict, link existing GeneOntology to the
    gene annotate with it, and create the GeneOntology entry if
    it doesn't exist.

    Args:
        session (Session): SqlAlchemy session object to access to the db
        go_terms (List[GoInfo]): List of dict return by extract_go_info +
                                add_gene_to_list_of_dict
    """
    for go_term in go_terms:
        gene_ontology_in_db = get_gene_ontology_by_name(session, go_term["go_term"])
        if gene_ontology_in_db:
            gene_ontology_in_db.genes.append(go_term["gene"])
            session.commit()
        else:
            go = GeneOntology(term=go_term["go_term"])
            if "annotation" in go_term:
                go.type = go_term["type"]
                go.annotation = go_term["annotation"]
            go.genes.append(go_term["gene"])
            session.add(go)

    session.commit()


def add_gene_to_list_of_dict(
    list_of_dict: List[Dict[str, str]], gene: Gene
) -> List[Dict[str, str]]:
    """Add the gene given to the dicts in the list

    Args:
        list_of_dict (List[Dict[str, str]]): self explanetory
        gene (Gene): Gene instance of the gene to add at each dict

    Returns:
        List[Dict[str, str]]: go_info with gene added
    """
    updated = deepcopy(list_of_dict)
    for dict_to_update in updated:
        dict_to_update["gene"] = gene
    return updated


def insert_domains_annotation_in_db(session: Session, domains_annotation_info) -> None:
    """For each dict in domains_annotation_info, create the domain_annotation
    entry if it doesn't exist, or link with the associated gene if it exist

    Args:
        session (Session): SqlAlchemy session object to access to the db
        domains_annotation_info (List[Dict[str, str/Gene]]): List of dict return by
                        extract_domain_annotation_info + add_gene_to_list_of_dict
    """
    for domain_annotation in domains_annotation_info:
        domain_annotation_in_db = get_domain_annotation_by_accession(
            session, domain_annotation["accession"]
        )
        # If entry already exist, link with the gene given
        if domain_annotation_in_db:
            domain_annotation_in_db.genes.append(domain_annotation["gene"])
            session.commit()
        else:
            # Create the interpro_subdb entry
            domain_object = DomainAnnotation(
                db_name=domain_annotation["db_name"],
                accession=domain_annotation["accession"],
            )
            # Add gene link
            domain_object.genes.append(domain_annotation["gene"])
            # Add description if present
            if "description" in domain_annotation:
                domain_object.description = domain_annotation["description"]
            session.add(domain_object)
    session.commit()


def description_is_different_than_accession(description_match, accession_match) -> bool:
    return description_match.group(1) != accession_match.group(1)


def extract_domain_annotation_info(uniprot_sheet: str) -> List[Dict[str, str]]:
    """Using an uniprot txt page extract InterPro and other domain annotation
    (ex: Pfam, HAMAP, PANTHER, Prosite) accession and description.
    Return one dict by id.

    Args:
        uniprot_sheet (str): An uniprot protein page in txt format already readed

    Returns:
        List[Dict[str, str]]: List of domain id and function in dict
    """
    domains_annotation = []
    pass_go_line = False
    for line in uniprot_sheet.splitlines():
        if not line.startswith("DR   "):
            continue
        elif line.startswith("DR   GO"):
            pass_go_line = True
            continue
        elif pass_go_line:
            tmp_domain_annotation = {}
            db_name_match = re.search(r"DR   (\w*)", line)
            accession_match = re.search(r"DR   \w*; ([^;]*)", line)
            description_match = re.search(r"DR   \w*; [^;]*; ([^;]*)", line)

            tmp_domain_annotation["db_name"] = db_name_match.group(1)
            tmp_domain_annotation["accession"] = accession_match.group(1)
            if (
                description_is_different_than_accession(
                    description_match, accession_match
                )
                and description_match.group(1) != "-"
            ):
                if description_match.group(1).endswith("."):
                    tmp_domain_annotation["description"] = description_match.group(1)[
                        :-1
                    ]
                else:
                    tmp_domain_annotation["description"] = description_match.group(1)
            domains_annotation.append(tmp_domain_annotation)

    return domains_annotation


def retrieve_annotation_and_insert_in_db(session: Session):
    genes = get_genes_with_uniprot_id(session)
    print(f"Number of genes with annotations to retrieve: {len(genes)}")
    go_info = []
    domains_annotation_info = []
    for i, gene in enumerate(genes):

        print(f"Retrieving annotation from gene {gene.uniprotkb} {i+1}/{len(genes)}")
        uniprot_sheet = retrieve_uniprot_sheet_from_uniprot_id(gene.uniprotkb)

        go_extracted = extract_go_info(uniprot_sheet)
        go_info_with_gene = add_gene_to_list_of_dict(go_extracted, gene)
        go_info += go_info_with_gene

        domain_annotation_extracted = extract_domain_annotation_info(uniprot_sheet)
        domain_annotation_with_gene = add_gene_to_list_of_dict(
            domain_annotation_extracted, gene
        )
        domains_annotation_info += domain_annotation_with_gene

    print("Insert go term in db")
    insert_go_term_in_db(session, go_info)
    print("Insert domains annotation in db")
    insert_domains_annotation_in_db(session, domains_annotation_info)
