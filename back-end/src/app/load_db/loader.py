from app import SPECIES, EMAIL
from app.db import SessionLocal
from app.load_db.ensembl.genome_info import EnsemblRetrieveGenomeInfo
from app.load_db.ensembl.dna import EnsemblRetrieveGenomeSequence
from app.load_db.ensembl.gene import retrieve_genes_and_insert_in_db
from app.load_db.ensembl.transcript import retrieve_transcript_and_insert_in_db
from app.load_db.ensembl.exon import retrieve_exons_and_insert_in_db
from app.load_db.ensembl.cds import retrieve_cds_and_insert_in_db
from app.load_db.ensembl.variant import retrieve_variants_and_insert_in_db
from app.load_db.ensembl.xrefs import RetrieveXrefs
from app.load_db.uniprot.annotation import retrieve_annotation_and_insert_in_db
from app.load_db.uniprot.domain import retrieve_gene_domains_and_insert_in_db
from app.load_db.gprofiler.kegg import RetrieveKegg
from app.load_db.ncbi.rna_seq_metadata import NcbiRetrieveRnaSeqMetaData


def loader():
    session = SessionLocal()
    # Retrieve Assembly info
    EnsemblRetrieveGenomeInfo(session).run(SPECIES)
    # Retrieve DNA sequence.
    # Depends on the presence of a genome, its top_level_region and the
    # seq_region associated in the db to retrieve sequence
    # from ensembl REST API and link them to the sequences
    EnsemblRetrieveGenomeSequence(session).run(SPECIES)
    # Retrieve gene in ensembl associated with the species
    # Depends on the presence of a genome, the top_level_region
    # and seq_region
    retrieve_genes_and_insert_in_db(session, SPECIES)
    # Retrieve Trancscript in ensembl associated with the species
    # Depends on the presence of a genome, the top_level_region
    # seq_region, and gene
    retrieve_transcript_and_insert_in_db(session, SPECIES)
    # Retrieve Exon in ensembl associated with the species
    # Depends on the presence of a genome, the top_level_region
    # seq_region, gene, and transcript
    retrieve_exons_and_insert_in_db(session, SPECIES)
    # Retrieve CDS in ensembl associated with the species
    # Depends on the presence of a genome, the top_level_region
    # seq_region, gene, and transcript
    retrieve_cds_and_insert_in_db(session, SPECIES)
    # Retrieve GO term and domains annotation associated with gene
    # in uniprot if those have an uniprotkb id.
    # Depends on gene presence in db.
    retrieve_annotation_and_insert_in_db(session)
    # Retrieve Domains position and linked annotation for
    # genes with uniprotkb id.
    # Depends on gene and domains annotation presence in db.
    retrieve_gene_domains_and_insert_in_db(session)
    # Retrieve Kegg associated with gene using gProfiler API,
    # and link them
    # Depends on gene presence in db.
    RetrieveKegg(session, SPECIES)
    # Retrieve variant in ensembl using the REST API,
    # Depends on the presence of a Genome
    # retrieve_variants_and_insert_in_db(session, SPECIES)
    # Retrieve Rna-seq metadata from SRA,
    # Doesn't depend on anything
    NcbiRetrieveRnaSeqMetaData(session, SPECIES)
    # Retrieve Xrefs
    # Depends on the presence of a Genome,
    # the top_level_region, seq_region, and gene
    RetrieveXrefs(session, EMAIL).run(SPECIES)
