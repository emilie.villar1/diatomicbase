ENSEMBL_REST = "http://rest.ensembl.org/"
UNIPROT_API = "https://www.uniprot.org/uniprot/"
SPECIES = "Phaeodactylum tricornutum"
EMAIL = "test@mail.com"

# Used by RNASeqManager to find DESeq2_precomp.R and compute diff expression analysis
DIATOMICBASE_PATH = "/users/mpb/zweig/diatomicbase-back-end"

DATABASE_URL = "postgresql://diatomicbase:*********@localhost:5432/diatomicbase_dev"
DATABASE_TEST_URL = "sqlite://"

LOG_FILE_PATH = "back_end.log"
API_DATA_FOLDER_PATH = "../data"
# Pipeline argument
PIPELINE_CPUS = 2
PIPELINE_MAX_RAM = 7
# The gtf files are expected to be found in RNA_SEQ_DOWNLOAD_DIR/genomes
GTF_FILES = [
    {
        "species": "Phaeodactylum tricornutum",
        "filename": "Phaeodactylum_tricornutum.ASM15095v2.47.gtf",
    }
]
# The tx2gene files are expected to be found in RNA_SEQ_DOWNLOAD_DIR/genomes
TX2GENE_FILES = [
    {
        "species": "Phaeodactylum tricornutum",
        "filename": "tx2gene.csv",
    }
]
USE_FASTERQ = True
FASTERQ_TMPDIR = "/localtmp"
RNA_SEQ_COMPARISON_TABLE = "../data/rna_seq_comparisons.tsv"  # In TSV
RNA_SEQ_DIFF_EXPRESSION_OUTPUT = "../data/out.tsv"
RNA_SEQ_DOB_TABLE = (
    "../data/table_DOB.csv"  # Path to manually curated rna-seq metadata file
)
RNA_SEQ_DOB_TABLE = "../data/rna_seq_bioproject_description.tsv"
RNA_SEQ_RUNS_GROUP_TABLE = "../data/rna_seq_runs_group.tsv"
RNA_SEQ_ENV = "conda"  # Use by nfcore/rna-seq to determine if it should use docker/singularity/conda
RNA_SEQ_ALIGNER = "hisat2"  # Choice between STAR and hisat2
RNA_SEQ_WORK_DIR = "/usr/rna_work_dir"
RNA_SEQ_DOWNLOAD_DIR = "/usr/src/app/app/pipeline/downloads/RNA_seq"
RNA_SEQ_BIOPROJECT_BLACKLIST = [
    101889
]  # Blacklist of bioproject from processing due to weird behavior (ex: having only one fastq dl in paired rna_seq)
NXF_SINGULARITY_CACHEDIR = (
    ""  # Where nextflow stock singularity image to avoid duplication
)
SINGULARITY_TMPDIR = ""

# BLAST
BLAST_WORK_DIR = "../data/BLAST/"

# COEXPRESSION NETWORK (tsv)
GENE_TO_MODULEPHAEONET = "../data/gene2modulePhaeonet.tsv"
MODULEPHAEONET_TO_KEGG = "../data/modulesPhaeonet2KEGG.tsv"

# URL to the jbrowse data not handled by FastAPI server (no slash at the end)
SERVER_DATA_URL = "http://localhost:8000/file"

PROCESS_CPUS = 2

PHAEONET_DATA_TSV = "../data/data_from_phaeonet.tsv"
