from fastapi import FastAPI
from fastapi_sqlalchemy import DBSessionMiddleware, db
from fastapi.middleware.cors import CORSMiddleware
from app import API_DATA_FOLDER_PATH, PROCESS_CPUS
from app.db import DATABASE_URL
from app.api import gene, jbrowse, rna_seq, file, search
from app.api.staticfiles import RangedStaticFiles
from app.pipeline.background_worker import queue_manager

app = FastAPI()
# Add Db middleware for simple db access in function
app.add_middleware(DBSessionMiddleware, db_url=DATABASE_URL)
# Configure CORS
origins = [
    "*",
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(gene.router, prefix="/gene")
app.include_router(search.router, prefix="/search")
app.include_router(rna_seq.router, prefix="/rna_seq")
app.include_router(jbrowse.router, prefix="/jbrowse")
app.mount("/file", RangedStaticFiles(directory=API_DATA_FOLDER_PATH), name="file")


@app.on_event("startup")
async def launch_queue_process():
    with db():
        queue_manager(db.session, n_process=PROCESS_CPUS)
