from typing import List, Dict
from Bio.Seq import Seq
from sqlalchemy import func
from sqlalchemy import (
    Column,
    Integer,
    String,
    ForeignKey,
    Float,
    DateTime,
    Enum,
    Boolean,
    UnicodeText,
)
from sqlalchemy.orm import relationship, Session
from app.db.base import Base


def retrieve_sequence(
    session: Session, sequence: str, start: int, length_seq: int
) -> str:
    select_seq_func = func.substr(sequence, start, length_seq)
    return session.query(select_seq_func).first()[0]


def reverse_complement(sequence: str) -> str:
    translate_table = str.maketrans("ACTG", "TGAC")
    return sequence.translate(translate_table)[::-1]


class Genome(Base):
    __tablename__ = "genome"

    id = Column(Integer, primary_key=True)
    species = Column(String, nullable=False)
    assembly_accession = Column(String(20))
    assembly_name = Column(String(20))
    assembly_date = Column(String(20))
    base_pairs = Column(Integer)
    genebuild_method = Column(String(20))
    genebuild_initial_release_date = Column(String(20))
    genebuild_last_geneset_update = Column(String(20))
    genebuild_start_date = Column(String(20))

    karyotype = relationship("GenomeKaryotype")
    top_level_region = relationship("GenomeTopLevelRegion")


class GenomeKaryotype(Base):
    __tablename__ = "karyotype"

    id = Column(Integer, primary_key=True)
    genome_id = Column(Integer, ForeignKey("genome.id"), nullable=False)
    name = Column(String(20), nullable=False)


class GenomeTopLevelRegion(Base):
    __tablename__ = "top_level_region"

    id = Column(Integer, primary_key=True)
    length = Column(Integer, nullable=False)
    coord_system = Column(String(20), nullable=False)
    genome_id = Column(Integer, ForeignKey("genome.id"), nullable=False)
    seq_region_id = Column(Integer, ForeignKey("seq_region.id"))

    seq_region = relationship("SeqRegion", foreign_keys=seq_region_id)


class Dna(Base):
    __tablename__ = "dna"

    id = Column(Integer, primary_key=True)
    sequence = Column(String, nullable=False)


class SeqRegion(Base):
    __tablename__ = "seq_region"

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    dna_id = Column(Integer, ForeignKey("dna.id"))

    dna = relationship("Dna", foreign_keys=dna_id)


class Gene(Base):
    __tablename__ = "gene"

    id = Column(Integer, primary_key=True)
    name = Column(String(30), nullable=False)
    biotype = Column(String(20), nullable=False)
    strand = Column(Integer)
    start = Column(Integer, nullable=False)
    end = Column(Integer, nullable=False)
    description = Column(String, nullable=True)
    eco = Column(String(10), nullable=True)
    embl = Column(String(20), nullable=True)
    uniprotkb = Column(String(10), nullable=True)
    ncbi_display_id = Column(String(50), nullable=True)
    ncbi_internal_id = Column(Integer, nullable=True)
    source = Column(String, nullable=False)
    search_description = Column(String)

    assembly_id = Column(Integer, ForeignKey("genome.id"))
    seq_region_id = Column(Integer, ForeignKey("seq_region.id"))
    kegg_id = Column(Integer, ForeignKey("kegg.id"))

    assembly = relationship("Genome", foreign_keys=assembly_id)
    seq_region = relationship("SeqRegion", foreign_keys=seq_region_id)
    transcript = relationship("Transcript", back_populates="gene")
    exon = relationship("Exon", back_populates="gene")
    cds = relationship("Cds", back_populates="gene")
    go = relationship("GeneOntology", secondary="gene_go_ref", back_populates="genes")
    domains = relationship("Domain", back_populates="gene")
    domains_annotation = relationship(
        "DomainAnnotation",
        secondary="gene_domain_annotation_ref",
        back_populates="genes",
    )
    kegg = relationship("Kegg", foreign_keys=kegg_id, back_populates="genes")

    def get_sequence(self, session: Session) -> str:
        """Retrieve Gene sequence and return it,
        depend on a sqlalchemy Session object to retrieve sequence efficiently using SQL

        Args:
            session (Session): A sqlalchemy Session object to access the db

        Returns:
            str: A gene DNA sequence
        """
        length_seq = self.end - self.start + 1
        seq = retrieve_sequence(
            session, self.seq_region.dna.sequence, self.start, length_seq
        )
        if self.strand == -1:
            seq = reverse_complement(seq)
        return seq

    def __retrieve_cds_coordinate(self) -> List[Dict[str, int]]:
        cds_coord = [{"start": cds.start, "end": cds.end} for cds in self.cds]
        return cds_coord

    def get_protein_sequence(self, session: Session) -> str:
        dna_seq = ""
        cds_coords = self.__retrieve_cds_coordinate()
        for coord in cds_coords:
            length_cds = coord["end"] - coord["start"] + 1
            dna_seq += retrieve_sequence(
                session, self.seq_region.dna.sequence, coord["start"], length_cds
            )
        if self.strand == -1:
            dna_seq = reverse_complement(dna_seq)
        seq = Seq(dna_seq).translate()
        if seq.endswith("*"):
            seq = seq[:-1]
        return seq


class Transcript(Base):
    __tablename__ = "transcript"

    id = Column(Integer, primary_key=True)
    name = Column(String(30), nullable=False)
    biotype = Column(String(20), nullable=False)
    description = Column(String, nullable=True)
    strand = Column(Integer)
    start = Column(Integer, nullable=False)
    end = Column(Integer, nullable=False)
    source = Column(String, nullable=False)

    assembly_id = Column(Integer, ForeignKey("genome.id"))
    seq_region_id = Column(Integer, ForeignKey("seq_region.id"))
    gene_id = Column(Integer, ForeignKey("gene.id"))

    assembly = relationship("Genome", foreign_keys=assembly_id)
    seq_region = relationship("SeqRegion", foreign_keys=seq_region_id)
    gene = relationship("Gene", back_populates="transcript", foreign_keys=gene_id)
    exons = relationship("Exon", back_populates="transcript")
    cds = relationship("Cds", back_populates="transcript")

    def __retrieve_exons_coordinate(self) -> List[Dict[str, str]]:
        exons_coord = [{"start": exon.start, "end": exon.end} for exon in self.exons]
        return exons_coord

    def get_sequence(self, session: Session) -> str:
        """Retrieve transcript sequence using exons start/end position and return it,
        depend on a sqlalchemy Session object to retrieve sequence efficiently using SQL

        Args:
            session (Session): A sqlalchemy Session object to access the db

        Returns:
            str: A transcript dna sequence
        """
        seq = ""
        exons_coord = self.__retrieve_exons_coordinate()
        for coord in exons_coord:
            length_exon = coord["end"] - coord["start"] + 1
            seq += retrieve_sequence(
                session, self.seq_region.dna.sequence, coord["start"], length_exon
            )
        if self.strand == -1:
            seq = reverse_complement(seq)
        return seq


class Exon(Base):
    __tablename__ = "exon"

    id = Column(Integer, primary_key=True)
    name = Column(String(30), nullable=False)
    strand = Column(Integer)
    start = Column(Integer, nullable=False)
    end = Column(Integer, nullable=False)
    constitutive = Column(Integer, nullable=False)
    rank = Column(Integer, nullable=False)
    phase = Column(Integer, nullable=False)
    end_phase = Column(Integer, nullable=False)
    source = Column(String, nullable=False)

    assembly_id = Column(Integer, ForeignKey("genome.id"))
    seq_region_id = Column(Integer, ForeignKey("seq_region.id"))
    gene_id = Column(Integer, ForeignKey("gene.id"))
    transcript_id = Column(Integer, ForeignKey("transcript.id"))

    assembly = relationship("Genome", foreign_keys=assembly_id)
    seq_region = relationship("SeqRegion", foreign_keys=seq_region_id)
    gene = relationship("Gene", back_populates="exon", foreign_keys=gene_id)
    transcript = relationship(
        "Transcript", back_populates="exons", foreign_keys=transcript_id
    )


class Cds(Base):
    __tablename__ = "cds"

    id = Column(Integer, primary_key=True)
    name = Column(String(30), nullable=False)
    strand = Column(Integer)
    start = Column(Integer, nullable=False)
    end = Column(Integer, nullable=False)
    source = Column(String, nullable=False)
    phase = Column(Integer, nullable=False)

    assembly_id = Column(Integer, ForeignKey("genome.id"))
    seq_region_id = Column(Integer, ForeignKey("seq_region.id"))
    gene_id = Column(Integer, ForeignKey("gene.id"))
    transcript_id = Column(Integer, ForeignKey("transcript.id"))

    assembly = relationship("Genome", foreign_keys=assembly_id)
    seq_region = relationship("SeqRegion", foreign_keys=seq_region_id)
    gene = relationship("Gene", back_populates="cds", foreign_keys=gene_id)
    transcript = relationship(
        "Transcript", back_populates="cds", foreign_keys=transcript_id
    )


class Variant(Base):
    __tablename__ = "variant"

    id = Column(Integer, primary_key=True)
    strand = Column(Integer)
    start = Column(Integer, nullable=False)
    end = Column(Integer, nullable=False)
    source = Column(String(50), nullable=False)
    alleles = Column(String(400), nullable=False)
    consequence_type = Column(String(200))
    var_class = Column(String(20))
    ambiguity = Column(String(5))
    # clinical_significance = Column(String)

    assembly_id = Column(Integer, ForeignKey("genome.id"))
    seq_region_id = Column(Integer, ForeignKey("seq_region.id"))

    assembly = relationship("Genome", foreign_keys=assembly_id)
    seq_region = relationship("SeqRegion", foreign_keys=seq_region_id)
    populations = relationship("VariantPopulation", back_populates="variant")


class VariantPopulation(Base):
    __tablename__ = "variant_population"

    id = Column(Integer, primary_key=True)
    allele = Column(String, nullable=False)
    allele_count = Column(Integer, nullable=False)
    frequency = Column(Float, nullable=False)
    population = Column(String)

    variant_id = Column(Integer, ForeignKey("variant.id"), nullable=False)

    variant = relationship(
        "Variant", back_populates="populations", foreign_keys=variant_id
    )


class GeneOntology(Base):
    __tablename__ = "gene_ontology"

    id = Column(Integer, primary_key=True)
    type = Column(String(1), nullable=False)
    term = Column(String(10), unique=True)
    annotation = Column(String)

    genes = relationship("Gene", secondary="gene_go_ref", back_populates="go")


class GeneGoRef(Base):
    __tablename__ = "gene_go_ref"

    gene_id = Column(Integer, ForeignKey("gene.id"), primary_key=True)
    gene_ontology_id = Column(Integer, ForeignKey("gene_ontology.id"), primary_key=True)
    assign_by = Column(String(20))


class DomainAnnotation(Base):
    __tablename__ = "domain_annotation"

    id = Column(Integer, primary_key=True)
    db_name = Column(String(15), nullable=False)
    accession = Column(String, nullable=False, unique=True)
    description = Column(String)
    genes = relationship(
        "Gene",
        secondary="gene_domain_annotation_ref",
        back_populates="domains_annotation",
    )


class GeneDomainAnnotationRef(Base):
    __tablename__ = "gene_domain_annotation_ref"

    gene_id = Column(Integer, ForeignKey("gene.id"), primary_key=True)
    domain_annotation_id = Column(
        Integer, ForeignKey("domain_annotation.id"), primary_key=True
    )


class Domain(Base):
    __tablename__ = "domain"

    id = Column(Integer, primary_key=True)
    start = Column(Integer, nullable=False)
    end = Column(Integer, nullable=False)

    annotation_id = Column(Integer, ForeignKey("domain_annotation.id"))
    gene_id = Column(Integer, ForeignKey("gene.id"), nullable=False)

    annotation = relationship("DomainAnnotation", foreign_keys=annotation_id)
    gene = relationship("Gene", foreign_keys=gene_id, back_populates="domains")


class SraExperiment(Base):
    __tablename__ = "sra_experiment"

    id = Column(Integer, primary_key=True)
    run = Column(String(12), nullable=False)
    release_date = Column(DateTime, nullable=False)
    load_date = Column(DateTime, nullable=False)
    average_length = Column(Integer, nullable=False)
    dl_path = Column(String(100), nullable=False)
    library_name = Column(String)
    library_selection = Column(String, nullable=False)
    library_layout = Column(
        Enum("SINGLE", "PAIRED", name="library_layout"), nullable=False
    )
    platform = Column(String(20), nullable=False)
    model = Column(String(30), nullable=False)
    project = Column(Integer, nullable=False)
    sample_name = Column(String, nullable=False)
    center_name = Column(String, nullable=False)
    species_name = Column(String, nullable=False)
    study_id = Column(Integer, ForeignKey("sra_study.id"), nullable=False)
    study = relationship(
        "SraStudy", foreign_keys=study_id, back_populates="experiments"
    )


class SraStudy(Base):
    __tablename__ = "sra_study"

    id = Column(Integer, primary_key=True)
    accession = Column(String(10), nullable=False)
    title = Column(String(300))
    abstract = Column(String(5000))
    experiments = relationship("SraExperiment", back_populates="study")

    def __str__(self):
        return (
            f"id={self.id}\naccession={self.accession}\ntitle={self.title}\n"
            f"abstract={self.abstract}"
        )


class Kegg(Base):
    __tablename__ = "kegg"

    id = Column(Integer, primary_key=True)
    accession = Column(String(10), nullable=False)
    description = Column(String(100))
    genes = relationship("Gene", back_populates="kegg")


class ProcessQueue(Base):
    __tablename__ = "process_queue"

    id = Column(Integer, primary_key=True)
    # Should be Enum, but I have bug with them: only take
    # "blastn" | "blastx" | "blastp"
    worker_type = Column(String(20))
    parameter = Column(UnicodeText)
