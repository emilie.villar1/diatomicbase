import io
from csv import DictReader
from typing import List, Optional, Dict
from copy import deepcopy
from fastapi import APIRouter, HTTPException
from sqlalchemy.orm import joinedload
from fastapi_sqlalchemy import db
from fastapi.responses import StreamingResponse
from app import (
    RNA_SEQ_COMPARISON_TABLE,
    RNA_SEQ_DIFF_EXPRESSION_OUTPUT,
    RNA_SEQ_DOB_TABLE,
    GENE_TO_MODULEPHAEONET,
    MODULEPHAEONET_TO_KEGG,
    PHAEONET_DATA_TSV,
)
from app.db import DATABASE_URL
from app.db.models import Gene, Cds, Variant
from app.api.models import GeneInfoDetailled, OneSearchGene, GeneExpression
from app.api.utils import format_gene_response, get_dict_from_list_of_objects

router = APIRouter()


@router.get("/genes_name", response_model=List[str])
def get_genes_name():
    genes_name = db.session.query(Gene.name).all()
    genes_name = [tuple_gene_name[0] for tuple_gene_name in genes_name]
    return genes_name


@router.get("/{gene_name}", response_model=GeneInfoDetailled)
def get_gene_info_by_name(gene_name: str = ""):
    gene: Gene = (
        db.session.query(Gene)
        .filter(gene_name == Gene.name)
        .options(
            joinedload(Gene.seq_region),
            joinedload(Gene.domains),
            joinedload(Gene.go),
            joinedload(Gene.domains_annotation),
            joinedload(Gene.kegg),
        )
        .first()
    )
    response_object = deepcopy(gene.__dict__)
    seq_region = deepcopy(gene.seq_region.__dict__)
    list_go = get_dict_from_list_of_objects(gene.go)
    list_domains = get_dict_from_list_of_objects(gene.domains)
    list_domains_annotation = get_dict_from_list_of_objects(gene.domains_annotation)
    del response_object["_sa_instance_state"]
    del seq_region["_sa_instance_state"]
    response_object["seq_region"] = seq_region
    response_object["go"] = list_go
    response_object["domains"] = list_domains
    response_object["domains_annotation"] = list_domains_annotation
    response_object["species"] = gene.assembly.species
    response_object["NCBI"] = gene.ncbi_display_id
    del response_object["ncbi_display_id"]
    if gene.kegg_id:
        response_object["KEGG"] = gene.kegg.accession
    return response_object


@router.get("/download/{format}/{gene_name}")
def download_gene(format: str, gene_name: str, type_seq: Optional[str] = "gene"):
    gene_name = gene_name.replace(".fa", "").replace(".vcf", "")
    gene: Gene = db.session.query(Gene).filter(gene_name == Gene.name).first()
    if format == "fasta":
        if type_seq == "gene":
            fasta_header = create_gene_fasta_header(gene)
            seq = gene.get_sequence(db.session)
            fasta = f"{fasta_header}\n{seq}"
        elif type_seq == "protein":
            fasta_header = create_protein_fasta_header(gene)
            seq = gene.get_protein_sequence(db.session)
            fasta = f"{fasta_header}\n{seq}"
        else:
            return {"type_seq": "not supported"}

        file_in_memory = io.BytesIO(fasta.encode())
        return StreamingResponse(
            file_in_memory,
            media_type="application/octet-stream",
        )
    elif format == "vcf":
        vcf_in_memory = get_vcf(gene)
        return StreamingResponse(vcf_in_memory, media_type="application/octet-stream")
    else:
        raise HTTPException(status_code=400, detail=f"format: {format} not supported")


@router.get("/expression/{gene_name}", response_model=List[GeneExpression])
def get_gene_expression(gene_name: str):
    to_return = []
    diff_expression_reader = DictReader(
        open(RNA_SEQ_DIFF_EXPRESSION_OUTPUT, "r"), delimiter="\t"
    )
    # Get the row corresponding at the gene_name given.
    # Contain all comp_id and status associated
    for row in diff_expression_reader:
        if row["GeneID"] != gene_name:
            continue
        comps_id = list(row.keys())
        comps_id.remove("GeneID")
        # for each comparison 2 colums is present in diff_expression_output
        # one for the log2FoldChange, one for the pvalue.
        # This keep only the comparison name
        comps_id = list(
            set(
                [
                    comp_id.replace("_log2FC", "").replace("_padj", "")
                    for comp_id in comps_id
                ]
            )
        )
        # For each comparison
        for comp_id in comps_id:
            # Retrieve project id, subsets name, and keyword
            project_id, subset1, subset2, keyword = get_comparison_table_info(comp_id)
            # Retrieve project name entierely and its description
            bioproject, description, publication, doi = get_dob_info(project_id)
            log2FC, pvalue = get_log2FC_and_pvalue(comp_id, row)
            # Put all info about the comparison id in one dict and append it to to_return
            experience_dict = {}
            experience_dict["comp_id"] = comp_id
            experience_dict["log2FC"] = log2FC
            experience_dict["pvalue"] = pvalue
            experience_dict["status"] = determine_DEG_status(comp_id, row)
            experience_dict["keyword"] = keyword
            experience_dict["comp"] = f"{subset1} vs {subset2}"
            experience_dict["bioproject"] = bioproject
            experience_dict["description"] = description
            experience_dict["publication"] = publication
            experience_dict["doi"] = doi
            to_return.append(experience_dict)
        return to_return

    raise HTTPException(status_code=404, detail="Gene name not found in the table")


@router.get("/expression_data/{gene_name}")
def get_gene_expression_data(gene_name):
    gene_name = gene_name.replace("_expression_data.tsv", "")
    diff_expression_lines = open(RNA_SEQ_DIFF_EXPRESSION_OUTPUT, "r").readlines()
    header = diff_expression_lines[0]
    for line in diff_expression_lines:
        if not line.startswith(gene_name):
            continue
        output_file = f"{header}{line}"
        return StreamingResponse(
            io.BytesIO(output_file.encode()), media_type="application/octet-stream"
        )
    raise HTTPException(status_code=404, detail="Gene name not found in the table")


def get_log2FC_and_pvalue(comp_id: str, gene_row: Dict[str, str]) -> (float, float):
    comp_id_log2FC = comp_id + "_log2FC"
    comp_id_pvalue = comp_id + "_padj"
    log2FC = gene_row[comp_id_log2FC]
    pvalue = gene_row[comp_id_pvalue]
    return log2FC, pvalue


def determine_DEG_status(comp_id: str, gene_row: Dict[str, str]) -> str:
    log2FC, pvalue = get_log2FC_and_pvalue(comp_id, gene_row)
    if pvalue == "NA" or log2FC == "NA":
        return "NA"
    elif float(log2FC) > 1 and float(pvalue) < 0.05:
        return "UP"
    elif float(log2FC) < -1 and float(pvalue) < 0.05:
        return "DOWN"
    else:
        return "NS"


@router.get("/coexpression/{gene_name}")
def get_cooexpression_network(gene_name: str):
    tsv = open_tsv(GENE_TO_MODULEPHAEONET)
    for row in tsv:
        if row["Phaeodactylum gene model"] != gene_name:
            continue
        kegg_associated = get_kegg_for_pheonet(row["PhaeoNet Module"])
        return {"color": row["PhaeoNet Module"], "kegg": kegg_associated}
    # Some gene doesn't appear in the phaeonet file
    return {"color": "#N/D", "kegg": []}


def get_kegg_for_pheonet(module_name: str) -> List[str]:
    tsv = open_tsv(MODULEPHAEONET_TO_KEGG)
    kegg_associated = []
    for row in tsv:
        if row["modules"] != module_name:
            continue
        kegg_associated.append(row["KEGG"])
    return kegg_associated


def open_tsv(tsv: str) -> List[Dict]:
    try:
        content = DictReader(open(tsv, "r"), delimiter="\t")
        return content
    except FileNotFoundError as e:
        raise FileNotFoundError(
            f"{e}\n{tsv} not found, Is it correctly defined in init.py ?"
        )


def get_dob_info(project_id: int) -> List[str]:
    """Retrieve information associated with the project_id given in DOB table.
    return the Bioproject accession and the description link to the project id.

    Args:
        project_id (int): ID from Project_ID_DOB in DOB table and Project_ID in comparison table

    Returns:
        List[str]: Return Bioproject Accession, Description linked, Publication resume, and doi with the project_id
    """
    reader = DictReader(open(RNA_SEQ_DOB_TABLE, "r"), delimiter="\t")
    for row in reader:
        if row["project_id"] != project_id:
            continue
        return (
            row["bioproject_accession"],
            row["description"],
            row["publication_resume"],
            row["doi"],
        )


def get_comparison_table_info(comparison_id: str) -> List[str]:
    """Retrieve information associated with the comparison_id given in comparison table.
    Return the Project_ID, subset 1 and 2 title and keyword associated

    Args:
        comparison_id (str): ID in column 'Comp_ID' in comparision_table.tsv

    Returns:
        List[str]: List of 4 element, Project_ID, subset1, subset2, keyword
    """
    reader = DictReader(open(RNA_SEQ_COMPARISON_TABLE, "r"), delimiter="\t")
    for row in reader:
        if row["comp_id"] != comparison_id:
            continue
        return [row["project_id"], row["subset_1"], row["subset_2"], row["keyword"]]
    raise HTTPException(
        status_code=500, detail=f"{comparison_id} not found in comparison_table"
    )


def get_vcf(gene: Gene):
    start, end = gene.start, gene.end
    variants: List[Variant] = (
        db.session.query(Variant)
        .filter(
            Variant.seq_region_id == gene.seq_region_id,
            Variant.start >= start,
            Variant.end <= end,
        )
        .options(joinedload(Variant.seq_region))
        .all()
    )
    vcf = ""
    for variant in variants:
        chrom = variant.seq_region.name
        start = variant.start
        origin = variant.alleles.split("/")[0]
        mut = variant.alleles.split("/")[1]
        variant_id = f"tmp_{variant.seq_region.name}_{variant.start}_{origin}_{mut}"
        vcf += (
            f"{chrom}\t{variant.start}\t{variant_id}\t{origin}\t{mut}\t.\t.\tTSA=SNV\n"
        )
    return io.BytesIO(vcf.encode())


def create_gene_fasta_header(gene: Gene) -> str:
    """From a gene, create a fasta header with the gene name,
    chromosome name and strand.

    Args:
        gene (Gene): A Gene instance

    Returns:
        str: A fasta header folowing this model:
            '>{name} seq={chrom_name} strand={strand}'
    """
    name = gene.name
    chrom_name = gene.seq_region.name
    strand = "+" if gene.strand == 1 else "-"
    return f">{name} seq={chrom_name} strand={strand}"


def create_protein_fasta_header(gene: Gene) -> str:
    """From a gene, create a fasta header with the protein name, gene name,
    chromosome name and strand.

    Args:
        gene (Gene): A Gene instance

    Returns:
        str: A fasta header folowing this model:
            '>{name} gene={gene_name} seq={chrom_name} strand={strand}'
    """
    if gene.uniprotkb:
        name = gene.uniprotkb
    else:
        name = gene.cds[0].name
    gene_name = gene.name
    chrom_name = gene.seq_region.name
    strand = "+" if gene.strand == 1 else "-"
    return f">{name} gene={gene_name} seq={chrom_name} strand={strand}"


@router.get("/phaeonet/{gene_name}")
def get_phaeonet(gene_name: str):

    phaeonet = open_tsv(PHAEONET_DATA_TSV)
    row = get_corresponding_row(phaeonet, "DOB_ID", gene_name)
    if not row:
        raise HTTPException(404)
    polycomb = False if row["polycomb_marked_gene"] == "No" else True
    intron_retention = False if row["intron_retention"] == "No" else True
    exon_skipping = False if row["exon_skipping"] == "No" else True

    return {
        "polycomb": polycomb,
        "intron_retention": intron_retention,
        "exon_skipping": exon_skipping,
        "asafind": row["ASAFIND"],
        "hectar": row["HECTAR"],
        "mitofates": row["MITOFATES"],
        "wsort_animal": row["wolfpsort_animal"],
        "wsort_plant": row["wolfpsort_plant"],
        "wsort_fungi": row["wolfpsort_fungi"],
        "wsort_consensus": row["wolfpsort_consensus"],
        "OP_lineage": row["ochrophytes_and_plastid_lineage"],
        "OP_subgroup": row["ochrophytes_and_plastid_subgroup"],
        "OP_nbtophit": row["ochrophytes_and_plastid_nbtophit"],
        "OP_localdiversity": row["ochrophytes_and_plastid_localdiversity"],
        "SC_lineage": row["SAR_and_CCTH_lineage"],
        "SC_subgroup": row["SAR_and_CCTH_subgroup"],
        "SC_nbtophit": row["SAR_and_CCTH_nbtophit"],
        "SC_localdiversity": row["SAR_and_CCTH_localdiversity"],
    }


def get_corresponding_row(dict_reader: List[Dict], row_name: str, gene_name: str):
    for row in dict_reader:
        if row[row_name] == gene_name:
            return row
