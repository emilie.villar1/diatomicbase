import os
from csv import DictReader
from typing import List, Dict
from fastapi_sqlalchemy import db
from fastapi import APIRouter, HTTPException
from fastapi.responses import FileResponse
from app import (
    RNA_SEQ_DOB_TABLE,
    RNA_SEQ_RUNS_GROUP_TABLE,
    RNA_SEQ_COMPARISON_TABLE,
    RNA_SEQ_WORK_DIR,
)
from app.api.models import BioProjectMetaData, BioSamplesDragAndDrop, DlTranscriptomics
from app.db.models import SraExperiment

router = APIRouter()


@router.get("/", response_model=List[BioProjectMetaData])
def rna_seq_dob():
    table = open_tsv(RNA_SEQ_DOB_TABLE)
    to_return_rna_seq_data = []
    biosample_index = 0
    for row in table:
        biosamples = get_associated_biosamples(row["project_id"], biosample_index)
        # Get the last index used. Those id are needed by react-sortablejs
        if len(biosamples) > 0:
            biosample_index = biosamples[-1]["id"] + 1
        rna_data = {
            "BioProject": row["bioproject_accession"],
            "Description": row["description"],
            "Nb_runs": row["nb_runs"],
            "Nb_replicates": row["nb_replicates"],
            "Design": row["design"],
            "Publication": row["publication_resume"],
            "Keywords": row["keywords"],
            "doi": row["doi"],
            "Strain": row["strain"],
            "BioSamples": biosamples,
        }
        to_return_rna_seq_data.append(rna_data)
    return to_return_rna_seq_data


@router.get("/dl_page/{bioproject}", response_model=DlTranscriptomics)
def bioproject_dl_info(bioproject: str):
    table = open_tsv(RNA_SEQ_DOB_TABLE)
    bioproject_dob = {}
    for row in table:
        if row["bioproject_accession"] == bioproject:
            bioproject_dob = row
            break
    if not bioproject_dob:
        raise HTTPException(
            status_code=404, detail=f"{bioproject} not found in RNA_SEQ_DOB_TABLE"
        )
    runs = get_runs_name_and_dl(row["project_id"])
    comparisons = get_comparison(row["project_id"])
    return {
        "bioproject": row["bioproject_accession"],
        "description": row["description"],
        "design": row["design"],
        "publication": row["publication_resume"],
        "doi": row["doi"],
        "runs": runs,
        "comparisons": comparisons,
    }


@router.get("/dl_comparison/{comparison}")
def dl_comparison(comparison: str):
    detailed_result = os.path.join(RNA_SEQ_WORK_DIR, "detailed_results")
    files = os.listdir(detailed_result)
    if comparison in files:
        return FileResponse(os.path.join(detailed_result, comparison))
    else:
        raise HTTPException(404)


def get_runs_name_and_dl(project_id: int):
    table = open_tsv(RNA_SEQ_RUNS_GROUP_TABLE)
    runs_to_return = []
    for row in table:
        if row["project_id"] != project_id:
            continue
        if "add_in_dob" in list(row.keys()) and row["add_in_dob"] == "no":
            continue
        dl_path = get_dl_path_from_run_acc(row["run"])
        runs_to_return.append({"name": row["run"], "dl": dl_path})
    return runs_to_return


def get_dl_path_from_run_acc(run: str) -> str:
    dl_path = (
        db.session.query(SraExperiment.dl_path).filter(SraExperiment.run == run).first()
    )
    return dl_path[0]


def get_comparison(project_id: int):
    table = open_tsv(RNA_SEQ_COMPARISON_TABLE)
    comparisons = [
        {
            "id": row["comp_id"],
            "subset1": row["subset_1"],
            "subset2": row["subset_2"],
        }
        for row in table
        if row["project_id"] == project_id
    ]
    return comparisons


@router.get("/name", response_model=List[str])
def get_bioproject_name():
    table = open_tsv(RNA_SEQ_DOB_TABLE)
    bioproject_names = [row["bioproject_accession"] for row in table]
    return bioproject_names


def get_associated_biosamples(
    project_id: int, biosample_index: int
) -> List[BioSamplesDragAndDrop]:
    table = open_tsv(RNA_SEQ_RUNS_GROUP_TABLE)
    biosample_to_return = []
    for row in table:
        if row["project_id"] != project_id:
            continue
        if "add_in_dob" in list(row.keys()) and row["add_in_dob"] == "no":
            continue
        biosample_to_return.append({"id": biosample_index, "name": row["sample_name"]})
        biosample_index += 1
    return biosample_to_return


def open_tsv(rna_seq_dob_path: str) -> List[Dict]:
    try:
        content = DictReader(open(rna_seq_dob_path, "r"), delimiter="\t")
        return content
    except FileNotFoundError as e:
        raise FileNotFoundError(
            f"{e}\nRNA_SEQ_COMPARISON_TABLE not found, Is it correctly defined in init.py ?"
        )
