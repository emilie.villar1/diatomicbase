import os
import re
import json
from typing import List, Dict, Tuple
from Bio import SearchIO
from fastapi import APIRouter, HTTPException
from fastapi_sqlalchemy import db
from sqlalchemy import or_, desc
from sqlalchemy.orm import joinedload
from app import BLAST_WORK_DIR
from app.db.models import Gene, GeneOntology, DomainAnnotation, Kegg, ProcessQueue
from app.api.utils import format_gene_response
from app.api.models import OneSearchGene
from app.api.utils import format_gene_response
from sqlalchemy import func


router = APIRouter()


@router.get("/searchbar/{term}")
def search_bar(term: str):
    if term == "":
        raise HTTPException(404)
    term = decode_slash(term.strip())
    find_by = "gene"
    genes = []
    # Exact (but caseinsensitive) search through gene fields
    # (name, eco, embl, uniprotkbm ncbi, search description)
    genes: List[Gene] = (
        db.session.query(Gene)
        .filter(
            or_(
                Gene.name.ilike(f"%{term}%"),
                Gene.eco.ilike(f"%{term}%"),
                Gene.embl.ilike(f"%{term}%"),
                Gene.uniprotkb.ilike(f"%{term}%"),
                Gene.ncbi_display_id.ilike(f"%{term}%"),
                Gene.search_description.ilike(f"%{term}%"),
            )
        )
        .all()
    )
    # Fuzzy Search for gene.name
    if not genes:
        genes = (
            db.session.query(Gene)
            .filter(func.similarity(Gene.name, term) > 0.7)
            .order_by(func.similarity(Gene.name, term))
            .all()
        )
    # Fuzzy search for gene.ncbi
    if not genes:
        genes = (
            db.session.query(Gene)
            .filter(func.similarity(Gene.ncbi_display_id, term) > 0.6)
            .order_by(desc(func.similarity(Gene.ncbi_display_id, term)))
            .all()
        )
    # Fuzzy search in search_description
    if not genes:
        genes = (
            db.session.query(Gene)
            .filter(func.word_similarity(Gene.search_description, term) > 0.3)
            .order_by(func.word_similarity(Gene.search_description, term))
            .all()
        )
    # Ilike search in GO.term and annotation
    if not genes:
        gos = (
            db.session.query(GeneOntology)
            .filter(
                or_(
                    GeneOntology.term.ilike(f"%{term}%"),
                    GeneOntology.annotation.ilike(f"%{term}%"),
                )
            )
            .all()
        )
        if gos:
            find_by = "GO"
        genes = list(set([gene for go in gos for gene in go.genes]))
    # Fuzzy search in GO.term and annotation
    if not genes:
        gos = (
            db.session.query(GeneOntology)
            .filter(
                or_(
                    func.similarity(GeneOntology.term, term) > 0.7,
                    func.word_similarity(GeneOntology.annotation, term) > 0.2,
                )
            )
            .all()
        )
        if gos:
            find_by = "GO"
        genes = list(set([gene for go in gos for gene in go.genes]))
    # Fuzzy search in KEGG.accession and description
    if not genes:
        keggs = (
            db.session.query(Kegg)
            .filter(
                or_(
                    func.similarity(Kegg.accession, term) > 0.5,
                    func.word_similarity(Kegg.description, term) > 0.4,
                )
            )
            .all()
        )
        if keggs:
            find_by = "KEGG"
        genes = list(set([gene for kegg in keggs for gene in kegg.genes]))

    # Fuzzy search in domainAnnotation (INTERPRO, and INTERPRO sub db)
    if not genes:
        domains = (
            db.session.query(DomainAnnotation)
            .filter(
                or_(
                    func.similarity(DomainAnnotation.accession, term) > 0.7,
                    func.similarity(DomainAnnotation.description, term) > 0.6,
                )
            )
            .all()
        )
        if domains:
            find_by = "INTERPRO"
        genes = list(set([gene for domain in domains for gene in domain.genes]))

    if find_by == "GO":
        response_object = format_gene_response(genes, ["go"])
    elif find_by == "KEGG":
        response_object = format_gene_response(genes, ["kegg"])
    elif find_by == "INTERPRO":
        response_object = format_gene_response(genes, ["domain_annotation", "interpro"])
    else:
        response_object = format_gene_response(genes)
    return [find_by, response_object]


@router.get("/gene/{gene_name}", response_model=List[OneSearchGene])
def search_gene(gene_name: str = "all"):
    gene_name = decode_slash(gene_name.strip())
    genes: List[Gene] = (
        db.session.query(Gene)
        .filter(
            or_(
                Gene.name.ilike(f"%{gene_name}%"),
                Gene.ncbi_display_id.ilike(f"%{gene_name}%"),
            )
        )
        .options(joinedload(Gene.seq_region), joinedload(Gene.assembly))
        .all()
    )
    if not genes:
        genes = []
        genes = (
            db.session.query(Gene)
            .filter(func.similarity(Gene.name, gene_name) > 0.7)
            .order_by(desc(func.similarity(Gene.name, gene_name)))
            .options(joinedload(Gene.seq_region), joinedload(Gene.assembly))
            .all()
        )
        genes_ncbi = (
            db.session.query(Gene)
            .filter(func.similarity(Gene.ncbi_display_id, gene_name) > 0.6)
            .order_by(desc(func.similarity(Gene.ncbi_display_id, gene_name)))
            .options(joinedload(Gene.seq_region), joinedload(Gene.assembly))
            .all()
        )
        genes += genes_ncbi
        genes = list(set(genes))
    # Obtain the a dict of the value of the gene, deleting the sqlalchemy internal variable
    response_object = format_gene_response(genes)

    return response_object


@router.get("/keyword/{keyword}")
def search_keyword(keyword: str):
    keyword = decode_slash(keyword.strip())
    genes: List[Gene] = (
        db.session.query(Gene)
        .filter(Gene.description.ilike(f"%{keyword}%"))
        .options(joinedload(Gene.seq_region), joinedload(Gene.assembly))
        .all()
    )

    if not genes:
        genes: List[Gene] = (
            db.session.query(Gene)
            .filter(func.word_similarity(Gene.search_description, keyword) > 0.3)
            .options(joinedload(Gene.seq_region), joinedload(Gene.assembly))
            .all()
        )

    # Obtain the a dict of the value of the gene, deleting the sqlalchemy internal variable
    response_object = format_gene_response(genes)

    return response_object


@router.get("/go/{go_term}")
def search_go(go_term: str):
    go_term = decode_slash(go_term.strip())
    gos: List[GeneOntology] = (
        db.session.query(GeneOntology)
        .filter(GeneOntology.term.ilike(f"%{go_term}%"))
        .all()
    )
    if not gos:
        gos = (
            db.session.query(GeneOntology)
            .filter(func.similarity(GeneOntology.term, go_term) > 0.7)
            .order_by(desc(func.similarity(GeneOntology.term, go_term)))
            .all()
        )
    if not gos:
        gos = (
            db.session.query(GeneOntology)
            .filter(func.word_similarity(GeneOntology.annotation, go_term) > 0.2)
            .order_by(desc(func.word_similarity(GeneOntology.annotation, go_term)))
            .all()
        )
    genes = list(set([gene for go in gos for gene in go.genes]))
    response_object = format_gene_response(genes, ["go"])
    return response_object


@router.get("/interpro/{interpro_term}")
def search_interpro(interpro_term: str):
    interpro_term = decode_slash(interpro_term.strip())
    interpro: List[DomainAnnotation] = (
        db.session.query(DomainAnnotation)
        .filter(
            DomainAnnotation.db_name == "InterPro",
            DomainAnnotation.accession.ilike(f"%{interpro_term}%"),
        )
        .all()
    )

    if not interpro:
        interpro = (
            db.session.query(DomainAnnotation)
            .filter(
                DomainAnnotation.db_name == "InterPro",
                func.similarity(DomainAnnotation.accession, interpro_term) > 0.7,
            )
            .order_by(desc(func.similarity(DomainAnnotation.accession, interpro_term)))
            .all()
        )
    if not interpro:
        interpro = (
            db.session.query(DomainAnnotation)
            .filter(
                DomainAnnotation.db_name == "InterPro",
                func.similarity(DomainAnnotation.description, interpro_term) > 0.6,
            )
            .order_by(
                desc(func.similarity(DomainAnnotation.description, interpro_term) > 0.6)
            )
            .all()
        )
    genes = list(set([gene for it in interpro for gene in it.genes]))

    response_object = format_gene_response(genes, ["domain_annotation", "interpro"])
    return response_object


@router.get("/kegg/{kegg_term}")
def search_kegg(kegg_term: str):
    kegg_term = decode_slash(kegg_term.strip())
    keggs: List[Kegg] = (
        db.session.query(Kegg).filter(Kegg.accession.ilike(f"%{kegg_term}%")).all()
    )

    if not keggs:
        keggs = (
            db.session.query(Kegg)
            .filter(func.similarity(Kegg.accession, kegg_term) > 0.5)
            .order_by(desc(func.similarity(Kegg.accession, kegg_term)))
            .all()
        )
    if not keggs:
        keggs = (
            db.session.query(Kegg)
            .filter(func.word_similarity(Kegg.description, kegg_term) > 0.4)
            .order_by(desc(func.word_similarity(Kegg.description, kegg_term)))
            .all()
        )
    genes = list(set([gene for kegg in keggs for gene in kegg.genes]))

    response_object = format_gene_response(genes, ["kegg"])
    return response_object


def decode_slash(term: str) -> str:
    """Ugly solution to issue #34
    decode the slash manually encode in the search term

    Args:
        term (str): searchTerm to decode

    Returns:
        str: searchTerm with slash if present
    """
    return term.replace("1be24f945cde", "/")


@router.post("/blast/")
def post_blast(blast_info: Dict):
    seq = blast_info["seq"].replace("\n", "\\n")
    blast = ProcessQueue(
        worker_type=blast_info["blast_type"],
        parameter=f'{{"seq": "{seq}"}}',
    )
    db.session.add(blast)
    db.session.commit()
    return {"blast_id": blast.id}


@router.get("/blast/{blast_id}")
def get_blast(blast_id: int):
    result_path = os.path.join(BLAST_WORK_DIR, "blast_output", f"{blast_id}.xml")
    if not os.path.exists(result_path):
        raise HTTPException(status_code=404, detail="run not processed")
    else:
        res = SearchIO.read(result_path, "blast-xml")
        hits = []
        for hit in res.hits:
            chrom_and_species = extract_chrom_species_from_description(hit.description)
            hsps = []
            for hsp in hit.hsps:
                hsps.append(
                    {
                        "evalue": hsp.evalue,
                        "gap_num": hsp.gap_num,
                        "ident_num": hsp.ident_num,
                        "pos_num": hsp.pos_num,
                        "bitscore": hsp.bitscore,
                        "hit_seq": str(hsp.hit.seq),
                        "query_seq": str(hsp.query.seq),
                    }
                )
            hits.append(
                {
                    "accession": hit.accession,
                    "description": get_gene_description(hit.accession),
                    "chrom": chrom_and_species[0],
                    "species": chrom_and_species[1].replace("_", " "),
                    "seq_len": hit.seq_len,
                    "hsps": hsps,
                }
            )
        delete_file(result_path)
        return {
            "blast_type": res.program,
            "blast_version": res.version,
            "blast_id": blast_id,
            "blast_accession": res.id,
            "hits": hits,
        }


def get_gene_description(gene_accession: str):
    description = (
        db.session.query(Gene.description).filter(Gene.name == gene_accession).first()
    )
    return description[0]


def extract_chrom_species_from_description(description: str):
    re_res = re.match("chrom=([^\s]*) species=([^\s]*)", description)
    if re_res:
        chrom = re_res.group(1)
        species = re_res.group(2)
        return [chrom, species]
    raise AttributeError(f"Chrom or Species wasn't found in {description}")


def delete_file(filename: str):
    try:
        os.remove(filename)
    except FileNotFoundError:
        pass
