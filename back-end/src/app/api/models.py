from enum import Enum
from datetime import date, datetime
from typing import Optional, Dict, List, Union
from pydantic import BaseModel


class SeqRegion(BaseModel):
    id: int
    dna_id: int
    name: str


class GeneInfo(BaseModel):
    id: int
    name: str
    start: int
    end: int
    strand: int
    biotype: str
    source: str
    assembly_id: int
    eco: Optional[str]
    embl: Optional[str]
    kegg_id: Optional[int]
    uniprotkb: Optional[str]
    description: Optional[str]
    ncbi_display_id: Optional[str]
    ncbi_internal_id: Optional[str]
    seq_region_id: int
    seq_region: SeqRegion


class Assembly(BaseModel):
    assembly_accession: str
    assembly_date: str
    assembly_name: str
    base_pairs: int
    genebuild_initial_release_date: str
    genebuild_last_geneset_update: str
    genebuild_method: str
    genebuild_start_date: str
    id: int
    species: str


class OneSearchGene(GeneInfo):
    assembly: Assembly


class GoTypeEnum(str, Enum):
    F = "F"
    C = "C"
    P = "P"


class GO(BaseModel):
    term: str
    type: GoTypeEnum
    annotation: str


class Domains(BaseModel):
    start: int
    end: int
    id: int
    annotation_id: int
    gene_id: int


class DomainsAnnotation(BaseModel):
    db_name: str
    description: Optional[str]
    id: int
    accession: str


class GeneInfoDetailled(GeneInfo):
    species: str
    go: List[GO]
    KEGG: Optional[str]
    NCBI: Optional[str]
    domains: List[Domains]
    domains_annotation: List[DomainsAnnotation]


class BioSamplesDragAndDrop(BaseModel):
    id: int
    name: str


class BioProjectMetaData(BaseModel):
    BioProject: str
    Description: str
    Nb_runs: str
    Nb_replicates: str
    Design: str
    Publication: str
    Keywords: str
    doi: str
    Strain: str
    BioSamples: List[BioSamplesDragAndDrop]


class JbrowseLocation(BaseModel):
    uri: str


class AssembliesAdapter(BaseModel):
    type: str
    fastaLocation: JbrowseLocation
    faiLocation: JbrowseLocation
    gziLocation: JbrowseLocation


class AssembliesSequence(BaseModel):
    type: str
    trackId: str
    adapter: AssembliesAdapter


class Assemblies(BaseModel):
    name: str
    sequence: AssembliesSequence


class TracksIndex(BaseModel):
    location: JbrowseLocation
    indexType: str = "TBI"


class DefaultTracksAdapter(BaseModel):
    type: str = "Gff3TabixAdapter"
    gffGzLocation: JbrowseLocation
    index: TracksIndex


class VCFTracksAdapter(BaseModel):
    type: str = "Gff3TabixAdapter"
    vcfGzLocation: JbrowseLocation
    index: TracksIndex


class WiggleAdapater(BaseModel):
    type: str = "BigWig"
    bigWigLocation: JbrowseLocation


class TracksRenderer(BaseModel):
    type: str = "SvgFeatureRenderer"


class JbrowseTrack(BaseModel):
    type: str = "BasicTrack"
    trackId: str = "ASM15095v2-genes"
    name: str = "Phaeodactylum tricornutum Ensembl genes"
    category: List[str] = ["Ensembl", "Genes"]
    assemblyNames: List[str] = ["ASM15095v2"]
    adapter: Union[DefaultTracksAdapter, VCFTracksAdapter, WiggleAdapater]
    renderer: Optional[TracksRenderer]


class GeneExpressionEnum(str, Enum):
    UP = "UP"
    DOWN = "DOWN"
    NA = "NA"
    NS = "NS"


class GeneExpression(BaseModel):
    comp: str
    comp_id: str
    keyword: str
    bioproject: str
    description: str
    status: GeneExpressionEnum
    pvalue: Union[float, str]
    log2FC: Union[float, str]
    publication: str
    doi: str


class TranscriptomicsRuns(BaseModel):
    name: str
    dl: str


class TranscriptomicsComparisons(BaseModel):
    id: str
    subset1: str
    subset2: str


class DlTranscriptomics(BaseModel):
    bioproject: str
    description: str
    design: str
    publication: str
    doi: str
    runs: List[TranscriptomicsRuns]
    comparisons: List[TranscriptomicsComparisons]
