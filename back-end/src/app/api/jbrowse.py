from typing import Dict, List
from collections import OrderedDict
from fastapi import APIRouter, HTTPException
from fastapi_sqlalchemy import db
from app import (
    SERVER_DATA_URL,
    RNA_SEQ_DIFF_EXPRESSION_OUTPUT,
    RNA_SEQ_RUNS_GROUP_TABLE,
)
from app.db.models import Genome
from app.api.models import Assemblies, JbrowseTrack
from app.api.gene import (
    get_comparison_table_info,
    get_dob_info,
    get_log2FC_and_pvalue,
    DictReader,
)

router = APIRouter()


@router.get("/assembly/{species}", response_model=Assemblies)
def assembly(species: str):
    genome = db.session.query(Genome).filter(Genome.species == species).first()
    assembly = get_assembly_by_name(genome.assembly_name, ASSEMBLIES)
    return assembly


@router.get("/tracks/{species}", response_model=List[JbrowseTrack])
def tracks(species: str):
    genome = db.session.query(Genome).filter(Genome.species == species).first()
    tracks = get_tracks_by_name(genome.assembly_name, TRACKS)
    return tracks


@router.get("/tracks/{species}/{gene_id}")
def tracks_gene(species: str, gene_id: str):
    genome: Genome = db.session.query(Genome).filter(Genome.species == species).first()
    common_tracks = get_tracks_by_name(genome.assembly_name, TRACKS)

    specific_tracks = []
    diff_expression_reader = DictReader(
        open(RNA_SEQ_DIFF_EXPRESSION_OUTPUT, "r"), delimiter="\t"
    )
    # Get the row corresponding at the gene_name given.
    # Contain all comp_id and status associated
    for row in diff_expression_reader:
        if row["GeneID"] != gene_id:
            continue
        gene_row = row

    comps_id = get_comps_id_from_gene_row(gene_row)
    # For each comparison
    for comp_id in comps_id:
        # Retrieve project id, subsets name, and keyword
        project_id, subset_1, subset_2, keyword = get_comparison_table_info(comp_id)
        log2FC, pvalue = get_log2FC_and_pvalue(comp_id, gene_row)
        if log2FC == "NA" or pvalue == "NA":
            continue
        if not significant(float(log2FC), float(pvalue)):
            continue
        bioproject, _, publication, _ = get_dob_info(project_id)
        runs = get_SRR_from_project_id_and_subset(project_id, subset_1, subset_2)
        runs = remove_runs_already_in_specific_tracks(runs, specific_tracks)
        for run in runs:
            specific_tracks.append(
                {
                    "type": "QuantitativeTrack",
                    "trackId": run["sample_name"],
                    "name": run["sample_name"],
                    "category": [
                        "RNA-seq",
                        keyword,
                        f"{bioproject} \t {publication}",
                    ],
                    "assemblyNames": [genome.assembly_name],
                    "adapter": {
                        "type": "BigWigAdapter",
                        "bigWigLocation": {
                            "uri": f"{SERVER_DATA_URL}/BigWig/{run['srr']}.bw"
                        },
                    },
                }
            )

    tracks = common_tracks + specific_tracks
    return tracks


def get_comps_id_from_gene_row(gene_row) -> List[str]:
    comps_id = list(gene_row.keys())
    comps_id.remove("GeneID")
    # For each comparison 2 colums is present in diff_expression_output
    # one for the log2FoldChange, one for the pvalue.
    # This keep only the comparison name
    comps_id = list(
        set(
            [
                comp_id.replace("_log2FC", "").replace("_padj", "")
                for comp_id in comps_id
            ]
        )
    )
    return comps_id


def significant(log2FC: float, pvalue: float) -> bool:
    return pvalue < 0.05 and (log2FC < -1 or log2FC > 1)


def get_SRR_from_project_id_and_subset(project_id: str, subset_1: str, subset_2: str):
    runs_group_reader = DictReader(open(RNA_SEQ_RUNS_GROUP_TABLE, "r"), delimiter="\t")
    srrs = []
    for row in runs_group_reader:
        if row["project_id"] != project_id:
            continue
        if row["group_name"] != subset_1 and row["group_name"] != subset_2:
            continue
        srrs.append({"srr": row["run"], "sample_name": row["sample_name"]})
    return OrderedDict((frozenset(item.items()), item) for item in srrs).values()


def remove_runs_already_in_specific_tracks(runs, specific_tracks):
    list_name_tracks = [track["name"] for track in specific_tracks]
    runs_without_duplicate = [
        run for run in runs if run["sample_name"] not in list_name_tracks
    ]
    return runs_without_duplicate


def get_assembly_by_name(
    assembly_name: str, assemblies: List[Assemblies]
) -> Assemblies:
    for assembly in assemblies:
        if assembly["name"] == assembly_name:
            return assembly
    raise HTTPException(
        status_code=404, detail=f"{assembly_name} not found in ASSEMBLIES"
    )


def get_tracks_by_name(
    assembly_name: str, tracks: Dict[str, List[JbrowseTrack]]
) -> List[JbrowseTrack]:
    if assembly_name in list(tracks.keys()):
        return tracks[assembly_name]
    raise HTTPException(status_code=404, detail=f"{assembly_name} not found in TRACKS")


ASSEMBLIES = [
    {
        "name": "ASM15095v2",
        "sequence": {
            "type": "ReferenceSequenceTrack",
            "trackId": "ASM15095v2-ReferenceSequenceTrack",
            "adapter": {
                "type": "BgzipFastaAdapter",
                "fastaLocation": {
                    "uri": f"{SERVER_DATA_URL}/Phaeodactylum_tricornutum.ASM15095v2.dna.toplevel.fa.gz",
                },
                "faiLocation": {
                    "uri": f"{SERVER_DATA_URL}/Phaeodactylum_tricornutum.ASM15095v2.dna.toplevel.fa.gz.fai",
                },
                "gziLocation": {
                    "uri": f"{SERVER_DATA_URL}/Phaeodactylum_tricornutum.ASM15095v2.dna.toplevel.fa.gz.gzi",
                },
            },
        },
        "refNameAliases": {
            "adapter": {
                "type": "RefNameAliasAdapter",
                "location": {
                    "uri": f"{SERVER_DATA_URL}/ASM15095v2.aliases.txt",
                },
            },
        },
    }
]

TRACKS = {
    "ASM15095v2": [
        {
            "type": "FeatureTrack",
            "trackId": "ASM15095v2-genes",
            "name": "Phatr3 gene models (Ensembl)",
            "category": ["  Genes"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "Gff3TabixAdapter",
                "gffGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/Phaeodactylum_tricornutum_sort.ASM15095v2.48.gff3.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/Phaeodactylum_tricornutum_sort.ASM15095v2.48.gff3.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
            "renderer": {
                "type": "SvgFeatureRenderer",
            },
        },
        {
            "type": "FeatureTrack",
            "trackId": "Phatr2_genes_filtered",
            "name": "Phatr2 gene models (JGI)",
            "category": ["  Genes"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "Gff3TabixAdapter",
                "gffGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/phatr2_gene_models_filtered_sorted.gff3.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/phatr2_gene_models_filtered_sorted.gff3.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
            "renderer": {
                "type": "SvgFeatureRenderer",
            },
        },
        {
            "type": "FeatureTrack",
            "trackId": "Phatr2_genes_unfiltered",
            "name": "Phatr2 gene models unfiltered (JGI)",
            "category": ["  Genes"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "Gff3TabixAdapter",
                "gffGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/phatr2_gene_models_sorted.gff3.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/phatr2_gene_models_sorted.gff3.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
            "renderer": {
                "type": "SvgFeatureRenderer",
            },
        },
        {
            "type": "FeatureTrack",
            "trackId": "ASM15095v2-lincRNAs",
            "name": "Phaeodactylum tricornutum lincRNAs",
            "category": [" lincRNAs"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "Gff3TabixAdapter",
                "gffGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/Phaeodactylum_tricornutum_sort_lincRNAs.gtf.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/Phaeodactylum_tricornutum_sort_lincRNAs.gtf.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
            "renderer": {
                "type": "SvgFeatureRenderer",
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "H3K4me2_lowN",
            "name": "H3K4me2_lowN",
            "category": [" Histone mark"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {"uri": f"{SERVER_DATA_URL}/H3K4me2_lowN.bw"},
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "H3K4me2_NormalN_36bp",
            "name": "H3K4me2_NormalN_36bp",
            "category": [" Histone mark"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {"uri": f"{SERVER_DATA_URL}/H3K4me2_NormalN_36pb.bw"},
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "H3K4me2_NormalN_100bp",
            "name": "H3K4me2_NormalN_100bp",
            "category": [" Histone mark"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {
                    "uri": f"{SERVER_DATA_URL}/H3K4me2_NormalN_100pb.bw"
                },
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "H3K9me2_NormalN_36bp",
            "name": "H3K9me2_NormalN_36bp",
            "category": [" Histone mark"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {"uri": f"{SERVER_DATA_URL}/H3K9me2_NormalN_36pb.bw"},
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "H3K9me2_NormalN_100bp",
            "name": "H3K9me2_NormalN_100bp",
            "category": [" Histone mark"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {
                    "uri": f"{SERVER_DATA_URL}/H3K9me2_NormalN_100pb.bw"
                },
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "H3K27me3_36bp",
            "name": "H3K27me3_36bp",
            "category": [" Histone mark"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {"uri": f"{SERVER_DATA_URL}/H3K27me3_36pb.bw"},
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "H3K27me3_100bp",
            "name": "H3K27me3_100bp",
            "category": [" Histone mark"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {"uri": f"{SERVER_DATA_URL}/H3K27me3_100pb.bw"},
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "H4K914Ac_LowN",
            "name": "H4K914Ac_LowN",
            "category": [" Histone mark"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {"uri": f"{SERVER_DATA_URL}/H4K914Ac_LowN.bw"},
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "H4K914Ac_NormalN",
            "name": "H4K914Ac_NormalN",
            "category": [" Histone mark"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {"uri": f"{SERVER_DATA_URL}/H4K914Ac_NormalN.bw"},
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "H3K9me3_lowN",
            "name": "H3K9me3_lowN",
            "category": [" Histone mark"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {"uri": f"{SERVER_DATA_URL}/H3K9me3_lowN.bw"},
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "H3K9me3_NormalN",
            "name": "H3K9me3_NormalN",
            "category": [" Histone mark"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {"uri": f"{SERVER_DATA_URL}/H3K9me3_NormalN.bw"},
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "LowN_BIS_methylation",
            "name": "LowN_BIS_methylation",
            "category": [" Methylation"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {"uri": f"{SERVER_DATA_URL}/LowN_BIS_methylation.bw"},
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "McrBC_methylation",
            "name": "McrBC_methylation",
            "category": [" Methylation"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {"uri": f"{SERVER_DATA_URL}/McrBC_methylation.bw"},
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "RepleteN_BIS_methylation",
            "name": "RepleteN_BIS_methylation",
            "category": [" Methylation"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {
                    "uri": f"{SERVER_DATA_URL}/RepleteN_BIS_methylation.bw"
                },
            },
        },
        {
            "type": "VariantTrack",
            "trackId": "ASM15095v2-variant",
            "name": "Phaeodactylum tricornutum Ensembl variants",
            "category": [" Variants"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "VcfTabixAdapter",
                "vcfGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum.vcf.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum.vcf.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
        },
        {
            "type": "VariantTrack",
            "trackId": "PT1",
            "name": "Phaeodactylum tricornutum PT1 ecotype",
            "category": [" Ecotype"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "VcfTabixAdapter",
                "vcfGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/PT1.vcf.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/PT1.vcf.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
        },
        {
            "type": "VariantTrack",
            "trackId": "PT2",
            "name": "Phaeodactylum tricornutum PT2 Ecotype",
            "category": [" Ecotype"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "VcfTabixAdapter",
                "vcfGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/PT2.vcf.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/PT2.vcf.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
        },
        {
            "type": "VariantTrack",
            "trackId": "PT3",
            "name": "Phaeodactylum tricornutum PT3 Ecotype",
            "category": [" Ecotype"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "VcfTabixAdapter",
                "vcfGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/PT3.vcf.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/PT3.vcf.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
        },
        {
            "type": "VariantTrack",
            "trackId": "PT4",
            "name": "Phaeodactylum tricornutum PT4 Ecotype",
            "category": [" Ecotype"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "VcfTabixAdapter",
                "vcfGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/PT4.vcf.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/PT4.vcf.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
        },
        {
            "type": "VariantTrack",
            "trackId": "PT5",
            "name": "Phaeodactylum tricornutum PT5 Ecotype",
            "category": [" Ecotype"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "VcfTabixAdapter",
                "vcfGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/PT5.vcf.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/PT5.vcf.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
        },
        {
            "type": "VariantTrack",
            "trackId": "PT6",
            "name": "Phaeodactylum tricornutum PT6 Ecotype",
            "category": [" Ecotype"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "VcfTabixAdapter",
                "vcfGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/PT6-true.vcf.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/PT6-true.vcf.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
        },
        {
            "type": "VariantTrack",
            "trackId": "PT7",
            "name": "Phaeodactylum tricornutum PT7 Ecotype",
            "category": [" Ecotype"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "VcfTabixAdapter",
                "vcfGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/PT7.vcf.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/PT7.vcf.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
        },
        {
            "type": "VariantTrack",
            "trackId": "PT8",
            "name": "Phaeodactylum tricornutum PT8 Ecotype",
            "category": [" Ecotype"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "VcfTabixAdapter",
                "vcfGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/PT8.vcf.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/PT8.vcf.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
        },
        {
            "type": "VariantTrack",
            "trackId": "PT9",
            "name": "Phaeodactylum tricornutum PT9 Ecotype",
            "category": [" Ecotype"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "VcfTabixAdapter",
                "vcfGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/PT9.vcf.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/PT9.vcf.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
        },
        {
            "type": "VariantTrack",
            "trackId": "PT10",
            "name": "Phaeodactylum tricornutum PT10 Ecotype",
            "category": [" Ecotype"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "VcfTabixAdapter",
                "vcfGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/PT10.vcf.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/PT10.vcf.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
        },
    ]
}
