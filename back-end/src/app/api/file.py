import os
from fastapi import APIRouter
from fastapi.responses import FileResponse
from app import API_DATA_FOLDER_PATH

router = APIRouter()


@router.get("/{filename}")
def get_file(filename: str):
    files = os.listdir(API_DATA_FOLDER_PATH)
    if filename in files:
        return FileResponse(os.path.join(API_DATA_FOLDER_PATH, filename))
    else:
        return {"File": "not found"}
