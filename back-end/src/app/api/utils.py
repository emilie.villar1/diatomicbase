import re
from typing import List, Dict
from copy import deepcopy
from app.db.models import Gene, DomainAnnotation
from app.api.models import OneSearchGene


def format_gene_response(
    genes: List[Gene],
    additional_attributes: List[str] = [],
) -> List[Dict[str, str]]:
    # Obtain the a dict of the value of the gene, deleting the sqlalchemy internal variable
    response_object = []
    for gene in genes:
        seq_region = deepcopy(gene.seq_region.__dict__)
        assembly = deepcopy(gene.assembly.__dict__)
        gene_dict = deepcopy(gene.__dict__)
        del gene_dict["_sa_instance_state"]
        del assembly["_sa_instance_state"]
        del seq_region["_sa_instance_state"]
        gene_dict["seq_region"] = seq_region
        gene_dict["assembly"] = assembly
        if "go" in additional_attributes:
            gene_dict["goTerms"] = get_list_go_term(gene.go)
        if "domain_annotation" in additional_attributes:
            if "interpro" in additional_attributes:
                gene_dict[
                    "domainsAnnotationAccession"
                ] = get_list_domain_annotation_term(
                    gene.domains_annotation, filter_interpro_only
                )
            else:
                gene_dict[
                    "domainsAnnotationAccession"
                ] = get_list_domain_annotation_term(gene.domains_annotation)
        if "kegg" in additional_attributes:
            gene_dict["keggAccession"] = gene.kegg.accession
        response_object.append(gene_dict)
    # Sort by Chromosome
    response_object = sorted(
        response_object, key=lambda gene: natural_keys(gene["seq_region"]["name"])
    )
    return response_object


def atoi(text):
    return int(text) if text.isdigit() else text


def natural_keys(text):
    return [atoi(c) for c in re.split(r"(\d+)", text)]


def get_list_go_term(list_go):
    go_term = []
    if not list_go:
        return []
    for go in list_go:
        go_term.append(go.term)
    return go_term


def get_list_domain_annotation_term(list_domain_annotation, filter_fn=None):
    domain_annotation_accession = []
    if not list_domain_annotation:
        return []
    for domain_annotation in list_domain_annotation:
        if filter_fn and not filter_fn(domain_annotation):
            continue
        domain_annotation_accession.append(domain_annotation.accession)
    return domain_annotation_accession


def get_dict_from_list_of_objects(list_object, filter_function=None):
    objects = []
    if len(list_object) > 0:
        for obj in list_object:
            if filter_function and not filter_function(obj):
                continue
            temp_obj = deepcopy(obj.__dict__)
            del temp_obj["_sa_instance_state"]
            objects.append(temp_obj)
    return objects


def filter_interpro_only(domain_annotation: DomainAnnotation):
    if domain_annotation.db_name == "InterPro":
        return True
    else:
        return False