#!/usr/bin/env Rscript


#### Passing argument to R ###

args = commandArgs(trailingOnly=TRUE)
if (length(args)==0) {
  stop("You must supply 5 argument: 
      [1] = Result directory with :
      [2] = comparisons file (comparisons.tsv)
      [3] = sample groups (runs_group.tsv)
      [4] = gene/transcript correspondance (tx2gene.csv)
      [5] = output file is not mandatory [default = out.tsv]"
       , call.=FALSE)
} else if (length(args)==4) {
  # default output file
  args[5] = "out.tab"
}


library(DESeq2)
library(tximport)

### Preparing input files ###
dir <- args[1]
setwd(dir)
dir.create(file.path("detailed_results"), showWarnings=FALSE)


comparisons <- read.table(args[2], h=T, sep="\t")
run_groups <- read.table(args[3], h=T, sep="\t")
gene_names <- read.csv(args[4], h=F)

cols <- c("transcript_id","gene_id","syn")
colnames(gene_names) <- cols
tx2gene <- gene_names[,-3]


####### Loop on all the comparisons ######

FC_table <- matrix("NA", 12312, nrow(comparisons))  #### Phatr3 contains 12311 genes, has to be changed if mapping has not been done on Phatr3
colnames(FC_table) <- paste(comparisons$comp_id, "log2FC", sep="_")
padj_table <- matrix("NA", 12312, nrow(comparisons))  #### Phatr3 contains 12311 genes, has to be changed if mapping has not been done on Phatr3
colnames(padj_table) <- paste(comparisons$comp_id, "padj", sep="_")


for (i in 1:nrow(comparisons)){
  project <- subset(run_groups, project_id == as.character(comparisons[i,"project_id"]))
  compa <- subset(project, (group_name == as.character(comparisons[i,"subset_1"]) | group_name == as.character(comparisons[i,"subset_2"])), select=c(run, group_name))
  files_list = subset(project, (group_name == as.character(comparisons[i,"subset_1"]) | group_name == as.character(comparisons[i,"subset_2"])), select=run)
  files <- file.path(dir, files_list[,1], "results", "salmon", files_list[,1], "quant.sf")
  names(files) <- files_list[,1]
  txi.salmon <- tximport(files, type="salmon", tx2gene=tx2gene)
  ExpDesign <- data.frame(row.names=compa[, "run"], condition=compa[,"group_name"])
  dds <- DESeqDataSetFromTximport(txi.salmon, ExpDesign, ~condition)
  dds <- DESeq(dds, betaPrior=FALSE)
  res <- results(dds,contrast=c("condition", as.character(comparisons[i,"subset_1"]), as.character(comparisons[i,"subset_2"])))
  counts_table = counts(dds, normalized=TRUE)
  detailed_table <- merge(counts_table, res, by="row.names", all.x=TRUE)
  file_name = paste("detailed_results/", comparisons[i,"comp_id"], sep="")
  write.table(detailed_table, file_name, quote=FALSE, row.names=FALSE, sep="\t")
  FC_table[,i] <- detailed_table$log2FoldChange
  padj_table[,i] <- detailed_table$padj
}
DEG_table <- cbind(FC_table, padj_table)

write.table(data.frame("GeneID"=detailed_table$Row.names, DEG_table), file=args[5], quote=FALSE, row.names=FALSE, sep="\t")
