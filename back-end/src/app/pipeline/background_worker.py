import os
import re
import time
import json
import logging
import subprocess
from typing import List, Dict
from multiprocessing import Process, Pool
from sqlalchemy.orm import Session
from app import BLAST_WORK_DIR
from app.logger import logger
from app.db.models import ProcessQueue


def blastn_worker(res: ProcessQueue, log: logging.Logger):
    log.info("In Blastn worker")
    log.debug(f"res.id: {res.id}")
    parameter = json.loads(res.parameter)
    log.debug(f"parameter: {parameter}")

    blast_db = os.path.join(BLAST_WORK_DIR, "nucleotide_db", "all_gene")
    blast_input_dir = os.path.join(BLAST_WORK_DIR, "blast_input")
    blast_output_dir = os.path.join(BLAST_WORK_DIR, "blast_output")
    input_fasta = f"{blast_input_dir}/{res.id}.fa"
    output_blast = f"{blast_output_dir}/{res.id}.xml"

    isFasta = False
    seq: str = re.sub("^ ", "", parameter["seq"])
    log.debug(f"seq: {seq}")
    if seq.startswith(">"):
        isFasta = True
    log.debug(f"isFasta: {isFasta}")
    # Add fasta header
    if not isFasta:
        seq = f">input_seq_{res.id}\n{seq}"
    log.debug(f"seq: {seq}")
    # Create fasta
    try:
        open(input_fasta, "w").write(seq)
    except Exception as err:
        log.error(err)
    # Launch BLASTn
    res_sub = subprocess.run(
        f"blastn -query {input_fasta} -db {blast_db} -out {output_blast} -outfmt 5",
        shell=True,
        capture_output=True,
        text=True,
    )
    log.debug(f"after subprocess")
    # Log output
    if res_sub.stdout:
        log.debug(f"stdout: {res_sub.stdout}")
    if res_sub.stderr:
        log.debug(f"stderr: {res_sub.stderr}")
    # Delete input fasta
    os.remove(input_fasta)
    log.debug(f"End blastn worker for id {res.id}")


def blastp_worker(res: ProcessQueue, log: logging.Logger):
    logger.info("In Blastp worker")
    log.debug(f"res.id: {res.id}")
    parameter = json.loads(res.parameter)
    log.debug(f"parameter: {parameter}")

    blast_db = os.path.join(BLAST_WORK_DIR, "protein_db", "diamond_db")
    blast_input_dir = os.path.join(BLAST_WORK_DIR, "blast_input")
    blast_output_dir = os.path.join(BLAST_WORK_DIR, "blast_output")
    input_fasta = f"{blast_input_dir}/{res.id}.fa"
    output_blast = f"{blast_output_dir}/{res.id}.xml"

    isFasta = False
    seq: str = re.sub("^ ", "", parameter["seq"])
    log.debug(f"seq: {seq}")
    if seq.startswith(">"):
        isFasta = True
    log.debug(f"isFasta: {isFasta}")
    # Add fasta header
    if not isFasta:
        seq = f">input_seq_{res.id}\n{seq}"
    log.debug(f"seq: {seq}")
    # Create fasta
    open(input_fasta, "w").write(seq)
    # Launch BLASTn
    res_sub = subprocess.run(
        f"diamond blastp -q {input_fasta} -d {blast_db} -o {output_blast} -f 5 -p 1",
        shell=True,
        capture_output=True,
        text=True,
    )
    log.debug(f"after subprocess")
    # Log output
    if res_sub.stdout:
        log.debug(f"stdout: {res_sub.stdout}")
    if res_sub.stderr:
        log.debug(f"stderr: {res_sub.stderr}")
    # Delete input fasta
    os.remove(input_fasta)
    log.debug(f"End blastp worker for id {res.id}")


def blastx_worker(res: ProcessQueue, log: logging.Logger):
    logger.info("In Blastx worker")
    log.debug(f"res.id: {res.id}")
    parameter = json.loads(res.parameter)
    log.debug(f"parameter: {parameter}")

    blast_db = os.path.join(BLAST_WORK_DIR, "protein_db", "diamond_db")
    blast_input_dir = os.path.join(BLAST_WORK_DIR, "blast_input")
    blast_output_dir = os.path.join(BLAST_WORK_DIR, "blast_output")
    input_fasta = f"{blast_input_dir}/{res.id}.fa"
    output_blast = f"{blast_output_dir}/{res.id}.xml"

    isFasta = False
    seq: str = re.sub("^ ", "", parameter["seq"])
    log.debug(f"seq: {seq}")
    if seq.startswith(">"):
        isFasta = True
    log.debug(f"isFasta: {isFasta}")
    # Add fasta header
    if not isFasta:
        seq = f">input_seq_{res.id}\n{seq}"
    log.debug(f"seq: {seq}")
    # Create fasta
    open(input_fasta, "w").write(seq)
    # Launch BLASTn
    res_sub = subprocess.run(
        f"diamond blastx -q {input_fasta} -d {blast_db} -o {output_blast} -f 5 -p 1",
        shell=True,
        capture_output=True,
        text=True,
    )
    log.debug(f"after subprocess")
    # Log output
    if res_sub.stdout:
        log.debug(f"stdout: {res_sub.stdout}")
    if res_sub.stderr:
        log.debug(f"stderr: {res_sub.stderr}")
    # Delete input fasta
    os.remove(input_fasta)
    log.debug(f"End blastx worker for id {res.id}")


def select_worker(worker_type: str):
    if worker_type == "blastp":
        return blastp_worker
    elif worker_type == "blastn":
        return blastn_worker
    elif worker_type == "blastx":
        return blastx_worker


def queue_manager(session, n_process):
    logger = logging.getLogger("DiatOmicBase.queue_manager")

    pool = Pool(n_process)
    while True:
        time.sleep(1)
        process_queue: List[ProcessQueue] = session.query(ProcessQueue).all()

        if not process_queue:
            continue
        for process_todo in process_queue:
            print("manager process_todo.id:", process_todo.id)
            worker = select_worker(process_todo.worker_type)
            pool.apply_async(worker, args=(process_todo, logger))
            session.delete(process_todo)
            session.commit()


def launch_queue_manager(n_process):
    process = Process(
        target=queue_manager,
        args=(n_process,),
    )
    process.daemon = True
    process.start()


def test_func(session):
    launch_queue_manager(2)
