import sleep from "./sleep";

export default async function blinkTrackButton() {
  const target = document.getElementsByClassName(
    "MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textSecondary MuiButton-textSizeSmall MuiButton-sizeSmall"
  );
  const button = target[0];
  button.className = `${button.className} blink_me`;
  await sleep(2000);
  button.className = button.className.replace("blink_me", "");
}
