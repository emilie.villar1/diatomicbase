// For docker-compose, see issue #38:
const apiUrl = process.env.API_URL || process.env.NEXT_PUBLIC_API_URL;

export default apiUrl
