export function extractPublication(publication: string, doi: string) {
  const publications = publication.split("&");
  const dois = doi.split("&");
  let toReturn = [];
  for (let i = 0; i < publications.length; i++) {
    toReturn.push({ Publication: publications[i], doi: dois[i] });
  }
  return toReturn;
};

export function formatEtAl(publi: string) {
  const pub = publi.split("et al.");
  if (pub.length === 1) return publi;
  return (
    <div>
      {pub[0]}
      <i>et al.</i>
      {pub[1]}
    </div>
  );
};
