import * as React from "react";
import Head from "next/head";
import Layout from "../components/Layout";

export default function Error404() {
  return (
    <>
      <Head>
        <title>404-Error</title>
      </Head>
      <Layout>
        <div
          style={{
            display: "block",
            textAlign: "center",
            fontSize: "70px",
            fontWeight: "bold",
            margin: "2em",
          }}
        >
          Error 404
          <br />
          <p style={{ fontSize: "26px" }}>
            Return <a href="/">Home</a>
          </p>
        </div>
      </Layout>
    </>
  );
}
