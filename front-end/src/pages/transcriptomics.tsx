import React, { useState, useMemo } from "react";
import BioSampleSelector from "../components/BioSampleSelector";
import { GetStaticProps } from "next";
import styles from "./transcriptomics.module.scss";
import ReactTable from "react-table-v6";
import Fuse from "fuse.js";
import apiUrl from "../utils/getApiUrl";
import { extractPublication, formatEtAl } from "../utils/publications";
import Layout from "../components/Layout";

interface BioSamples
  extends Array<{
    id: number;
    name: string;
  }> {}

interface BioProjectMetaData {
  BioProject: string;
  Description: string;
  Nb_runs: number;
  Nb_replicates: number;
  Design: string;
  Publication: string;
  Keywords: string;
  doi: string;
  Strain: string;
  BioSamples: BioSamples;
}

export interface TranscriptomicsProps {
  rnaSeqMetaData: Array<BioProjectMetaData>;
}

export default function Transcriptomics({
  rnaSeqMetaData,
}: TranscriptomicsProps) {
  const [bioSamples, setBioSamples] = useState<BioSamples>([]);
  const [subset1, setSubset1] = useState<BioSamples>([]);
  const [subset2, setSubset2] = useState<BioSamples>([]);
  // Columns for React Table
  const columns = [
    {
      Header: "Select",
      accessor: "BioProject",
      Cell: (props) => (
        <input
          type="checkbox"
          name={props.value}
          id={props.value}
          onChange={onSelectClick}
        />
      ),
      width: 70,
    },
    { Header: "BioProject Accession", accessor: "BioProject", width: 200 },
    {
      Header: "Description",
      accessor: "Description",
      Cell: (props) => (
        <div className={styles.descriptionRow}>{props.value}</div>
      ),
    },
    { id: "nbRuns", Header: "Nb runs", accessor: "Nb_runs", width: 70 },
    {
      id: "nbReplicates",
      Header: "Nb replicates",
      accessor: "Nb_replicates",
      width: 120,
    },
    {
      Header: "SRA Experiments",
      accessor: "Design",
      Cell: (props) => (
        <div className={styles.descriptionRow}>{props.value}</div>
      ),
    },
    {
      id: "publication",
      Header: "Publication",
      accessor: (row) => {
        return { Publication: row.Publication, doi: row.doi };
      },
      Cell: (props) => {
        const listPublications = extractPublication(
          props.value.Publication,
          props.value.doi
        );
        return (
          <div className={styles.descriptionRow}>
            {listPublications.map((value, i) => (
              <div style={{ marginBottom: "0.5em" }} key={i}>
                {value.doi === "NA" ? (
                  <div>{value.Publication}</div>
                ) : (
                  <a href={value.doi} target="_blank" rel="noopener noreferrer">
                    {formatEtAl(value.Publication)}
                  </a>
                )}
              </div>
            ))}
          </div>
        );
      },
    },
  ];

  // Function to open browse when clicking on the button
  const clickBrowse = () => {
    document.getElementById("uploadRnaMetaData").click();
  };

  const removeDeselected = (bioProjectMetaData) => {
    const bioSamplesClean = bioSamples.filter((obj) => {
      return !bioProjectMetaData.BioSamples.find((objToRemove) => {
        return obj.id === objToRemove.id;
      });
    });
    const subset1Clean = subset1.filter((obj) => {
      return !bioProjectMetaData.BioSamples.find((objToRemove) => {
        return obj.id === objToRemove.id;
      });
    });
    const subset2Clean = subset2.filter((obj) => {
      return !bioProjectMetaData.BioSamples.find((objToRemove) => {
        return obj.id === objToRemove.id;
      });
    });

    if (bioSamples !== bioSamplesClean) {
      setBioSamples(bioSamplesClean);
    }
    if (subset1 !== subset1Clean) {
      setSubset1(subset1Clean);
    }
    if (subset2 !== subset2Clean) {
      setSubset2(subset2Clean);
    }
  };

  // Add or remove Biosample when selected
  const onSelectClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    const bioProjectMetaData = rnaSeqMetaData.filter((obj) => {
      return obj.BioProject === event.target.id;
    })[0];

    if (event.target.checked === true) {
      setBioSamples([...bioSamples, ...bioProjectMetaData.BioSamples]);
    } else if (event.target.checked === false) {
      removeDeselected(bioProjectMetaData);
    }
  };

  // Search part
  const [tableData, setTableData] = useState([]);
  const [searchValue, setSearchValue] = useState("");
  const fuseOptions = {
    keys: [
      "BioProject",
      "Description",
      "Design",
      "Publication",
      "Keywords",
      "Strain",
    ],
  };
  const fuse = useMemo(() => new Fuse(rnaSeqMetaData, fuseOptions), []);
  const searchTranscriptomic = () => {
    if (searchValue !== "") {
      setTableData(fuse.search(searchValue).map((obj) => obj.item));
    } else {
      setTableData([]);
    }
  };

  return (
    <>
      <Layout>
        <div className={styles.container}>
          <div className={styles.warning}>
            <strong>Work in Progress</strong>
          </div>
          <div className={styles.inlineFlex}>
            <p>Select the experiment:</p>
            <div className={styles.search}>
              <input
                type="text"
                id="search"
                placeholder="Search "
                value={searchValue}
                onChange={(e) => {
                  setSearchValue(e.target.value);
                  searchTranscriptomic();
                }}
              />
            </div>
          </div>
          <ReactTable
            className={(styles.table, "-highlight")}
            defaultPageSize={10}
            columns={columns}
            data={tableData.length > 0 ? tableData : rnaSeqMetaData}
          />
          <p>
            Or upload your own data{" "}
            <input
              type="file"
              className={styles.hidden}
              id="uploadRnaMetaData"
            />
            <button className={styles.button} onClick={clickBrowse}>
              Browse
            </button>
          </p>
          <p className={styles.subsetSentence}>
            Choose the subsets to compare:{" "}
          </p>
        </div>
        <BioSampleSelector
          bioSamples={bioSamples}
          setBioSamples={setBioSamples}
          subset1={subset1}
          setSubset1={setSubset1}
          subset2={subset2}
          setSubset2={setSubset2}
        />
      </Layout>
    </>
  );
}

export const getStaticProps: GetStaticProps = async (context) => {
  const result = await fetch(apiUrl + "/rna_seq/");
  const rnaSeqMetaData = await result.json();
  return {
    props: { rnaSeqMetaData },
  };
};
