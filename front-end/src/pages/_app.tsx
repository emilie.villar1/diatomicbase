import "../../styles/globals.css";
import "../../styles/react-table.css";
import "fontsource-roboto";

import { AppProps } from "next/app";

function MyApp({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />;
}

export default MyApp;
