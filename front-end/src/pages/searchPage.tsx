import React from "react";
import { useRouter, NextRouter } from "next/router";
import styles from "./searchPage.module.scss";
import useSearch, { typeSearchType } from "../hooks/useSearch";
import useBlastSearch from "../hooks/useBlastSearch";
import Head from "next/head";
import Layout from "../components/Layout";

export default function SearchPage() {
  const router = useRouter();
  const [
    blastSeq,
    setBlastSeq,
    blastError,
    clearBlast,
    launchBlastIfValid,
  ] = useBlastSearch(router);

  return (
    <>
      <Head>
        <title>Gene Page</title>
      </Head>
      <Layout>
        <div className={styles.container}>
          <div className={styles.description}>
            On each gene page you will find: cDNA and protein sequences, a
            genome browser (with Phatr2 and Phatr3 gene models, ecotype
            variants, histone marks, non-coding RNAs), comparisons of gene
            expression in public RNA-Seq datasets, co-expression networks as
            well as several domains annotation.
          </div>
          <div className={styles.column}>
            <SearchTextField
              label="Gene ID"
              term="gene"
              placeholder="e.g. Phatr3_EG02276"
              router={router}
              example={(click) => {
                return (
                  <div>
                    E.g. <a onClick={click}>Phatr3_EG02276</a>,{" "}
                    <a onClick={click}>J23</a>
                  </div>
                );
              }}
            />
            <SearchTextField
              label="Gene Ontology"
              term="go"
              placeholder="e.g. GO:0050897"
              router={router}
              example={(click) => {
                return (
                  <div>
                    E.g. <a onClick={click}>GO:0006406</a>,{" "}
                    <a onClick={click}>nitrate assimilation</a>
                  </div>
                );
              }}
            />
            <SearchTextField
              label="KEGG ID"
              term="kegg"
              placeholder="e.g. Ko00650"
              router={router}
              example={(click) => {
                return (
                  <div>
                    E.g. <a onClick={click}>ko00650</a>,{" "}
                    <a onClick={click}>500</a>, <a onClick={click}>Thiamine</a>
                  </div>
                );
              }}
            />
            <SearchTextField
              label="Keyword"
              term="keyword"
              placeholder="e.g. Hydrogenase"
              router={router}
              example={(click) => {
                return (
                  <div>
                    E.g. <a onClick={click}>dehydrogenase</a>,{" "}
                    <a onClick={click}>Ferritin</a>
                  </div>
                );
              }}
            />
            <SearchTextField
              label="INTERPRO"
              term="interpro"
              placeholder="e.g. IPR003959"
              router={router}
              example={(click) => {
                return (
                  <div>
                    E.g. <a onClick={click}>IPR003959</a>,{" "}
                    <a onClick={click}>3593</a>
                  </div>
                );
              }}
            />
          </div>
          <div className={styles.column}>
            <label htmlFor="BLAST">BLAST</label>
            <div
              className={
                blastError.length === 0
                  ? styles.blastSearch
                  : styles.blastSearchError
              }
            >
              <textarea
                wrap="soft"
                cols={70}
                rows={15}
                name="BLAST"
                id="BLAST"
                placeholder="Paste your fasta here"
                value={blastSeq}
                onChange={(e) => setBlastSeq(e.target.value)}
              />
              {blastError.length !== 0 && <p>{blastError}</p>}
              <button className={styles.secondary} onClick={clearBlast}>
                Clear Blast
              </button>
              <br />
              <button onClick={() => launchBlastIfValid("blastn")}>
                BLASTn
              </button>
              <span style={{ margin: "10px" }}>or</span>
              <button onClick={() => launchBlastIfValid("blastx")}>
                BLASTx (Diamond)
              </button>
              <span style={{ margin: "10px" }}>or</span>
              <button onClick={() => launchBlastIfValid("blastp")}>
                BLASTp (Diamond)
              </button>
            </div>
            <label htmlFor="HMM">HMM</label>
            <div className={`${styles.search} ${styles.disabled}`}>
              <input
                type="text"
                name="HMM"
                id="HMM"
                placeholder="Paste your sequence here"
              />
              <button className={styles.disabled} style={{ fontSize: "10px" }}>
                Not Implemented
              </button>
            </div>
          </div>
        </div>
      </Layout>
    </>
  );
}

interface SearchTextFieldProps {
  term: typeSearchType;
  label: string;
  placeholder?: string;
  example?: React.FC<(e: any) => void>;
  router: NextRouter;
}

function SearchTextField(props: SearchTextFieldProps) {
  const { term, label, placeholder, example, router } = props;
  const [termValue, setTermValue, submitTerm] = useSearch(term, router);

  const submitOnEnter = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === "Enter") submitTerm();
  };

  const egOnClick = (e) => {
    setTermValue(e.target.textContent);
  };

  return (
    <>
      <label htmlFor={term}>{label}</label>
      <div className={styles.search}>
        <input
          type="text"
          name={term}
          id={term}
          placeholder={placeholder !== undefined ? placeholder : "placeholder"}
          value={termValue}
          onChange={(e) => setTermValue(e.target.value)}
          onKeyPress={submitOnEnter}
        />
        <button onClick={submitTerm}>Search</button>
      </div>
      {example ? (
        <div className={styles.example}>
          <div className={styles.line}></div>
          <div className={styles.eg}>{example(egOnClick)}</div>
        </div>
      ) : (
        <div className={styles.marginBottom}></div>
      )}
    </>
  );
}
