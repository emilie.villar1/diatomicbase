import * as React from "react";
import Head from "next/head";
import styles from "./legalNotice.module.scss";
import Layout from "../components/Layout";

export default function LegalNotice() {
  return (
    <>
      <Head>
        <title>Legal Notice</title>
      </Head>
      <Layout>
        <div className={styles.container}>
          <h2>Legal Notices</h2>
          Published on 19 January 2021 The website diatomicbase.fr is owned
          exclusively by the département de biologie de l’ENS, and respects the
          French and international legislation on copyright and intellectual
          property.
          <h3>Publisher</h3>
          Département de Biologie de l’École normale supérieure 46 rue d’Ulm
          F-75230 Paris cedex 05
          <h3>Editor</h3>
          Pierre Paoletti, Director or the Département de Biologie de l’École
          Normale Supérieure
          <h3>Technical Informations</h3>
          Hosting and management of the website: Service Informatique du
          département de biologie de l’ENS. Accessibility according to Law
          n°2005-102 for equal opportunity, participation and citizenship for
          disabled people.
          <h3>Copyright</h3>
          Content, graphics, and design are property of the département de
          biologie de l’ENS and/or their use has been acquired by a third
          person. A copy, partial or total, is allowed only by explicit
          agreement of the département de biologie de l’ENS. The département de
          biologie de l’ENS agrees to the use of links directing to its website,
          as long as the pages do not open inside another website, but on a
          separate window. The département de biologie de l’ENS updates
          regularly the website but cannot guarantee the accuracy and/or
          topicality of the information published. You can notify the webmaster
          of any error, out-of-date information or broken link:
          diatomicbase@bio.ens.psl.eu
          <h3>
            Right of Accessing, Modifying, Correcting and Suppressing Data
          </h3>
          (Law nº78-17 of January 6 1978, about Informatic, Files and Liberty,
          or "Loi Informatique et Libertés") Every person whose name or picture
          (allowing his/her identification) appears on this website can, at any
          time, ask for the suppression or modification of information relating
          to him/her, by sending an email to the webmaster:
          diatomicbase@bio.ens.psl.eu
          <h3>Rights and Duties of Users</h3>
          The use of this website is subject to French legal dispositions about
          nominative treatments. Users are responsible for the queries they make
          as well as for the interpretation and use they make of results. They
          have to make a use respecting the current reglementations and the
          recommandations of the Commission Nationale de l’Informatique et des
          Libertés (CNIL) when data have a nominative character. Capturing
          nominative information in order to create or enrich another nominative
          treatement (for instance, for a commercial or advertising use) is
          strictly forbidden for all countries.
          <h3>Information on the Personal data protection</h3>
          When browsing the website we collect information about IP address,
          date/time of visit, page visited, browser type, data transferred and
          success of the request. This information is collected to better
          understand how visitors use the DiatOmicBase website, to create
          summary reports, to monitor, protect the security and stability of our
          website resources.
        </div>
      </Layout>
    </>
  );
}
