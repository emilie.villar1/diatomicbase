import React from "react";
import Head from "next/head";
import { GetStaticPaths, GetStaticProps } from "next";
import styles from "./[dlTranscriptomics].module.scss";
import apiUrl from "../../../utils/getApiUrl";
import Layout from "../../../components/Layout";

interface bioProjectInfoInterface {
  bioproject: string;
  description: string;
  design: string;
  publication: string;
  doi: string;
  runs: Array<{ name: string; dl: string }>;
  comparisons: Array<{ id: string; subset1: string; subset2: string }>;
}

export default function dlTranscriptomics({
  bioProjectInfo,
}: {
  bioProjectInfo: bioProjectInfoInterface;
}) {
  return (
    <>
      <Head>
        <title>{bioProjectInfo.bioproject}</title>
      </Head>
      <Layout>
        <div className={styles.main}>
          <h2>
            <a
              href={`https://www.ncbi.nlm.nih.gov/bioproject/?term=${bioProjectInfo.bioproject}`}
              target="_blank"
              rel="noopener noreferrer"
            >
              {bioProjectInfo.bioproject}
            </a>{" "}
            Download Page
          </h2>
          <hr />
          <p>
            {bioProjectInfo.description}: {bioProjectInfo.design}
          </p>
          <br />
          <p>
            {bioProjectInfo.runs.length}{" "}
            {bioProjectInfo.runs.length > 1 ? "runs:" : "run:"}
          </p>
          {bioProjectInfo.runs.map((run, i) => {
            return (
              <div key={i} className={styles.dl}>
                <a href={run.dl} download>
                  - Download {run.name} fastq -
                </a>
              </div>
            );
          })}
          <p>
            {bioProjectInfo.comparisons.length}{" "}
            {bioProjectInfo.comparisons.length > 1
              ? "comparisons:"
              : "comparison:"}
          </p>
          {bioProjectInfo.comparisons.map((comparison, i) => {
            return (
              <div key={i} className={styles.comparisonList}>
                <a
                  className={styles.dl}
                  href={`${apiUrl}/rna_seq/dl_comparison/${comparison.id}`}
                  target="_blank"
                  rel="noopener noreferrer"
                  download
                >
                  - Download {comparison.subset1} vs {comparison.subset2}{" "}
                  Differential Expression table -
                </a>
              </div>
            );
          })}
          <p>
            Reference:{" "}
            <a
              href={bioProjectInfo.doi}
              target="_blank"
              rel="noopener noreferrer"
            >
              {bioProjectInfo.publication}
            </a>
          </p>
        </div>
      </Layout>
    </>
  );
}

async function getAllBioProjectPaths() {
  const response = await fetch(apiUrl + "/rna_seq/name");
  const bioProjectNames: string[] = await response.json();
  // Nextjs want the paths, not just the bioProjectName
  return bioProjectNames.map((bioProjectName) => {
    return `/resources/transcriptomics/${bioProjectName}`;
  });
}

async function getBioProjectInfo(bioProjectName: string) {
  const response = await fetch(`${apiUrl}/rna_seq/dl_page/${bioProjectName}`);
  const bioProjectInfo = await response.json();
  return bioProjectInfo;
}

export const getStaticPaths: GetStaticPaths = async () => {
  const paths = await getAllBioProjectPaths();
  return {
    paths,
    fallback: false,
  };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  // Nextjs will give a params props with the name of the React page as path.
  const bioProjectInfo = await getBioProjectInfo(
    params.dlTranscriptomics as string
  );
  return {
    props: {
      bioProjectInfo,
    },
  };
};
