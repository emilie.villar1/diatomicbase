import React, { useState, useEffect } from "react";
import styles from "./searchBlastResult.module.scss";
import useQuery from "../hooks/useQuery";
import ReactTable from "react-table-v6";
import Link from "next/link";
import apiUrl from "../utils/getApiUrl";
import Layout from "../components/Layout";

interface BlastResult {
  blast_type: "blastn" | "blastp";
  blast_version: string;
  blast_id: number;
  blast_accession: string;
  hits: Array<{
    accession: string;
    description: string;
    chrom: string;
    species: string;
    seq_len: number;
    hsps: Array<hspsInterface>;
  }>;
}

interface hspsInterface {
  evalue: number;
  gap_num: number;
  ident_num: number;
  pos_num: number;
  bitscore: number;
  hit_seq: string;
  query_seq: string;
}

export default function SearchBlastResult() {
  const query = useQuery();
  const [blastData, setBlastData] = useState<BlastResult>();
  const [loadingTime, setLoadingTime] = useState(0);
  const loading = blastData === undefined ? true : false;

  useEffect(() => {
    const FetchUntilFindAndUpdateTimer = async (blast_id: string) => {
      await sleep(1000);
      let loading = await fetchBlastResult(blast_id);
      if (loading === true) {
        setLoadingTime(loadingTime + 1);
      }
    };

    // Fetch blastResult and also return if the loading have ended as for the while loop
    const fetchBlastResult = async (blast_id: string): Promise<boolean> => {
      const res = await fetch(`${apiUrl}/search/blast/${blast_id}`);
      const json = await res.json();
      if (res.ok) {
        setBlastData(json);
        return false;
      } else {
        return true;
      }
    };

    // Wait for nextjs hydratation so query is initialized
    if (!query) {
      return;
    }

    FetchUntilFindAndUpdateTimer(query.blast_id as string);
  }, [query, loadingTime]);

  const mean = (field: "evalue", hsps: Array<hspsInterface>) => {
    let average = (array) => array.reduce((a, b) => a + b);
    if (field === "evalue") {
      const evalues = hsps.map((hsp) => hsp.evalue);
      return average(evalues);
    }
  };

  const tableColumns = [
    {
      Header: "Description",
      accessor: "description",
    },
    {
      Header: "Gene ID",
      accessor: "accession",
      Cell: (props) => (
        <Link href={`genes/${props.value}`}>
          <a className={styles.geneCell}>{props.value}</a>
        </Link>
      ),
      width: 175,
    },
    {
      Header: "E-value",
      accessor: "hsps",
      Cell: (props) => <div className="">{mean("evalue", props.value)}</div>,
      width: 150,
    },
    {
      Header: "Chromosome",
      accessor: "chrom",
      Cell: (props) => <div className={styles.geneCell}>{props.value}</div>,
      width: 125,
    },
    {
      Header: "Length",
      accessor: "seq_len",
      width: 100,
    },
    {
      Header: "Species",
      accessor: "species",
      width: 250,
    },
  ];

  return (
    <>
      <Layout>
        {loading ? (
          <div className={styles.container}>
            Waiting for blast:{" "}
            {new Date(loadingTime * 1000).toISOString().substr(11, 8)}
          </div>
        ) : blastData.hits.length === 0 ? (
          <div className={styles.container}>
            <p>No hit found for {blastData.blast_accession} </p>
          </div>
        ) : (
          <div className={styles.container}>
            <ReactTable
              className="-highlight"
              columns={tableColumns}
              data={blastData.hits}
              defaultPageSize={10}
            />
          </div>
        )}
      </Layout>
    </>
  );
}

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
