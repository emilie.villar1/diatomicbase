import React, { useState, useEffect } from "react";
import { useRouter, NextRouter } from "next/router";
import Link from "next/link";
import ReactTable from "react-table-v6";
import Head from "next/head";
import styles from "./searchResult.module.scss";
import { geneInfoProps } from "./genes/[gene]";
import apiUrl from "../utils/getApiUrl";
import { typeSearchType } from "../hooks/useSearch";
import Layout from "../components/Layout";

interface genesInfoFind extends geneInfoProps {
  length: number;
  goTerms?: string[];
  goDisplay?: string;
  domainAnnotationDisplay?: string;
  kegg?: { accession: string; description: string };
  keggAccession?: string;
  domainsAnnotationAccession?: string[];
}

export default function SearchResult() {
  const router = useRouter();
  const { searchTerm, searchType } = getSearchTypeAndTerm(router);
  const [resData, setResData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [findBy, setFindBy] = useState("");

  // Get query searchTerm and searchType and if present multiple time
  // return just the first one
  function getSearchTypeAndTerm(router: NextRouter) {
    let searchTerm = router.query.searchTerm;
    if (Array.isArray(searchTerm)) {
      searchTerm = searchTerm[0];
    }
    let searchType = router.query.searchType;
    if (Array.isArray(searchType)) {
      searchType = searchType[0];
    }
    return { searchTerm, searchType } as {
      searchTerm: string;
      searchType: typeSearchType;
    };
  }

  useEffect(() => {
    async function searchResult(searchTerm) {
      const url = getApiUrl(searchType, searchTerm);
      const response = await fetch(url);
      let json = await response.json();
      if (response.ok) {
        json = formatJson(json, searchType);
        setResData(json);
        setLoading(false);
      } else if (response.status === 404) {
        setResData([]);
        setLoading(false);
      } else {
        setLoading(true);
      }
    }
    if (searchTerm === "") {
      setResData([]);
      setLoading(false);
    }
    if (searchTerm !== undefined) searchResult(searchTerm);
  }, [searchTerm]);

  // Get API URL corresponding to the searchType given
  const getApiUrl = (searchType: string, searchTerm: string): string => {
    return `${apiUrl}/search/${searchType}/${searchTerm}`;
  };

  const formatJson = (
    json: genesInfoFind[] | [string, genesInfoFind[]],
    searchType: typeSearchType
  ): genesInfoFind[] => {
    let data: genesInfoFind[];
    let findBy;
    if (searchType === "searchbar") {
      findBy = json[0];
      data = json[1] as genesInfoFind[];
      setFindBy(findBy);
    } else {
      data = json as genesInfoFind[];
    }

    data = addGenesLength(data);
    if (searchType === "go" || findBy === "GO") {
      data = formatGo(data);
    } else if (searchType === "interpro" || findBy === "INTERPRO") {
      data = formatDomainAnnotation(data);
    }
    return data;
  };

  const addGenesLength = (json: geneInfoProps[]): genesInfoFind[] => {
    json.forEach((geneInfo) => {
      geneInfo["size"] = geneInfo.end - geneInfo.start;
    });
    return json as genesInfoFind[];
  };

  // Add a field named goDisplay to have info readable by ReactTable
  const formatGo = (json: genesInfoFind[]): genesInfoFind[] => {
    for (let row of json) {
      let goDisplay = row.goTerms[0];
      for (let go of row.goTerms.slice(1)) {
        goDisplay += `; ${go}`;
      }
      row.goDisplay = goDisplay;
    }
    return json;
  };

  // Add a field named domainAnnotationdisplay to have those info readable by ReactTable
  const formatDomainAnnotation = (json: genesInfoFind[]): genesInfoFind[] => {
    for (let row of json) {
      let dADisplay = row.domainsAnnotationAccession[0];
      for (let dA of row.domainsAnnotationAccession.slice(1)) {
        dADisplay += `; ${dA}`;
      }
      row.domainAnnotationDisplay = dADisplay;
    }
    return json;
  };

  const getColumns = (searchType: string = "") => {
    if (
      searchType === "" ||
      searchType === "keyword" ||
      (searchType === "searchbar" && findBy === "gene") ||
      searchType === "gene"
    ) {
      return columnsGene;
    } else if (searchType === "go" || findBy === "GO") {
      return columnsGo;
    } else if (searchType === "interpro" || findBy === "INTERPRO") {
      return columnsInterpro;
    } else if (searchType === "kegg" || findBy === "KEGG") {
      return columnsKegg;
    }
  };

  const columns = getColumns(searchType);

  return (
    <>
      <Head>
        <title>Search result</title>
      </Head>
      <Layout>
        <div className={styles.query}>
          Query: <strong>{searchTerm}</strong>
        </div>
        <div
          className={(styles.tableContainer, "-highlight")}
          style={{ margin: "1rem" }}
        >
          {loading ? (
            <h2 className={styles.loading}>Loading...</h2>
          ) : resData.length === 0 ? (
            <h2 className={styles.loading}>No Result</h2>
          ) : (
            <ReactTable
              className="-highlight"
              data={resData}
              columns={columns}
            />
          )}
        </div>
      </Layout>
    </>
  );
}

const columnsGene = [
  {
    Header: "Gene",
    accessor: "name",
    Cell: (props) => (
      <Link href={`genes/${props.value}`}>
        <a className={styles.geneCell}>{props.value}</a>
      </Link>
    ),
    width: 175,
  },
  {
    id: "ncbi",
    Header: "NCBI ID",
    accessor: (row) => {
      return { ncbi_id: row.ncbi_display_id, gene_id: row.name };
    },
    Cell: (props) => (
      <Link href={`genes/${props.value.gene_id}`}>
        <a className={styles.geneCell}>{props.value.ncbi_id}</a>
      </Link>
    ),
    width: 175,
  },
  {
    Header: "Description",
    accessor: "description",
    // width: 300,
    Cell: (props) => <div className={styles.descriptionRow}>{props.value}</div>,
  },
  {
    Header: "Species",
    accessor: "assembly.species",
    width: 225,
  },
  {
    Header: "Chromosome",
    accessor: "seq_region.name",
    Cell: (props) => <div className={styles.geneCell}>{props.value}</div>,
    width: 150,
  },
  {
    Header: "Length",
    accessor: "size",
    width: 100,
  },
];

const columnsGo = [
  {
    Header: "Gene",
    accessor: "name",
    Cell: (props) => (
      <Link href={`genes/${props.value}`}>
        <a className={styles.geneCell}>{props.value}</a>
      </Link>
    ),
    width: 175,
  },
  {
    id: "ncbi",
    Header: "NCBI ID",
    accessor: (row) => {
      return { ncbi_id: row.ncbi_display_id, gene_id: row.name };
    },
    Cell: (props) => (
      <Link href={`genes/${props.value.gene_id}`}>
        <a className={styles.geneCell}>{props.value.ncbi_id}</a>
      </Link>
    ),
    width: 175,
  },
  {
    Header: "GO",
    accessor: "goDisplay",
    Cell: (props) => <div className={styles.descriptionRow}>{props.value}</div>,
  },
  {
    Header: "Description",
    accessor: "description",
    Cell: (props) => <div className={styles.descriptionRow}>{props.value}</div>,
  },
  {
    Header: "Species",
    accessor: "assembly.species",
    width: 225,
  },
  {
    Header: "Chromosome",
    accessor: "seq_region.name",
    Cell: (props) => <div className={styles.geneCell}>{props.value}</div>,
    width: 150,
  },
  {
    Header: "Length",
    accessor: "size",
    width: 100,
  },
];

const columnsInterpro = [
  {
    Header: "Gene",
    accessor: "name",
    Cell: (props) => (
      <Link href={`genes/${props.value}`}>
        <a className={styles.geneCell}>{props.value}</a>
      </Link>
    ),
    width: 175,
  },
  {
    id: "ncbi",
    Header: "NCBI ID",
    accessor: (row) => {
      return { ncbi_id: row.ncbi_display_id, gene_id: row.name };
    },
    Cell: (props) => (
      <Link href={`genes/${props.value.gene_id}`}>
        <a className={styles.geneCell}>{props.value.ncbi_id}</a>
      </Link>
    ),
    width: 175,
  },
  {
    Header: "Interpro",
    accessor: "domainAnnotationDisplay",
    Cell: (props) => <div className={styles.descriptionRow}>{props.value}</div>,
  },
  {
    Header: "Description",
    accessor: "description",
    Cell: (props) => <div className={styles.descriptionRow}>{props.value}</div>,
  },
  {
    Header: "Species",
    accessor: "assembly.species",
    width: 225,
  },
  {
    Header: "Chromosome",
    accessor: "seq_region.name",
    Cell: (props) => <div className={styles.geneCell}>{props.value}</div>,
    width: 150,
  },
  {
    Header: "Length",
    accessor: "size",
    width: 100,
  },
];

const columnsKegg = [
  {
    Header: "Gene",
    accessor: "name",
    Cell: (props) => (
      <Link href={`genes/${props.value}`}>
        <a className={styles.geneCell}>{props.value}</a>
      </Link>
    ),
    width: 175,
  },
  {
    id: "ncbi",
    Header: "NCBI ID",
    accessor: (row) => {
      return { ncbi_id: row.ncbi_display_id, gene_id: row.name };
    },
    Cell: (props) => (
      <Link href={`genes/${props.value.gene_id}`}>
        <a className={styles.geneCell}>{props.value.ncbi_id}</a>
      </Link>
    ),
    width: 175,
  },
  {
    Header: "Kegg",
    accessor: "keggAccession",
    width: 200,
    Cell: (props) => <div className={styles.descriptionRow}>{props.value}</div>,
  },
  {
    Header: "Description",
    accessor: "description",
    Cell: (props) => <div className={styles.descriptionRow}>{props.value}</div>,
  },
  {
    Header: "Species",
    accessor: "assembly.species",
    width: 225,
  },
  {
    Header: "Chromosome",
    accessor: "seq_region.name",
    Cell: (props) => <div className={styles.geneCell}>{props.value}</div>,
    width: 150,
  },
  {
    Header: "Length",
    accessor: "size",
    width: 100,
  },
];
