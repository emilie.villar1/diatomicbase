import React from "react";
import Head from "next/head";
import styles from "./genomeBrowser.module.scss";
import dynamic from "next/dynamic";
import Layout from "../components/Layout";
import blinkTrackButton from "../utils/blinkTrackButton";

const DynamicGenomeLinearView = dynamic(
  () => import("../components/GenomeLinearView"),
  {
    ssr: false,
    loading: () => (
      <div className={styles.placeholder}>
        <img src="placeholder_browser.svg" />
      </div>
    ),
  }
);

export default function GenomeBrowser() {
  const defaultSpecies = "Phaeodactylum tricornutum";
  return (
    <>
      <Head>
        <title>Genome Browser</title>
      </Head>
      <Layout>
        <div className={styles.jbrowse}>
          <p className={styles.svg}>
            Click on
            <svg
              className={styles.pointer}
              focusable="false"
              viewBox="0 0 24 24"
              aria-hidden="true"
              onClick={blinkTrackButton}
            >
              <path d="M21 19v-2H8v2h13m0-6v-2H8v2h13M8 7h13V5H8v2M4 5v2h2V5H4M3 5a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1H4a1 1 0 01-1-1V5m1 6v2h2v-2H4m-1 0a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1H4a1 1 0 01-1-1v-2m1 6v2h2v-2H4m-1 0a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1H4a1 1 0 01-1-1v-2z"></path>
            </svg>
            (top left) to display additional information on the browser: Phatr2
            prediction, ecotype variants, histone marks, ncRNAs...
          </p>
          <DynamicGenomeLinearView
            species={defaultSpecies}
            defaultSession={defaultSession}
            location="1:10000..200000"
          />
        </div>
      </Layout>
    </>
  );
}

const defaultSession = {
  name: "My session",
  view: {
    id: "linearGenomeView",
    type: "LinearGenomeView",
    tracks: [
      {
        type: "ReferenceSequenceTrack",
        configuration: "ASM15095v2-ReferenceSequenceTrack",
        displays: [
          {
            type: "LinearReferenceSequenceDisplay",
            height: 140,
          },
        ],
      },
      {
        type: "FeatureTrack",
        configuration: "ASM15095v2-genes",
        displays: [
          {
            type: "LinearBasicDisplay",
            height: 350,
          },
        ],
      },
    ],
  },
};
