import React from "react";
import Head from "next/head";
import { useRouter } from "next/router";
import { GetStaticProps } from "next";
import ReactTable from "react-table-v6";
import styles from "./resources.module.scss";
import { TranscriptomicsProps } from "./transcriptomics";
import apiUrl from "../utils/getApiUrl";
import Layout from "../components/Layout";
import { extractPublication, formatEtAl } from "../utils/publications";

export default function Resources({ rnaSeqMetaData }: TranscriptomicsProps) {
  const router = useRouter();
  const transcriptomicsDl = (event: React.MouseEvent<HTMLButtonElement>) => {
    router.push({
      pathname: `/resources/transcriptomics/${event.currentTarget.id}`,
    });
  };

  const columns = [
    {
      Header: "BioProject Accession",
      accessor: "BioProject",
      width: 200,
    },
    {
      Header: "Description",
      accessor: "Description",
      Cell: (props) => (
        <div className={styles.descriptionRow}>{props.value}</div>
      ),
    },
    {
      id: "publication",
      Header: "Publication",
      width: 500,
      accessor: (row) => {
        return { Publication: row.Publication, doi: row.doi };
      },
      Cell: (props) => {
        const listPublications = extractPublication(
          props.value.Publication,
          props.value.doi
        );
        return (
          <div className={styles.descriptionRow}>
            {listPublications.map((value, i) => (
              <div style={{ marginBottom: "0.5em" }} key={i}>
                {value.doi === "NA" ? (
                  <div>{value.Publication}</div>
                ) : (
                  <a href={value.doi} target="_blank" rel="noopener noreferrer">
                    {formatEtAl(value.Publication)}
                  </a>
                )}
              </div>
            ))}
          </div>
        );
      },
    },
    {
      Header: "Link",
      accessor: "BioProject",
      width: 100,
      Cell: (props) => (
        <button
          id={props.value}
          className={styles.dlButton}
          onClick={transcriptomicsDl}
        >
          Download
        </button>
      ),
    },
  ];

  return (
    <>
      <Head>
        <title>Resources</title>
      </Head>
      <Layout>
        <div className={styles.main}>
          <h2>Assembly</h2>
          <hr />
          <p>
            An international group of researchers in collaboration with DOE's
            Joint Genome Institute has sequenced the genome of P. tricornutum
            using whole genome shotgun (WGS) sequencing. The clone of P.
            tricornutum that was sequenced is CCAP1055/1 and is available from
            the Culture collection of Algae and Protozoa
            (http://www.ccap.ac.uk). This clone represents a monoclonal culture
            derived from a fusiform cell in May 2003 from strain CCCP632, which
            was originally isolated in 1956 off Blackpool (U.K.). It has been
            maintained in culture continuously in F/2 medium. The 27.4 Mb genome
            assembly contains 33 chromosomes and XX scaffolds.
          </p>
          <p>
            Reference:
            <a
              href="https://doi.org/10.1038/nature07410"
              style={{ paddingLeft: "10px" }}
            >
              Bowler C, Allen AE, Badger JH, et al. The Phaeodactylum genome
              reveals the evolutionary history of diatom genomes. Nature. 2008
              Nov;456(7219):239-244.
            </a>
          </p>
          {Object.entries(assembly).map(([regionName, dl], i) => {
            return (
              <p key={i} className={styles.dl}>
                <a href={dl}> - Download {regionName} - </a>
              </p>
            );
          })}
          <h2>Gene Annotation</h2>
          <hr />
          <div>
            The Phaeodactylum tricornutum genome is a reannotation with 12,089
            gene models predicted by usnig existing gene models, expression data
            and protein sequences from related species used to train the SNAP
            and Augustus gene prediction programs using the MAKER2 annotation
            pipeline. The inputs were:
            <ul>
              <li>10,402 gene models from a previous genebuild from JGI</li>
              <li>13,828 non-redundant ESTs</li>
              <li>
                42 libraries of RNA-Seq generated using Illumina technology
              </li>
              <li>
                49 libraries of RNA-Seq data generated under various iron
                conditions using SoLiD technology{" "}
              </li>
              <li>
                93,206 Bacillariophyta ESTs from dbEST, 22,502 Bacillariophyta
                and 118,041 Stramenopiles protein sequences from UniProt.
              </li>
            </ul>
          </div>
          <p>
            Reference:
            <a
              href="https://doi.org/10.1038/s41598-018-23106-x"
              style={{ paddingLeft: "10px" }}
            >
              Rastogi, A., Maheswari, U., Dorrell, R.G. et al. Integrative
              analysis of large scale transcriptome data draws a comprehensive
              landscape of Phaeodactylum tricornutum genome and evolutionary
              origin of diatoms. Sci Rep 8, 4834 (2018).
            </a>
          </p>
          {Object.entries(annotations).map(([typeAnnotation, dl], i) => {
            return (
              <p key={i} className={styles.dl}>
                {" "}
                <a href={dl}> - Download {typeAnnotation} - </a>
              </p>
            );
          })}
          <h2>Transcriptomics</h2>
          <hr />
          <ReactTable
            className={(styles.table, "-highlight")}
            defaultPageSize={20}
            columns={columns}
            data={rnaSeqMetaData}
          />
        </div>
      </Layout>
    </>
  );
}

export const getStaticProps: GetStaticProps = async () => {
  const result = await fetch(apiUrl + "/rna_seq/");
  const rnaSeqMetaData = await result.json();
  return {
    props: { rnaSeqMetaData },
  };
};

const assembly = {
  "all genomic content":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.toplevel.fa.gz",
  chloroplast:
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.chloroplast.fa.gz",
  "Contig 1	":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.1.fa.gz",
  "Contig 2	":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.2.fa.gz",
  "Contig 3	":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.3.fa.gz",
  "Contig 4	":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.4.fa.gz",
  "Contig 5	":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.5.fa.gz",
  "Contig 6	":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.6.fa.gz",
  "Contig 7	":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.7.fa.gz",
  "Contig 8	":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.8.fa.gz",
  "Contig 9	":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.9.fa.gz",
  "Contig 10":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.10.fa.gz",
  "Contig 11":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.11.fa.gz",
  "Contig 12":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.12.fa.gz",
  "Contig 13":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.13.fa.gz",
  "Contig 14":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.14.fa.gz",
  "Contig 15":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.15.fa.gz",
  "Contig 16":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.16.fa.gz",
  "Contig 17":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.17.fa.gz",
  "Contig 18":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.18.fa.gz",
  "Contig 19":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.19.fa.gz",
  "Contig 20":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.20.fa.gz",
  "Contig 21":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.21.fa.gz",
  "Contig 22":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.22.fa.gz",
  "Contig 23":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.23.fa.gz",
  "Contig 24":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.24.fa.gz",
  "Contig 25":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.25.fa.gz",
  "Contig 26":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.26.fa.gz",
  "Contig 27":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.27.fa.gz",
  "Contig 28":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.28.fa.gz",
  "Contig 29":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.29.fa.gz",
  "Contig 30":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.30.fa.gz",
  "Contig 31":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.31.fa.gz",
  "Contig 32":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.32.fa.gz",
  "Contig 33":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.33.fa.gz",
  "bottom drawer":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna/Phaeodactylum_tricornutum.ASM15095v2.dna.nonchromosomal.fa.gz",
};

const annotations = {
  " all genomic content":
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/gff3/phaeodactylum_tricornutum/Phaeodactylum_tricornutum.ASM15095v2.48.gff3.gz",
  CDNA:
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/cdna/Phaeodactylum_tricornutum.ASM15095v2.cdna.all.fa.gz",
  CDS:
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/cds/Phaeodactylum_tricornutum.ASM15095v2.cds.all.fa.gz",
  DNA:
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/dna_index/Phaeodactylum_tricornutum.ASM15095v2.dna.toplevel.fa.gz",
  NCRNA:
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/ncrna/Phaeodactylum_tricornutum.ASM15095v2.ncrna.fa.gz",
  PEP:
    "ftp://ftp.ensemblgenomes.org/pub/protists/release-48/fasta/phaeodactylum_tricornutum/pep/Phaeodactylum_tricornutum.ASM15095v2.pep.all.fa.gz",
};
