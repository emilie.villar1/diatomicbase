import Head from "next/head";
import Link from "next/link";
import styles from "./index.module.scss";
import Layout from "../components/Layout";

export default function Home() {
  return (
    <div>
      <Head>
        <title>DiatOmicBase</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Layout>
        <div className={styles.container}>
          <div className={styles.textContainer}>
            <div>
              <strong>DiatOmicBase</strong> is a genome portal to perform
              research on Phaeodactylum tricornutum, gathering comprehensive
              omic resources to ease the exploration of dispersed public
              datasets.
              <br />
              Here you will find:
              <ul>
                <li>
                  <Link href="/searchPage">Genes pages</Link>: each Phatr3 gene
                  model page displays cDNA and protein sequences, a genome
                  browser, comparisons of gene expression in public RNA-Seq
                  datasets, co-expression networks as well as several domains
                  annotation. Gene pages can be accessed through the general
                  search bar in the header, or through specific search (e.g.
                  domaines, annotation) or blast.
                </li>
                <li>
                  A{" "}
                  <Link href="/genomeBrowser">
                    <a>genome browser</a>
                  </Link>{" "}
                  with the latest annotation updates: Phatr2 and Phatr3 gene
                  models, ecotype variants, histone marks, non-coding RNAs,
                  domains.
                </li>
                <li>
                  A{" "}
                  <Link href="/transcriptomics">
                    <a href="">transcriptomic module</a>
                  </Link>{" "}
                  where you can re-analyse published RNA-Seq datasets.
                </li>
                <li>
                  A{" "}
                  <Link href="/resources">
                    <a href="">resource page</a>
                  </Link>{" "}
                  to download the latest omic resources available.
                </li>
              </ul>
              <p>
                <strong>Note about RNA-Seq analysis:</strong>
                <br />
                We precomputed Differential Expression for a hundred pairwise
                comparisons corresponding to 24 already published BioProjects,
                using the{" "}
                <a href="https://github.com/nf-core/rnaseq">
                  nf-core rnaseq
                </a>{" "}
                pipeline and DESeq2 R package (see Methods). Results are
                displayed by gene (for all the comparisons) on gene pages, and
                by experiment (for all the genes) on the{" "}
                <Link href="/resources">resource page</Link>. Read densities are
                shown on the genome browser of gene pages only for significant
                comparisons. If you want to compare other conditions, please use
                the <Link href="/transcriptomics">transcriptomic module</Link>{" "}
                and choose the subsets to compare.
              </p>
              <p>
                <strong>Future directions:</strong>
                <ul>
                  <li>
                    We would like to implement the transcriptomic module with
                    the possibility for user to analyse its own data.
                  </li>
                  <li>
                    We will add other species like Thalassiosira pseudonana and
                    Pseudo-Nitzschia multistriata
                  </li>
                </ul>
                Don’t hesitate to contact the team for any suggestions:{" "}
                <a href="mailto: diatomicbase@bio.ens.psl.eu">
                  diatomicbase@bio.ens.psl.eu
                </a>
              </p>
              <br />
              <br />
              DiatOmicBase is funded by the Gordon and Betty Moore Foundation.
              <br />
              The steering committee consists of Chris Bowler (ENS France),
              Angela Falciatore (IBPC France), Klaas Vandepoele (VIB-UGent,
              PLAZA), Michele Fabris (University of Technology Sydney/CSIRO
              Synthetic Biology FSP; DiatomCyc).
              <br />
              The project is managed by Emilie Villar and developed by Nathanael
              Zweig.
            </div>
            <div>
              <div>
                <a href="https://www.moore.org/">
                  <img
                    className={styles.logo}
                    src="moore-logo-color-copy.jpg"
                    alt="Moor logo transcparent"
                  />
                </a>
                <a href="https://www.ens.psl.eu/">
                  <img
                    className={styles.logo}
                    src="ENS_Logo_TL.jpg"
                    alt="Logo ENS"
                  />
                </a>
                <a href="https://www.ibens.ens.fr/?lang=fr">
                  <img
                    className={styles.logo}
                    src="ibens.png"
                    alt="IBENS logo"
                  />
                </a>
                <a href="https://psl.eu/">
                  <img className={styles.logo} src="PSL.jpg" alt="PSL logo" />
                </a>
              </div>
              <div>
                <a href="https://www.cnrs.fr/">
                  <img className={styles.logo} src="CNRS.jpg" alt="CNRS logo" />
                </a>
                <a href="http://www.ibpc.fr/en/">
                  <img className={styles.logo} src="ibpc.png" alt="IBPC logo" />
                </a>
                <a href="https://www.sorbonne-universite.fr/">
                  <img
                    className={styles.logo}
                    src="Sorbonne.png"
                    alt="Sorbonne University logo"
                  />
                </a>
              </div>
            </div>
          </div>
          <div className={styles.imageContainer}>
            <img
              src="/Phaeodactylum_tricornutum.jpg"
              alt="Phaeodactylum picture"
            />
            <p>
              Picture credit: Image showing four fusiform morphotype cells of P.
              tricornutum, Taken by De Martino.A and Bowler.C, Departement de
              Biologie, Ecole Normale Superieure, Paris, France
            </p>
          </div>
        </div>
      </Layout>
    </div>
  );
}
