import React, { useState, useEffect } from "react";
import Head from "next/head";
import Navbar from "../../components/NavBar";
import DomainBar from "../../components/DomainBar";
import Dropdown from "../../components/Dropdown";
import GeneExpression from "../../components/GeneExpression";
import styles from "./[gene].module.scss";
import InfoToggle from "../../components/InfoToggle";
import CoExpressionNetworks from "../../components/CoExpressionNetworks";
import Phaeonet from "../../components/Phaeonet";
import dynamic from "next/dynamic";
import apiUrl from "../../utils/getApiUrl";
import Layout from "../../components/Layout";
import blinkTrackButton from "../../utils/blinkTrackButton";

export interface geneInfoProps {
  name: string;
  start: number;
  end: number;
  KEGG?: string;
  strand: number;
  biotype: string;
  species: string;
  eco: string | null;
  embl: string | null;
  kegg_id: number | null;
  uniprotkb: string | null;
  description: string | null;
  NCBI: string | null;
  ncbi_internal_id: string | null;
  seq_region: {
    id: number;
    dna_id: number;
    name: string;
  };
  go?: [
    {
      term: string;
      type: "C" | "F" | "P";
      annotation: string;
    }
  ];
  domains: [domain];
  domains_annotation?: [
    {
      db_name: string;
      description: string;
      id: number;
      accession: string;
    }
  ];
}

interface domain {
  start: number;
  end: number;
  id: number;
  annotation_id: number;
  gene_id: number;
}

export interface assemblyInterface {
  name: string;
  sequence: {
    type: string;
    trackId: string;
    adapter: {
      type: string;
      fastaLocation: {
        uri: string;
      };
      faiLocation: {
        uri: string;
      };
      gziLocation: {
        uri: string;
      };
    };
  };
}

export interface tracksInterface {
  type: string;
  trackId: string;
  name: string;
  category: string[];
  assemblyNames: string[];
  adapter: {
    type: string;
    gffGzLocation: {
      uri: string;
    };
    index: {
      location: {
        uri: string;
      };
      indexType: string;
    };
  };
  renderer?: {
    type: string;
  };
}

const DynamicGenomeLinearView = dynamic(
  () => import("../../components/GenomeLinearView"),
  {
    ssr: false,
    loading: () => (
      <div className={styles.placeholder}>
        <img src="../../placeholder_browser.svg" />
      </div>
    ),
  }
);

export default function Gene() {
  const [geneInfo, setGeneInfo] = useState<geneInfoProps>();

  useEffect(() => {
    // Get geneInfo using geneName in url
    const geneName = window.location.pathname.split("/").pop();

    async function fetchGeneInfo(geneName: string): Promise<geneInfoProps> {
      const response = await fetch(apiUrl + "/gene/" + geneName);
      const geneInfo = await response.json();
      return geneInfo;
    }

    const getGeneInfo = async (geneName) => {
      // Get geneInfo and setup it in state
      const geneInfo = await fetchGeneInfo(geneName);
      setGeneInfo(geneInfo);
      console.log(geneInfo);
    };

    getGeneInfo(geneName);
  }, []);

  const geneName = geneInfo !== undefined ? geneInfo.name : undefined;

  return (
    <>
      <Head>
        <title>{geneInfo ? geneInfo.name : "Gene"}</title>
      </Head>
      <Layout>
        <div className={styles.main}>
          <GeneDescription geneInfo={geneInfo} />
          <GenomeBrowserGene
            geneInfo={geneInfo}
            defaultSession={defaultSession}
          />
          <GeneExpression geneName={geneName} />
          <GoTerms geneInfo={geneInfo} />
          <Domains geneInfo={geneInfo} />
          <CoExpressionDisplay geneInfo={geneInfo} />
        </div>
      </Layout>
    </>
  );
}

function GeneDescription({ geneInfo }: { geneInfo: geneInfoProps }) {
  const [additionalIdPresent, setAdditionalIdPresent] = useState<string[]>([]);

  useEffect(() => {
    // Find which additional id is present
    function getAdditionalIds(geneInfo: geneInfoProps) {
      const additionalIdList = ["embl", "uniprotkb", "KEGG", "NCBI"];
      let tmpIdPresent: string[] = [];
      additionalIdList.forEach((id) => {
        if (geneInfo[id] !== null && geneInfo[id] !== "") {
          tmpIdPresent.push(id);
        }
      });
      setAdditionalIdPresent(tmpIdPresent);
    }
    if (geneInfo !== undefined) getAdditionalIds(geneInfo);
  }, [geneInfo]);

  const position = geneInfo && getPosition(geneInfo);
  const geneName = geneInfo !== undefined ? geneInfo.name : undefined;
  return (
    <>
      <div className={styles.geneAndDl}>
        <h3>Gene: {geneInfo && geneInfo.name}</h3>
        <Dropdown
          name="Download"
          contents={[
            <a
              href={`${apiUrl}/gene/download/fasta/${geneName}.fa`}
              target="_blank"
              rel="noopener noreferrer"
              download
            >
              Gene Fasta{" "}
            </a>,
            <a
              href={`${apiUrl}/gene/download/fasta/${geneName}.fa?type_seq=protein`}
              target="_blank"
              rel="noopener noreferrer"
              download
            >
              Protein Fasta{" "}
            </a>,
            <a
              href={`${apiUrl}/gene/download/vcf/${geneName}.vcf`}
              target="_blank"
              rel="noopener noreferrer"
              download
            >
              Variants VCF{" "}
            </a>,
          ]}
        />
      </div>
      <p>Description: {geneInfo && geneInfo.description}</p>
      <p>Biotype: {geneInfo && geneInfo.biotype.replace("_", " ")}</p>
      <p>Position: {geneInfo && position}</p>
      <p className={styles.additionalIdRow}>
        Additional ID:
        <span className={styles.additionalIdFlex}>
          {geneInfo &&
            additionalIdPresent.map((id, i) => (
              <span key={i} className={styles.additionalId}>
                {id}:{" "}
                {id === "NCBI" ? (
                  <a
                    href={getHref(id, geneInfo.ncbi_internal_id)}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    {geneInfo[id]}
                  </a>
                ) : (
                  <a
                    href={getHref(id, geneInfo[id])}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    {geneInfo[id]}
                  </a>
                )}
              </span>
            ))}
        </span>
      </p>
    </>
  );
}

interface GenomeBrowserGeneProps {
  geneInfo: geneInfoProps;
  defaultSession: any;
}

function GenomeBrowserGene({
  geneInfo,
  defaultSession,
}: GenomeBrowserGeneProps) {
  const [location, setLocation] = useState("");
  const [assemblyAndTracks, setAssemblyAndTracks] = useState<{
    assembly: assemblyInterface;
    tracks: tracksInterface[];
  }>();

  // Will need Update when multiple species will be added
  function getDefaultCoordinates(geneInfo: geneInfoProps): string {
    const chrom = geneInfo.seq_region.name;
    const length = geneInfo.end - geneInfo.start;
    const start = geneInfo.start - length * 3;
    const end = geneInfo.end + length * 3;
    const location = `${chrom}:${start}..${end}`;
    return location;
  }

  useEffect(() => {
    const fetchData = async (geneInfo: geneInfoProps) => {
      // Retrieve assembly and tracks
      const { assembly, tracks } = await fetchAssemblyAndTracks(
        geneInfo.species,
        geneInfo.name
      );
      // Setup defaultSession correctly, and get location
      setLocation(getDefaultCoordinates(geneInfo));
      setAssemblyAndTracks({ assembly, tracks });
    };
    if (geneInfo !== undefined) fetchData(geneInfo);
  }, [geneInfo]);

  return (
    <>
      <h3>Genome Browser</h3>
      <hr />
      <p>
        Click on
        <TrackSelectorIcon onClick={blinkTrackButton} />
        (top left) to display additional information on the browser: Phatr2
        prediction, ecotype variants, histone marks, ncRNAs...
      </p>
      {assemblyAndTracks && (
        <DynamicGenomeLinearView
          defaultSession={defaultSession}
          location={location}
          assembly={assemblyAndTracks.assembly}
          tracks={assemblyAndTracks.tracks}
          selectedGene={[geneInfo.name, geneInfo.NCBI]}
        />
      )}
    </>
  );
}

function TrackSelectorIcon(props) {
  return (
    <svg
      {...props}
      className={styles.svg}
      focusable="false"
      viewBox="0 0 24 24"
      aria-hidden="true"
    >
      <path d="M21 19v-2H8v2h13m0-6v-2H8v2h13M8 7h13V5H8v2M4 5v2h2V5H4M3 5a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1H4a1 1 0 01-1-1V5m1 6v2h2v-2H4m-1 0a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1H4a1 1 0 01-1-1v-2m1 6v2h2v-2H4m-1 0a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1H4a1 1 0 01-1-1v-2z"></path>
    </svg>
  );
}

function GoTerms({ geneInfo }) {
  return (
    <>
      <h3>Go term</h3>
      <hr />
      {geneInfo &&
        geneInfo.go.sort(sortGo).map((go, i) => {
          return (
            <div
              key={i}
              className={`${styles.goAndDomainContainer} ${getClassForGoType(
                go.type
              )}`}
            >
              <div className={styles.goGridContainer}>
                <p>
                  {go.type === "C"
                    ? "Cellular Component"
                    : go.type === "P"
                    ? "Molecular Function"
                    : go.type === "F"
                    ? "Biological Process"
                    : ""}
                </p>
                <a
                  href={getHref("go", go.term)}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {go.term}
                </a>
                <p>{go.annotation}</p>
              </div>
            </div>
          );
        })}
      <br />
    </>
  );
}

function Domains({ geneInfo }) {
  // Return domain annotation if exist
  function findDomainForAnnotationId(id: number): domain | null {
    for (let i = 0; i < geneInfo.domains.length; i++) {
      if (geneInfo.domains[i].annotation_id === id) {
        return geneInfo.domains[i];
      }
    }
    return null;
  }
  // Inform if the domain posses information about position
  function domainPositionIsPresent(id: number): boolean {
    const domain = findDomainForAnnotationId(id);
    if (domain === null) {
      return false;
    } else {
      return true;
    }
  }

  function renderDomainBar(id: number): JSX.Element {
    const domain = findDomainForAnnotationId(id);
    return (
      <DomainBar
        start={domain.start}
        end={domain.end}
        lengthProtein={(geneInfo.end - geneInfo.start) / 3}
      />
    );
  }

  return (
    <>
      <h3>Domains</h3>
      <hr />
      {geneInfo &&
        geneInfo.domains_annotation.sort(sortDomain).map((annotation, i) => {
          return (
            <div
              key={i}
              className={`${styles.goAndDomainContainer} ${getClassForDomain(
                annotation.db_name
              )}`}
            >
              <div className={styles.domainGridContainer}>
                <p>{annotation.db_name}</p>
                <a
                  href={getHref(annotation.db_name, annotation.accession)}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {annotation.accession}
                </a>
                <p>{annotation.description}</p>
                {domainPositionIsPresent(annotation.id) &&
                  renderDomainBar(annotation.id)}
              </div>
            </div>
          );
        })}
      <br />
    </>
  );
}

function CoExpressionDisplay({ geneInfo }: { geneInfo: geneInfoProps }) {
  const [coExpressionChecked, setcoExpressionChecked] = useState(false);

  return (
    <>
      <div className={styles.expressionHeader}>
        <h3>
          Co-Expression Networks
          <InfoToggle
            checked={coExpressionChecked}
            setChecked={setcoExpressionChecked}
          />
        </h3>
        <div
          className={
            coExpressionChecked ? styles.rnaSeqInfo : styles.rnaSeqInfoHidden
          }
        >
          WGCNA of 187 publically available RNAseq datasets generated under
          varying nitrogen, iron and phosphate growth conditions identified 28
          merged modules of co-expressed genes (
          <a href="https://doi.org/10.3389/fpls.2020.590949">
            Ait-Mohamed et al., 2020
          </a>
          ).
          <br />
          <br />
          Hierarchical clustering of 123 microarrays samples generated during
          silica limitation, acclimation to high light, exposure to cadmium,
          acclimation to light and dark cycles, exposure to a panel of
          pollutants, darkness and re-illumination, exposure to red, blue and
          green light (
          <a href="https://doi.org/10.1016/j.margen.2015.10.011">
            Ashworth et al., 2015
          </a>
          ).
        </div>
      </div>
      <hr />
      {geneInfo && <CoExpressionNetworks geneName={geneInfo.name} />}
      {geneInfo && geneInfo.species === "Phaeodactylum tricornutum" && (
        <>
          <h3>PhaeoNet Supporting informations</h3>
          <hr />
          <Phaeonet geneName={geneInfo.name} />
        </>
      )}
    </>
  );
}

export async function fetchAssemblyAndTracks(
  speciesName: string,
  geneName: string = null
) {
  const response = await fetch(apiUrl + "/jbrowse/assembly/" + speciesName);
  const assembly: assemblyInterface = await response.json();

  let tracks: tracksInterface[];
  if (geneName === null) {
    const responseTrack = await fetch(
      apiUrl + "/jbrowse/tracks/" + speciesName
    );
    tracks = await responseTrack.json();
  } else {
    const responseTrack = await fetch(
      `${apiUrl}/jbrowse/tracks/${speciesName}/${geneName}`
    );
    tracks = await responseTrack.json();
  }
  return { assembly, tracks };
}

function getHref(typeAddId: string, id: string): string {
  if (typeAddId === "uniprotkb") {
    return `https://www.uniprot.org/uniprot/${id}`;
  } else if (typeAddId === "embl") {
    return `https://www.ebi.ac.uk/ena/browser/view/${id}`;
  } else if (typeAddId === "KEGG") {
    return `https://www.genome.jp/dbget-bin/www_bget?pathway+${id}`;
  } else if (typeAddId === "go") {
    return `https://www.ebi.ac.uk/QuickGO/term/${id}`;
  } else if (typeAddId === "NCBI") {
    return `https://www.ncbi.nlm.nih.gov/gene/${id}`;
  } else if (typeAddId === "InterPro") {
    return `https://www.ebi.ac.uk/interpro/entry/InterPro/${id}`;
  } else if (typeAddId === "SUPFAM") {
    const clean_id = id.replace("SSF", "");
    return `http://supfam.org/SUPERFAMILY/cgi-bin/scop.cgi?sunid=${clean_id}`;
  } else if (typeAddId === "Pfam") {
    return `https://pfam.xfam.org/family/${id}`;
  } else if (typeAddId === "PROSITE") {
    return `https://prosite.expasy.org/${id}`;
  } else if (typeAddId === "TIGRFAMs") {
    return `https://www.ncbi.nlm.nih.gov/Structure/cdd/${id}`;
  } else if (typeAddId === "Gene3D") {
    return `http://www.cathdb.info/version/latest/superfamily/${id}`;
  } else if (typeAddId === "PANTHER") {
    return `http://www.pantherdb.org/panther/family.do?clsAccession=${id}`;
  } else if (typeAddId === "SMART") {
    return `http://smart.embl-heidelberg.de/smart/do_annotation.pl?ACC=${id}`;
  } else if (typeAddId === "HAMAP") {
    return `https://hamap.expasy.org/signature/${id}`;
  } else if (typeAddId === "PRINTS") {
    return `http://130.88.97.239/cgi-bin/dbbrowser/sprint/searchprintss.cgi?prints_accn=${id}&display_opts=Prints&category=None&queryform=false&regexpr=off`;
  } else if (typeAddId === "CDD") {
    return `https://www.ncbi.nlm.nih.gov/Structure/cdd/cddsrv.cgi?uid=${id}`;
  } else {
    return "";
  }
}

function getClassForGoType(goType: string) {
  if (goType === "F") {
    return styles.goTypeF;
  } else if (goType === "C") {
    return styles.goTypeC;
  } else if (goType === "P") {
    return styles.goTypeP;
  }
}

function getClassForDomain(db_name: string): string {
  if (db_name === "InterPro") {
    return styles.interpro;
  } else if (db_name === "Gene3D") {
    return styles.gene3d;
  } else if (db_name === "Pfam") {
    return styles.pfam;
  } else if (db_name === "TIGRFAMs") {
    return styles.tigrfam;
  } else if (db_name === "SUPFAM") {
    return styles.supfam;
  } else if (db_name === "PROSITE") {
    return styles.prosite;
  } else if (db_name === "PANTHER") {
    return styles.panther;
  } else if (db_name === "SMART") {
    return styles.smart;
  } else if (db_name === "HAMAP") {
    return styles.hamap;
  } else if (db_name === "PRINTS") {
    return styles.prints;
  } else if (db_name === "CDD") {
    return styles.cdd;
  } else if (db_name == "PIRSF") {
    return styles.pirsf;
  }
}

function sortGo(a, b) {
  if (a.type === "C") {
    return 1;
  } else if (a.type === "F") {
    return -1;
  } else if (a.type == "P") {
    return 1;
  }
}

function sortDomain(a, b) {
  if (b.db_name === "InterPro") {
    return 1;
  } else if (a.db_name === "InterPro") {
    return -1;
  } else if (a.db_name > b.db_name) {
    return 1;
  } else if (a.db_name < b.db_name) {
    return -1;
  } else {
    return 0;
  }
}

function getPosition(geneInfo: geneInfoProps): string {
  let position;
  if (!isNaN(Number(geneInfo.seq_region.name))) {
    position = `Chromosome ${geneInfo.seq_region.name}:${geneInfo.start}-${geneInfo.end}`;
  } else {
    position = `Super-Contig ${geneInfo.seq_region.name}:${geneInfo.start}-${geneInfo.end}`;
  }
  return position;
}

let defaultSession = {
  name: "My session",
  view: {
    id: "linearGenomeView",
    type: "LinearGenomeView",
    tracks: [
      {
        type: "FeatureTrack",
        configuration: "ASM15095v2-genes",
        displays: [
          {
            type: "LinearBasicDisplay",
            height: 250,
            configuration: "ASM15095v2-genes-LinearBasicDisplay",
          },
        ],
      },
      {
        type: "FeatureTrack",
        configuration: "Phatr2_genes_filtered",
        displays: [
          {
            type: "LinearBasicDisplay",
            height: 150,
          },
        ],
      },
    ],
  },
};
