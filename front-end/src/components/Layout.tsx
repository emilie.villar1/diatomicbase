import React, { useState, useEffect } from "react";
import NavBar from "./NavBar";
import Footer from "./Footer";
import Alert from "./Alert";

export default function Layout({ children }) {
  const [ieCompatibilityWarning, setIECompatibilityWarning] = useState(false);

  useEffect(() => {
    if (process.browser !== undefined && isIE()) {
      setIECompatibilityWarning(true);
    }
  }, [process.browser]);

  return (
    <div style={{ minHeight: "100vh" }}>
      <div style={{ minHeight: "calc(100vh - 5em)", paddingBottom: "50px" }}>
        <NavBar />
        {ieCompatibilityWarning && (
          <Alert type="danger" style={{ margin: "1em" }}>
            <strong>Internet Explorer</strong>&nbsp;is not supported, please use
            a more modern web browser, like Firefox or Chrome
          </Alert>
        )}
        {children}
      </div>
      <Footer />
    </div>
  );
}

function isIE() {
  const ua = navigator.userAgent;
  /* MSIE used to detect old browsers and Trident used to newer ones*/
  return ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;
}
