import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChevronCircleUp,
  faChevronCircleDown,
} from "@fortawesome/free-solid-svg-icons";
import styles from "./DownToggle.module.scss";

interface DownToggleProps {
  title: React.ReactNode;
  titleClass?: string;
  className?: string;
  children: React.ReactNode;
}

export default function DownToggle({
  title,
  titleClass,
  className,
  children,
}: DownToggleProps) {
  const [show, setShow] = useState(false);
  return (
    <div className={className}>
      <div className={styles.row}>
        <span className={titleClass}>{title}</span>
        <span className={styles.toggleIcon} onClick={() => setShow(!show)}>
          <FontAwesomeIcon
            icon={show ? faChevronCircleUp : faChevronCircleDown}
          />
        </span>
      </div>
      <div className={show ? "" : styles.hidden}>{children}</div>
    </div>
  );
}
