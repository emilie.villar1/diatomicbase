import React, { useState, useEffect } from "react";
import styles from "./CoExpressionNetworks.module.scss";
import apiUrl from "../utils/getApiUrl";

interface coExpressionNetworksProps {
  geneName: string;
}

interface coExpression {
  color: string;
  kegg: Array<string>;
}

export default function CoExpressionNetworks({
  geneName,
}: coExpressionNetworksProps) {
  const [coExpression, setCoExpression] = useState<coExpression>();

  useEffect(() => {
    async function fetchCoExpressionNetwork(geneName: string) {
      const response = await fetch(apiUrl + "/gene/coexpression/" + geneName);
      const json = await response.json();
      setCoExpression(json);
    }

    fetchCoExpressionNetwork(geneName);
  }, []);

  return (
    <>
      {coExpression &&
        (coExpression.color !== "#N/D" ? (
          <div className={`${styles.card} ${getColor(coExpression.color)}`}>
            <h3
              className={`${styles.cardTitle} ${getColor(coExpression.color)}`}
            >
              {coExpression.color}
            </h3>
            <div className={styles.cardContent}>
              {coExpression.kegg.length !== 0 ? (
                <p className={styles.description}>Enriched in:</p>
              ) : (
                <p className={styles.description}>
                  No enrichissement for DarkSlateBlue
                </p>
              )}
              <br />
              {coExpression.kegg.map((kegg_text, i) => (
                <div key={i}>{formatKeggTextWithHyperLink(kegg_text)}</div>
              ))}
            </div>
          </div>
        ) : (
          <div className="">Not in any coexpression network from phaeonet</div>
        ))}
    </>
  );
}

function formatKeggTextWithHyperLink(kegg_text: string) {
  const regex = /(ko\d*)]$/;
  const kegg_id = regex.exec(kegg_text)[1];
  const kegg_text_without_id = kegg_text.replace(regex, "");
  return (
    <p>
      {kegg_text_without_id}
      <a
        href={`https://www.genome.jp/dbget-bin/www_bget?pathwkay+${kegg_id}`}
        target="_blank"
      >
        {kegg_id}
      </a>
      ]
    </p>
  );
}

function getColor(color: string): string {
  switch (color) {
    case "orange":
      return styles.orange;
    case "cyan":
      return styles.cyan;
    case "salmon":
      return styles.salmon;
    case "green":
      return styles.green;
    case "floralwhite":
      return styles.floralwhite;
    case "darkgrey":
      return styles.darkgrey;
    case "magenta":
      return styles.magenta;
    case "greenyellow":
      return styles.greenyellow;
    case "skyblue":
      return styles.skyblue;
    case "blue":
      return styles.blue;
    case "violet":
      return styles.violet;
    case "brown":
      return styles.brown;
    case "grey":
      return styles.grey;
    case "darkmagenta":
      return styles.darkmagenta;
    case "steelblue":
      return styles.steelblue;
    case "tan":
      return styles.tan;
    case "paleturquoise":
      return styles.paleturquoise;
    case "ivory":
      return styles.ivory;
    case "darkgreen":
      return styles.darkgreen;
    case "red":
      return styles.red;
    case "bisque4":
      return styles.bisque;
    case "black":
      return styles.black;
    case "brown4":
      return styles.darkbrown;
    case "lightcyan1":
      return styles.lightcyan;
    case "lightsteelblue1":
      return styles.lightsteelblue;
    case "mediumpurple3":
      return styles.mediumpurple;
    case "orangered4":
      return styles.orangered;
    case "darkslateblue":
      return styles.darkslateblue;
    default:
      return "";
  }
}
