import React from "react";
import { ReactSortable, MultiDrag } from "react-sortablejs";
import styles from "./BioSampleSelector.module.scss";

export default function BioSampleSelector({
  bioSamples,
  setBioSamples,
  subset1,
  setSubset1,
  subset2,
  setSubset2,
}) {
  return (
    <div className={styles.container}>
      <div className={styles.bioSampleContainer}>
        <h4 className={styles.title}>Biosample</h4>
        <ReactSortable
          className={styles.listBox}
          group="h1"
          multiDrag
          list={bioSamples}
          setList={setBioSamples}
        >
          {bioSamples.map((item) => (
            <div key={item.id} className={styles.item}>
              {item.name}
            </div>
          ))}
        </ReactSortable>
      </div>
      <div className={styles.subsetsContainer}>
        <div className={styles.subset}>
          <h4>Subset 1</h4>
          <ReactSortable
            className={styles.listBox}
            group="h1"
            multiDrag
            list={subset1}
            setList={setSubset1}
          >
            {subset1.map((item) => (
              <div key={item.id} className={styles.item}>
                {item.name}
              </div>
            ))}
          </ReactSortable>
        </div>
        <div className={styles.subset}>
          <h4>Subset 2</h4>
          <ReactSortable
            className={styles.listBox}
            group="h1"
            multiDrag
            list={subset2}
            setList={setSubset2}
          >
            {subset2.map((item) => (
              <div key={item.id} className={styles.item}>
                {item.name}
              </div>
            ))}
          </ReactSortable>
        </div>
      </div>
    </div>
  );
}
