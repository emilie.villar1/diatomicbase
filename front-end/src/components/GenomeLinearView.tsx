import React, { useEffect, useState } from "react";
import { fetchAssemblyAndTracks } from "../pages/genes/[gene]";
import { assemblyInterface, tracksInterface } from "../pages/genes/[gene]";
import "requestidlecallback-polyfill";
import sleep from "../utils/sleep";
import {
  createViewState,
  createJBrowseTheme,
  JBrowseLinearGenomeView,
  ThemeProvider,
} from "@jbrowse/react-linear-genome-view";

interface defaultSessionInterface {
  name: string;
  view: {
    id: string;
    type: string;
    displayRegions?: Array<{
      id: string;
      refName: string;
      assemblyNames: Array<string>;
    }>;
    tracks: Array<{
      type: string;
      configuration: string;
      displays: Array<{
        type: string;
        height?: number;
      }>;
    }>;
  };
}
interface GenomeLinearViewProps {
  species?: string;
  location: string;
  selectedGene?: string[];
  defaultSession: defaultSessionInterface;
  assembly?: assemblyInterface;
  tracks?: tracksInterface[];
}

export default function GenomeLinearView(props: GenomeLinearViewProps) {
  const [state, setState] = useState<{
    location: string;
    defaultSession: defaultSessionInterface;
    assembly?: assemblyInterface;
    tracks?: tracksInterface[];
  }>();

  useEffect(() => {
    const setupGenomeBrowser = async (props: GenomeLinearViewProps) => {
      let { location, defaultSession, assembly, tracks } = props;
      if (tracks === undefined || assembly === undefined) {
        const {
          assembly: fetchedAssembly,
          tracks: fetchedTracks,
        } = await fetchAssemblyAndTracks(props.species);
        assembly = fetchedAssembly;
        tracks = fetchedTracks;
      }

      setState({
        assembly: assembly,
        tracks: tracks,
        location: location,
        defaultSession: defaultSession,
      });
    };
    setupGenomeBrowser(props);

    // Modify genomeBrowser look with js script
    async function doAtIntervalUntilTrue(
      workFn,
      args = {},
      interval = 100,
      toAwait = false
    ) {
      var isReady = false;
      while (isReady === false) {
        if (toAwait) isReady = await workFn(Object.values(args));
        else isReady = workFn(args);
        await sleep(interval);
      }
    }

    const highlightTrackButton = () => {
      const target = document.getElementsByClassName(
        "MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textSecondary MuiButton-textSizeSmall MuiButton-sizeSmall"
      );
      if (target.length !== 0) {
        const button = target[0];
        button.className = `${button.className} selectTracksButton`;
        return true;
      }
      return false;
    };

    // Use the gene name display below the representation (define by 'name' field in gff) to find selected gene.
    // Never return true because user can move in the genome browser far away enough that the gene is re-render
    // when he get back to the selected gene.
    const highlightSelectedGenes = ({
      selectedGenes,
    }: {
      selectedGenes: string[];
    }): boolean => {
      selectedGenes = selectedGenes
        .filter((gene) => gene != null)
        .map((gene) => gene.toLowerCase());
      const targets = document.getElementsByTagName("text");
      if (targets.length !== 0) {
        //@ts-ignore
        for (const target of targets) {
          if (isInList(selectedGenes, target.textContent)) {
            changeColorSibling(target);
          }
        }
      }
      return false;
    };

    const isInList = (selectedGenes: string[], textContent: string) => {
      textContent = textContent.toLowerCase();
      for (const gene of selectedGenes) {
        if (textContent.includes(gene)) return true;
      }
      return false;
    };

    // Recursive function to change gene representation color
    const changeColorSibling = (target: HTMLElement) => {
      const sibling = target.previousSibling as HTMLElement;
      if (sibling === undefined || sibling === null) return;
      if (sibling.hasAttribute("fill")) {
        if (colorAlreadyChanged(sibling)) return;
        const fillColor = sibling.getAttribute("fill");
        if (fillColor === "goldenrod") {
          sibling.setAttribute("fill", "#353b3c");
        } else if (fillColor === "#357089") {
          sibling.setAttribute("fill", "#f2542d");
        }
      }
      changeColorSibling(sibling);
    };

    const colorAlreadyChanged = (target) => {
      const fillColor = target.getAttribute("fill");
      if (fillColor === "#353b3c" || fillColor === "#f2542d") return true;
      else return false;
    };

    // Higlight tracks button and change color of selected gene
    doAtIntervalUntilTrue(highlightTrackButton);
    if (props.selectedGene !== undefined) {
      doAtIntervalUntilTrue(
        highlightSelectedGenes,
        {
          selectedGenes: props.selectedGene,
        },
        450
      );
    }
  }, []);

  ////////////////////////////////////////////////////////////////////////////////////
  // Add Href to to gene page
  ////////////////////////////////////////////////////////////////////////////////////

  const addHrefIfOpenFeatureDetail = (
    fieldNameToTransform: string[],
    transformFn: (el: HTMLElement) => void
  ) => {
    // Wait 20ms for Feature Detail to be rendered
    setTimeout(() => {
      if (FeatureDetailsIsNotOpen()) return;
      const accordions = document.getElementsByClassName(
        "MuiPaper-root MuiAccordion-root Mui-expanded MuiAccordion-rounded MuiPaper-elevation1 MuiPaper-rounded"
      );
      // @ts-ignore
      for (let element of accordions) {
        if (isNotAttributesAccordion(element)) continue;
        const listAttributesEl = getListAttributesEl(element);
        for (let fieldName of fieldNameToTransform) {
          const field = getElInList(listAttributesEl, fieldName);
          transformFn(field);
        }
      }
    }, 20);
  };

  const FeatureDetailsIsNotOpen = () => {
    const bandeau = document.getElementsByClassName(
      "MuiPaper-root MuiAppBar-root MuiAppBar-positionStatic MuiAppBar-colorPrimary MuiPaper-elevation4"
    );
    if (bandeau.length == 0) return true;
    const bandeauTitle = bandeau[0].children[0].children[0].innerHTML;
    if (bandeauTitle.includes("Feature Details")) return false;
    else return true;
  };

  const isNotAttributesAccordion = (accordion: HTMLElement) => {
    return !accordion.children[0].children[0].children[0].innerHTML.includes(
      "Attributes"
    );
  };

  const getListAttributesEl = (accordion: HTMLElement) => {
    return accordion.children[1].children[0].children[0].children[0].children[0]
      .children;
  };

  const getElInList = (elList: HTMLCollection, elName: string) => {
    // @ts-ignore
    for (let el of elList) {
      const fieldName = el.children[0].innerHTML;
      if (!fieldName.includes(elName)) continue;
      return el;
    }
  };

  const addGeneHref = (field: HTMLElement) => {
    const hrefAlreadyPresent = (fieldValueEl: Element) => {
      if (fieldValueEl.children.length !== 0) {
        const possibleHref = fieldValueEl.children[0];
        return possibleHref.tagName === "A" ? true : false;
      }
      return false;
    };

    const fieldValueEl = field.children[1].children[0];

    if (hrefAlreadyPresent(fieldValueEl)) return;
    const fieldValue = fieldValueEl.innerHTML;
    fieldValueEl.innerHTML = `<a href="/genes/${fieldValue}">${fieldValue}</a>`;
  };
  ////////////////////////////////////////////////////////////////////////////////////
  // End add gene page href
  ////////////////////////////////////////////////////////////////////////////////////

  const theme = createJBrowseTheme();
  const viewState =
    state &&
    createViewState({
      assembly: state.assembly,
      tracks: state.tracks,
      location: state.location,
      defaultSession: state.defaultSession,
    });

  return (
    <div style={{ fontFamily: "roboto, sans-serif" }}>
      {state && (
        <ThemeProvider theme={theme}>
          <div
            onClick={() => addHrefIfOpenFeatureDetail(["gene_id"], addGeneHref)}
          >
            {/* For a reason I don't understand using a module.css doesn't work so I use this ugly ass inline css rules */}
            <style>
              {
                ".selectTracksButton {\
                  border: 1px solid #c4c4c4;\
                  background: rgba(255, 255, 255, 0.8);\
                  color: rgba(0, 0, 0, 0.87);\
                }\
                .blink_me {\
                  background-color: yellow;\
                  animation: blinker 0.5s linear infinite;\
                }\
                \
                @keyframes blinker {\
                  50% {\
                    opacity: 0;\
                  }\
                }\
              "
              }
            </style>
            <JBrowseLinearGenomeView viewState={viewState} />
          </div>
        </ThemeProvider>
      )}
    </div>
  );
}
