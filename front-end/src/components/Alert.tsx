import * as React from "react";
import styles from "./Alert.module.scss";

interface alertProps {
  type: "danger";
  children: any;
  style?: React.CSSProperties;
}

export default function Alert(props: alertProps) {
  const { type, children } = props;
  return (
    <div
      className={`${styles.warning} ${type === "danger" && styles.danger}`}
      {...props}
    >
      {children}
    </div>
  );
}
