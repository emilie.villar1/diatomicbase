import React from "react";
import { useRouter } from "next/router";
import styles from "./SearchBar.module.scss";
import useSearch from "../hooks/useSearch";

export default function SearchBar({
  placeholder,
  className,
}: {
  placeholder?: string;
  className?: string;
}) {
  placeholder = placeholder || "Type your gene";
  const router = useRouter();
  const [searchTerm, setSearchTerm, submitSearch] = useSearch(
    "searchbar",
    router
  );

  const submitOnEnter = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === "Enter") {
      submitSearch();
    }
  };

  return (
    <div className={`${className}`}>
      <div className={styles.searchBar}>
        <input
          type="text"
          name=""
          id=""
          placeholder={placeholder}
          value={searchTerm}
          onKeyPress={submitOnEnter}
          onChange={(e) => setSearchTerm(e.target.value)}
        />
        <button type="submit" onClick={submitSearch}>
          Search
        </button>
      </div>
    </div>
  );
}
