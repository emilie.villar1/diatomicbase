import React, { SetStateAction } from "react";
import styles from "./InfoToggle.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faQuestionCircle } from "@fortawesome/free-regular-svg-icons";
import { faQuestionCircle as solidQuestionCircle } from "@fortawesome/free-solid-svg-icons";
import useOuterClick from "../hooks/useOuterClick";

interface InfoToggleProps {
  checked: boolean;
  setChecked: React.Dispatch<SetStateAction<boolean>>;
}

export default function InfoToggle({ checked, setChecked }: InfoToggleProps) {
  const innerRef = useOuterClick(() => setChecked(false));
  return (
    <div ref={innerRef} className={styles.container}>
      <input
        type="checkbox"
        checked={checked}
        onChange={() => setChecked(!checked)}
      />
      <FontAwesomeIcon
        className={styles.icon}
        icon={checked ? solidQuestionCircle : faQuestionCircle}
        onClick={() => setChecked(!checked)}
      />
    </div>
  );
}
