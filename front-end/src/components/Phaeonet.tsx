import React, { useState, useEffect, useRef } from "react";
import styles from "./Phaeonet.module.scss";
import InfoToggle from "./InfoToggle";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheckSquare, faSquare } from "@fortawesome/free-regular-svg-icons";
import apiUrl from "../utils/getApiUrl";

interface phaeonetProps {
  geneName: string;
}

interface phaeonetData {
  polycomb: boolean;
  intron_retention: boolean;
  exon_skipping: boolean;
  asafind: string;
  hectar: string;
  mitofates: string;
  wsort_animal: string;
  wsort_plant: string;
  wsort_fungi: string;
  wsort_consensus: string;
  OP_lineage: string;
  OP_subgroup: string;
  OP_nbtophit: string;
  OP_localdiversity: string;
  SC_lineage: string;
  SC_subgroup: string;
  SC_nbtophit: string;
  SC_localdiversity: string;
}

export default function Phaeonet({ geneName }: phaeonetProps) {
  const [data, setData] = useState<phaeonetData>();
  const [notPresent, setNotPresent] = useState(false);

  useEffect(() => {
    const fetchPhaeoData = async (geneName: string) => {
      const res = await fetch(`${apiUrl}/gene/phaeonet/${geneName}`);
      if (res.status === 404) {
        setNotPresent(true);
        return;
      }
      const json = await res.json();
      setData(json);
    };

    fetchPhaeoData(geneName);
  }, []);

  if (notPresent) {
    return <div className="">Gene not present in phaeonet</div>;
  }

  return (
    <>
      {data && (
        <div className={styles.container}>
          <div className={styles.row}>
            <div className={styles.histoneContainer}>
              <h4 className={styles.underline}>Histone methylation:</h4>
              <span>
                <CheckedIcon
                  style={{ margin: "3px 5px" }}
                  checked={data.polycomb}
                />{" "}
                <InfoDisplay
                  line={"Polycomb marked gene"}
                  description={
                    <>
                      Whether the gene is marked by Polycomb group histone
                      methylation (
                      <a
                        href="https://doi.org/10.3389/fmars.2020.00189"
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        Zhao et al., 2020
                      </a>
                      ).
                    </>
                  }
                  addition={30}
                />
              </span>
            </div>
            <div className={styles.alterSplicing}>
              <h4>
                <InfoDisplay
                  line={
                    <>
                      <span
                        className={styles.underline}
                        style={{ marginRight: "4px" }}
                      >
                        Alternative splicing
                      </span>{" "}
                      under N stress
                    </>
                  }
                  description={
                    <div className={styles.normalFontWeight}>
                      Whether transcripts of the gene are identified to undergo
                      alternative splicing under different N stress conditions (
                      <a
                        href="https://doi.org/10.1038/s41598-018-23106-x"
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        Rastogi et al. 2018
                      </a>
                      ,
                      <a
                        href="https://doi.org/10.1105/tpc.16.00910"
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        McCarthy et al. 2017
                      </a>
                      ).
                    </div>
                  }
                  widthMultiplier={1.5}
                />
              </h4>
              <div className={styles.row}>
                <span className={styles.alterSplicingElement}>
                  <CheckedIcon checked={data.intron_retention} /> Intron
                  retention
                </span>
                <span className={styles.alterSplicingElement}>
                  <CheckedIcon checked={data.exon_skipping} /> Exon skipping
                </span>
              </div>
            </div>
          </div>

          <div className={styles.row}>
            <div className={styles.targetPredictions}>
              <h4 className={styles.underline}>Targeting predictions:</h4>
              <div className={styles.row}>
                <InfoDisplay
                  line={"ASAfind"}
                  description={
                    <>
                      Targeting predictions identified for each protein using
                      ASAFind and SignalP v 3.0 (
                      <a
                        href="https://doi.org/10.1016/j.jmb.2004.05.028"
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        Bendtsen et al., 2004
                      </a>
                      ;{" "}
                      <a
                        href="https://doi.org/10.1111/tpj.12734"
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        Gruber et al.,2015
                      </a>
                      )
                    </>
                  }
                  widthMultiplier={5}
                  addition={20}
                />{" "}
                {data.asafind}
              </div>
              <div className={styles.row}>
                <InfoDisplay
                  line={"HECTAR"}
                  description={
                    <>
                      Targeting predictions identified for each protein using
                      HECTAR (
                      <a
                        href="https://doi.org/10.1186/1471-2105-9-393"
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        Gschloessl et al., 2008
                      </a>
                      )
                    </>
                  }
                  widthMultiplier={5}
                  addition={20}
                />{" "}
                {data.hectar}
              </div>
              <div className={styles.row}>
                <InfoDisplay
                  line={"MitoFates"}
                  description={
                    <>
                      Targeting predictions identified for each protein using a
                      modified version of MitoFates with threshold value 0.35 (
                      <a
                        href="https://doi.org/10.1074/mcp.M114.043083"
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        Fukasawa et al., 2015
                      </a>
                      )
                    </>
                  }
                  widthMultiplier={5}
                  addition={20}
                />{" "}
                {data.mitofates}
              </div>
            </div>
            <div className={styles.wolfPSort}>
              <h4 className={styles.underline}>
                <InfoDisplay
                  line={<span className={styles.underline}>WolfPSort</span>}
                  description={
                    <div className={styles.normalFontWeight}>
                      Targeting predictions identified for each protein using
                      WolfPSort with animal reference libraries (
                      <a
                        href="https://doi.org/10.1016/j.scitotenv.2017.01.190"
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        Horton et al., 2017
                      </a>
                      )
                    </div>
                  }
                  widthMultiplier={3}
                />
              </h4>
              <div className={styles.row}>Animal: {data.wsort_animal}</div>
              <div className={styles.row}>Plant: {data.wsort_plant}</div>
              <div className={styles.row}>Fungi: {data.wsort_fungi}</div>
              <div className={styles.row}>
                CONSENSUS: {data.wsort_consensus}
              </div>
            </div>
          </div>

          <div className={styles.gridContainer}>
            <InfoDisplay
              className={`${styles.evHeader} ${styles.gridCol1} ${styles.gridRow1}`}
              line={<span>Evolutionary origins</span>}
              description={
                <>
                  Probable evolutionary origin of each protein using BLAST top
                  hit analysis of a combined prokaryotic and eukaryotic tree of
                  life, excluding all SAR and CCTH clade members ({" "}
                  <a
                    href="https://doi.org/10.1038/s41598-018-23106-x"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    Rastogi et al. 2018
                  </a>
                  ). The statistics provided are: the lineage, and specific
                  taxonomic sub-group in which the best BLAST hit was
                  identified; and two confidence scores (with thresholds '3' and
                  'yes' for identifying hits of non-contaminant origin).
                </>
              }
              widthMultiplier={5}
              addition={60}
            />
            <div className={`${styles.gridCol1} ${styles.gridRow2}`}>
              Lineage
            </div>
            <InfoDisplay
              className={`${styles.gridCol1} ${styles.gridRow3}`}
              line={"Sub-group"}
              description={
                "Specific taxonomic sub-group in which the best BLAST hit was identified"
              }
              addition={60}
            />
            <InfoDisplay
              className={`${styles.gridCol1} ${styles.gridRow4}`}
              line={"Number of consecutive top hits"}
              description={
                "confidence score (with thresholds '3' for " +
                "identifying hits of non-contaminant origin)"
              }
              addition={60}
            />
            <InfoDisplay
              className={`${styles.gridCol1} ${styles.gridRow5}`}
              line={"Local diversity in top hits"}
              description={
                "confidence scores (with 'yes' for " +
                "identifying hits of non-contaminant origin)"
              }
              addition={60}
            />
            <div className={`${styles.gridCol2} ${styles.gridRow1}`}>
              Excluding ochrophytes and secondary plastids
            </div>
            <div className={`${styles.gridCol2} ${styles.gridRow2}`}>
              {data.OP_lineage}
            </div>
            <div className={`${styles.gridCol2} ${styles.gridRow3}`}>
              {data.OP_subgroup}
            </div>
            <div className={`${styles.gridCol2} ${styles.gridRow4}`}>
              {data.OP_nbtophit}
            </div>
            <div className={`${styles.gridCol2} ${styles.gridRow5}`}>
              {data.OP_localdiversity}
            </div>
            <div className={`${styles.gridCol3} ${styles.gridRow1}`}>
              Excluding SAR and CCTH
            </div>
            <div className={`${styles.gridCol3} ${styles.gridRow2}`}>
              {data.SC_lineage}
            </div>
            <div className={`${styles.gridCol3} ${styles.gridRow3}`}>
              {data.SC_subgroup}
            </div>
            <div className={`${styles.gridCol3} ${styles.gridRow4}`}>
              {data.SC_nbtophit}
            </div>
            <div className={`${styles.gridCol3} ${styles.gridRow5}`}>
              {data.SC_localdiversity}
            </div>
          </div>
        </div>
      )}
    </>
  );
}

function CheckedIcon({
  checked = false,
  className,
  style,
}: {
  checked?: boolean;
  className?: string;
  style?: React.CSSProperties;
}) {
  return (
    <FontAwesomeIcon
      className={className}
      style={style}
      icon={checked ? faCheckSquare : faSquare}
    />
  );
}

interface InfoDisplayProps {
  line: React.ReactNode;
  description: React.ReactNode;
  width?: number;
  widthMultiplier?: number;
  maxWidthPercentage?: number;
  addition?: number;
  className?: string;
}

function InfoDisplay(props: InfoDisplayProps) {
  const {
    line,
    width,
    className,
    description,
    widthMultiplier,
    maxWidthPercentage,
    addition = 0,
  } = props;
  // Display bioproject tooltip when checked
  const [checked, setChecked] = useState(false);
  // Set offset of hidden div with relative information
  const [lineWidth, setLineWidth] = useState(0);
  const ref = useRef<HTMLHeadingElement>(null);
  useEffect(() => {
    setLineWidth(ref.current.offsetWidth);
  }, [ref.current]);

  return (
    <div className={`${styles.rowInfo} ${className}`}>
      <div ref={ref} style={{ display: "flex" }}>
        {line}
        <InfoToggle checked={checked} setChecked={setChecked} />
      </div>
      <div
        className={
          checked ? styles.experienceInfo : styles.experienceInfoHidden
        }
        style={
          maxWidthPercentage !== undefined
            ? {
                left: `${lineWidth + addition}px`,
                maxWidth: `${maxWidthPercentage}%`,
              }
            : width !== undefined
            ? {
                left: `${lineWidth + addition}px`,
                width: `${width}px`,
              }
            : widthMultiplier !== undefined
            ? {
                left: `${lineWidth + addition}px`,
                width: `${(lineWidth + addition) * widthMultiplier}px`,
              }
            : {
                left: `${lineWidth + addition}px`,
                width: `${(lineWidth + addition) * 2}px`,
              }
        }
      >
        {description}
      </div>
    </div>
  );
}
