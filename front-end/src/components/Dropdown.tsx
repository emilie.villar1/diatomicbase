import React, { useState } from "react";
import styles from "./Dropdown.module.scss";
import useOuterClick from "../hooks/useOuterClick";

export default function Dropdown({
  name,
  chevronPos,
  contents,
}: {
  name: string;
  chevronPos?: string;
  contents: Array<React.ReactNode>;
}) {
  // Logic to close dropdown on click outside
  const [checked, setChecked] = useState(false);
  const innerRef = useOuterClick(() => setChecked(false));
  // If multiple dropdown is on the same page, this allow to target the inline
  // CSS for this dropdown in particular if `chevronPos` is used
  const dropdown_css_id = "_" + Math.random().toString(36).substr(2, 9);
  return (
    <label ref={innerRef} className={styles.dropdown}>
      {/* Ugly inline css to modify ddButton::after element top position if needed */}
      {chevronPos && (
        <style>
          {`.${dropdown_css_id}.${styles.ddButton}::after`} {"{"}top:{" "}
          {chevronPos}
          {"}"}
        </style>
      )}
      <div
        className={`${dropdown_css_id} ${styles.ddButton}`}
        onClick={() => setChecked(!checked)}
      >
        {name}
      </div>
      <input
        type="checkbox"
        className={styles.ddInput}
        checked={checked}
        readOnly
      />
      <ul className={styles.ddMenu} onClick={() => setChecked(false)}>
        {contents.map((content, idx) => {
          return <li key={idx}>{content}</li>;
        })}
      </ul>
    </label>
  );
}
