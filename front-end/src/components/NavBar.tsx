import React from "react";
import Link from "next/link";
import SearchBar from "./SearchBar";
import styles from "./Navbar.module.scss";

export default function NavBar() {
  return (
    <>
      <div className={styles.banner}>
        <Link href="/">
          <a className={styles.bannerTitle}>DiatOmicBase</a>
        </Link>
        <SearchBar />
      </div>
      <div className={styles.topnav}>
        <Link href="/">
          <a>Home</a>
        </Link>
        <Link href="/searchPage">
          <a>Gene Page</a>
        </Link>
        <Link href="/genomeBrowser">
          <a>Genome Browser</a>
        </Link>
        <Link href="/resources">
          <a>Resources</a>
        </Link>
        <Link href="/transcriptomics">
          <a>Transcriptomics</a>
        </Link>
        <Link href="/help">
          <a>Help</a>
        </Link>
      </div>
    </>
  );
}
