import React from "react";
import styles from "./DomainBar.module.scss";

export default function DomainBar({
  start,
  end,
  lengthProtein,
}: {
  start: number;
  end: number;
  lengthProtein: number;
}) {
  const lengthDomain = end - start;
  const percentStart = (start / lengthProtein) * 100;
  const percentDomain = (lengthDomain / lengthProtein) * 100;
  return (
    <svg className={styles.svg}>
      <rect
        className={styles.rect}
        x={`${percentStart}%`}
        width={`${percentDomain}%`}
      ></rect>
    </svg>
  );
}
