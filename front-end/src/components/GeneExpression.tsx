import React, { useState, useEffect, useRef } from "react";
import Link from "next/link";
import InfoToggle from "./InfoToggle";
import DownToggle from "./DownToggle";
import { HorizontalBar } from "react-chartjs-2";
import styles from "./GeneExpression.module.scss";
import byKey from "natural-sort-by-key";
import Dropdown from "./Dropdown";
import apiUrl from "../utils/getApiUrl";
import { extractPublication, formatEtAl } from "../utils/publications";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimesCircle } from "@fortawesome/free-regular-svg-icons";

interface expressionDataInterface {
  comp_id: string;
  keyword: string;
  log2FC: number | string;
  pvalue: number;
  status: "UP" | "DOWN" | "NA" | "NS";
  bioproject: string;
  description: string;
  comp: string;
  publication: string;
  doi: string;
}

interface expressionDetailProps {
  checked: number;
  expressionData: expressionDataInterface;
  setExpressionChecked: React.Dispatch<React.SetStateAction<number>>;
  barHeight: number;
  index: number;
}

export default function GeneExpression({ geneName }: { geneName: string }) {
  const [keywords, setKeywords] = useState<string[]>([]);
  const [expressionData, setExpressionData] = useState<
    Array<expressionDataInterface>
  >();
  const [expressionChecked, setExpressionChecked] = useState(-1);
  const [chartHeigth, chartRef] = useElementHeigth(expressionData);

  useEffect(() => {
    async function fetchExpressionData(geneName: string) {
      const response = await fetch(apiUrl + "/gene/expression/" + geneName);
      let expressionData: expressionDataInterface[] = await response.json();
      expressionData = sortExpressionData(expressionData);
      const keywords = Array.from(
        new Set(expressionData.map((item) => item.keyword))
      ).sort();

      setKeywords(keywords);
      setExpressionData(expressionData);
    }

    if (geneName !== undefined) fetchExpressionData(geneName);
  }, [geneName]);

  const getColor = (eD: expressionDataInterface) => {
    if (eD.log2FC > 0) {
      return `hsla(31, 86%, 51%, ${getColorIntensity(eD.pvalue)})`;
    } else if (eD.log2FC < 0) {
      return `hsla(204, 82%, 57%, ${getColorIntensity(eD.pvalue)})`;
    } else {
      return `hsla(180, 0%, 46%, ${getColorIntensity(eD.pvalue)})`;
    }
  };

  const getColorIntensity = (pvalue: number) => {
    const logPvalue = Math.abs(Math.log10(pvalue));
    if (logPvalue > 10) {
      return 1;
    } else if (logPvalue < 3) {
      return 0.3;
    } else {
      return logPvalue * 0.1;
    }
  };

  const getChartHeight = (expressionData: expressionDataInterface[]) => {
    if (window !== undefined) {
      if (window.innerWidth >= 1920) {
        return expressionData.filter((eD) => eD.pvalue < 0.05).length * 5 + 20;
      } else if (window.innerWidth > 1400) {
        return expressionData.filter((eD) => eD.pvalue < 0.05).length * 7 + 20;
      } else {
        return expressionData.filter((eD) => eD.pvalue < 0.05).length * 10 + 20;
      }
    }
  };

  const getBarHeightInPx = (
    expressionData: expressionDataInterface[],
    chartHeigth: number
  ) => {
    const height = chartHeigth / expressionData.length;
    return Math.round(height);
  };

  const sortExpressionData = (expressionData: expressionDataInterface[]) => {
    return expressionData
      .sort(byKey("comp_id"))
      .sort((a, b) =>
        a.keyword < b.keyword ? -1 : a.keyword > b.keyword ? 1 : 0
      );
  };

  const displayExpressionInfo = (e) => {
    if (e.length > 0 && "_model" in e[0]) {
      const label = e[0]._model.label && e[0]._model.label;
      const position = e[0]._index;
      if (expressionChecked !== position) {
        setExpressionChecked(position);
      } else {
        setExpressionChecked(-1);
      }
    } else {
      setExpressionChecked(-1);
    }
  };

  const significantED =
    expressionData &&
    expressionData.filter(
      (eD) => eD.pvalue < 0.05 && (eD.log2FC < -1 || eD.log2FC > 1)
    );
  const barHeight =
    expressionData && getBarHeightInPx(significantED, chartHeigth);

  return (
    <>
      <GeneExpressionDescription geneName={geneName} />
      {expressionData && (
        <div className={styles.barChartContainer}>
          <div className={styles.barChart} ref={chartRef}>
            <HorizontalBar
              height={getChartHeight(significantED)}
              getElementAtEvent={displayExpressionInfo}
              data={{
                labels: significantED.map((eD) => eD.comp_id),
                datasets: [
                  {
                    label: "Log2FoldChange",
                    data: significantED.map((eD) => eD.log2FC),
                    backgroundColor: significantED.map((eD) => getColor(eD)),
                  },
                ],
                borderWidth: 1,
              }}
              options={options}
            />
          </div>
          <div>
            {significantED.map((eD, i) => {
              return (
                <ExpressionDetail
                  key={i}
                  checked={expressionChecked}
                  setExpressionChecked={setExpressionChecked}
                  expressionData={eD}
                  barHeight={barHeight}
                  index={i}
                />
              );
            })}
          </div>
        </div>
      )}

      <div className={styles.container}>
        <div className={styles.column}>
          {expressionData && (
            <ExpressionList
              expressionData={expressionData}
              title={"Non Significant"}
              filter_fn={isNotSignificant}
              keywords={keywords}
            />
          )}
        </div>

        <div className={styles.column}>
          {expressionData && (
            <ExpressionList
              expressionData={expressionData}
              title={"Non Expressed"}
              filter_fn={isNotExpressed}
              keywords={keywords}
            />
          )}
        </div>
      </div>
    </>
  );
}

const options = {
  legend: { display: false },
  title: {
    display: true,
    text:
      "Log2FoldChange of comparisons with significant expression differences",
  },
  scales: {
    yAxes: [
      {
        ticks: {
          beginAtZero: true,
        },
      },
    ],
  },
};

export function GeneExpressionDescription({ geneName }: { geneName: string }) {
  const [expressionInfochecked, setExpressionInfochecked] = useState(false);

  const dlGraphImage = (e: React.MouseEvent<HTMLAnchorElement>) => {
    const canvas = document.getElementsByClassName(
      "chartjs-render-monitor"
    )[0] as HTMLCanvasElement;
    const href = canvas.toDataURL("image/png");
    const link = e.target as HTMLAnchorElement;
    link.download = `${geneName}_differential_expression_graph.png`;
    link.href = href;
  };

  return (
    <>
      {" "}
      <div className={styles.expressionHeader}>
        <h3>
          Expression (RNA-Seq)
          <InfoToggle
            checked={expressionInfochecked}
            setChecked={setExpressionInfochecked}
          />
        </h3>
        <div
          className={
            expressionInfochecked ? styles.rnaSeqInfo : styles.rnaSeqInfoHidden
          }
        >
          Salmon [1] was used with default settings to calculate read counts
          based on Phatr3 transcript sequences. Differential expressions from
          pairwise comparisons have been computed using the R package DESeq2
          [2].
          <br />
          <br />
          <a href="https://www.nature.com/articles/nmeth.4197">
            [1] Patro, R., Duggal, G., Love, M. I., Irizarry, R. A., &
            Kingsford, C. (2017). Salmon provides fast and bias-aware
            quantification of transcript expression. Nature Methods.
          </a>
          <br />
          <br />
          <a href="https://www.doi.org/10.1186/s13059-014-0550-8">
            [2] Love, M.I., Huber, W., Anders, S. (2014) Moderated estimation of
            fold change and dispersion for RNA-seq data with DESeq2. Genome
            Biology, 15:550. 10.1186/s13059-014-0550-8
          </a>
        </div>
      </div>
      <hr />
      <div className={styles.descriptionAndDropDown}>
        <p style={{ width: "60%" }}>
          {"Transcripts were considered as differentially expressed if e-value < 0.05 with " +
            "log2 fold change <-1 for down-regulated and log2FC >1 for up-regulated. Log2FC," +
            " adjusted e-value and experiment informations are available by clicking on the plot bars."}
          <br />
        </p>
        <Dropdown
          name="Download"
          chevronPos={"25%"}
          contents={[
            <a onClick={dlGraphImage}>Graph Image</a>,
            <a
              href={
                apiUrl +
                "/gene/expression_data/" +
                `${geneName}_expression_data.tsv`
              }
            >
              Graph Data
            </a>,
          ]}
        />
      </div>
      <br />
    </>
  );
}

function ExpressionDetail(props: expressionDetailProps) {
  const {
    checked,
    expressionData,
    setExpressionChecked,
    barHeight,
    index,
  } = props;
  const topPosition = barHeight * index;
  const listPublications = extractPublication(
    expressionData.publication,
    expressionData.doi
  );

  return (
    <div
      className={
        checked === index ? styles.experienceInfo : styles.experienceInfoHidden
      }
      style={{ left: `70%`, top: `${topPosition + 20}px`, width: "30%" }}
    >
      <div className={styles.descriptionHeader}>
        <div>
          <strong>BioProject: </strong>
          <Link
            href={`/resources/transcriptomics/${expressionData.bioproject}`}
          >
            <a>{expressionData.bioproject}</a>
          </Link>
        </div>
        <div
          className={`${styles.experienceInfoCloseButton}`}
          onClick={() => setExpressionChecked(-1)}
        >
          <FontAwesomeIcon icon={faTimesCircle} />
        </div>
      </div>
      <p>{expressionData.description} </p>
      <div>
        {listPublications.map((value, i) => (
          <div style={{ marginBottom: "0.5em" }} key={i}>
            {value.doi === "NA" ? (
              <div>{value.Publication}</div>
            ) : (
              <a href={value.doi} target="_blank" rel="noopener noreferrer">
                {formatEtAl(value.Publication)}
              </a>
            )}
          </div>
        ))}
      </div>
      <p>{expressionData.comp}</p>
      <strong>
        <p>Log2FC: {expressionData.log2FC}</p>
        <p>Pvalue: {expressionData.pvalue}</p>
      </strong>
    </div>
  );
}

export function ExpressionList({
  expressionData,
  title,
  filter_fn,
  keywords,
}: {
  expressionData: expressionDataInterface[];
  title: string;
  filter_fn: (expressionData: expressionDataInterface) => boolean;
  keywords: string[];
}) {
  const eDFiltered = expressionData.filter((eD) => filter_fn(eD));
  return (
    <div className={styles.category}>
      <DownToggle
        title={title.toUpperCase()}
        titleClass={styles.categoryHeader}
      >
        {keywords.map((k, i) => {
          return (
            <ExpressionSubList
              key={i}
              expressionData={eDFiltered}
              keyword={k}
            />
          );
        })}
      </DownToggle>
    </div>
  );
}

export function ExpressionSubList({
  expressionData,
  keyword,
}: {
  expressionData: expressionDataInterface[];
  keyword: string;
}) {
  const toRender =
    expressionData.filter((eD) => eD.keyword === keyword).length > 0
      ? true
      : false;
  return (
    <div className={styles.subCategory}>
      {toRender && (
        <>
          <h4 className={styles.subCategoryHeader}>{keyword.toUpperCase()}</h4>
          {expressionData
            .filter((eD) => eD.keyword === keyword)
            .map((eD, i) => {
              return <ExpressionDisplay key={i} expressionData={eD} />;
            })}
        </>
      )}
    </div>
  );
}

const isNotSignificant = (expressionData: expressionDataInterface) => {
  if (
    expressionData.pvalue > 0.05 ||
    (expressionData.log2FC > -1 && expressionData.log2FC < 1)
  )
    return true;
  else return false;
};

const isNotExpressed = (expressionData: expressionDataInterface) => {
  if (expressionData.log2FC === 0 || expressionData.log2FC === "NA")
    return true;
  else return false;
};

export function ExpressionDisplay({
  expressionData,
}: {
  expressionData: expressionDataInterface;
}) {
  // Display bioproject tooltip when checked
  const [checked, setChecked] = useState(false);
  // Set offset of hidden div with relative information
  const [width, setWidth] = useState(0);
  const ref = useRef<HTMLHeadingElement>(null);
  // UseEffect doesn't re-run when ref is render,
  // I use this code to wait for ref.current to be present
  const setDescriptionWidth = async () => {
    while (width === 0) {
      await sleep(1000);
      if (ref.current !== null) setWidth(ref.current.offsetWidth);
    }
  };
  setDescriptionWidth();

  return (
    <div className={styles.row}>
      <div ref={ref} className={styles.divP}>
        {expressionData.comp_id}
        <InfoToggle checked={checked} setChecked={setChecked} />
      </div>
      <div
        className={
          checked ? styles.experienceInfo : styles.experienceInfoHidden
        }
        style={{ left: `${width + 50}px` }}
      >
        <p>
          BioProject:{" "}
          <Link
            href={`/resources/transcriptomics/${expressionData.bioproject}`}
          >
            <a>{expressionData.bioproject}</a>
          </Link>
        </p>
        <p>{expressionData.description} </p>
        <p>{expressionData.comp}</p>
      </div>
      {/* <p>{expressionData.status}</p> */}
    </div>
  );
}

const useElementHeigth = (
  dependancies?: any
): [number, React.MutableRefObject<HTMLDivElement>] => {
  const ref = useRef<HTMLDivElement>();
  const [ElementHeigth, setElementHeigth] = useState(0);
  useEffect(() => {
    if (ref.current !== undefined) setElementHeigth(ref.current.clientHeight);
  }, [ref.current, dependancies]);
  return [ElementHeigth, ref];
};

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
