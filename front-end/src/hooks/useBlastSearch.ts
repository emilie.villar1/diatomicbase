import { NextRouter } from "next/router";
import React, { useState, SetStateAction } from "react";
import apiUrl from "../utils/getApiUrl";

type blastType = "blastn" | "blastp" | "blastx"

export default function useBlastSearch(
  router: NextRouter
): [
  string,
  React.Dispatch<SetStateAction<string>>,
  string,
  () => void,
  (blast_type: blastType) => void,
] {
  const [blastSeq, setBlastSeq] = useState("");
  const [blastError, setBlastError] = useState("");

  const launchBlastIfValid = (blast_type: "blastn" | "blastp" | "blastx") => {
    if (blastSeq === "" || blastSeq.replaceAll(" ", "").length === 0) {
      setBlastError("No sequence inputed");
    } else if (isFasta(blastSeq) && containMultipleFasta(blastSeq)) {
      setBlastError("It should contain only 1 fasta");
    } else if (isFasta(blastSeq) && fastaIsWronglyFormatted(blastSeq)) {
      setBlastError("Fasta wrongly formated, where is the sequence ?");
    } else if (
      (blast_type === "blastn" || blast_type === "blastx") &&
      notDNA(blastSeq)
    ) {
      setBlastError("The Sequence/Fasta inputed doesn't look like DNA");
    } else if (blast_type === "blastp" && isDNA(blastSeq)) {
      setBlastError("It look like a DNA sequence, not AA");
    } else if (blast_type === "blastp" && notAA(blastSeq)) {
      setBlastError("The Sequence/Fasta inputed doesn't look like AA");
    } else {
      setBlastError("");
      blast(blast_type, blastSeq);
    }
  };

  const isFasta = (fasta: string) => {
    if (fasta.trim().startsWith(">")) return true;
    else return false;
  };

  const containMultipleFasta = (fasta: string) => {
    if (fasta.trim().split(">").length > 2) return true;
    else return false;
  };

  const fastaIsWronglyFormatted = (fasta: string) => {
    if (fasta.split("\n").length < 2) {
      return true;
    }
  };

  const getSeqFromPossibleFasta = (fasta: string) => {
    let lines = fasta.split("\n");
    if (lines[0].trim().startsWith(">")) {
      lines.shift();
    }
    const seq = lines.join("");
    return seq;
  };

  const notDNA = (fasta: string) => {
    const seq = getSeqFromPossibleFasta(fasta);
    const isNotDNA = /[^ATGCN\*]+/i.test(seq);
    return isNotDNA;
  };

  const isDNA = (fasta: string) => {
    return !notDNA(fasta)
  }

  const notAA = (fasta: string) => {
    const seq = getSeqFromPossibleFasta(fasta);
    const isNotAA = /[^ARNDCQEGHILKMFPSTWYV\*]+/i.test(seq);
    return isNotAA;
  };

  const blast = async (blast_type: blastType, blastSeq: string) => {
    const res = await fetch(apiUrl + "/search/blast/", {
      headers: {
        Accept: "text/plain",
        "Content-Type": "text/plain",
      },
      method: "POST",
      body: JSON.stringify({ blast_type, seq: blastSeq }),
    });
    const blast_id = await res.json();
    router.push({
      pathname: "/searchBlastResult",
      query: blast_id,
    });
  };

  const clearBlast = () => {
    setBlastSeq("");
    setBlastError("");
  }

  return [blastSeq, setBlastSeq, blastError, clearBlast, launchBlastIfValid];
}
